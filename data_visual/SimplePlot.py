from datetime import datetime

import numpy as np
from matplotlib import ticker
from matplotlib.ticker import FuncFormatter, MultipleLocator

__author__ = 'VB7'

import matplotlib.pyplot as plt

class SimplePlot:

    def __init__(self):
        plt.close()

    def equidate_ax(self, fig, ax, dates, fmt="%Y-%m-%d %H:%M:%S", label="Date"):
        #https://stackoverflow.com/questions/1273472/how-to-skip-empty-dates-weekends-in-a-financial-matplotlib-python-graph/2335781
        """
        Sets all relevant parameters for an equidistant date-x-axis.
        Tick Locators are not affected (set automatically)

        Args:
            fig: pyplot.figure instance
            ax: pyplot.axis instance (target axis)
            dates: iterable of datetime.date or datetime.datetime instances
            fmt: Display format of dates
            label: x-axis label
        Returns:
            None

        """
        N = len(dates)
        def format_date(index, pos):
            index = np.clip(int(index + 0.5), 0, N - 1)
            return dates[index].strftime(fmt)
        ax.xaxis.set_major_formatter(FuncFormatter(format_date))
        ax.set_xlabel(label)
        fig.autofmt_xdate()
        return

    def addMajorLineData(self, xValues, values, labelText, colorName='g'):
        intXvalues = np.arange(len(xValues))
        line = plt.plot(intXvalues, values, label=labelText)
        plt.setp(line, linewidth=1, color=colorName)  # linestyle='--'
        self.xValues = xValues
        return

    def addMinorLineData(self, xValues, values, labelText, colorName):
        intXvalues = np.arange(len(xValues))
        line = plt.plot(intXvalues, values, label=labelText)
        plt.setp(line, linewidth=1, color=colorName)
        return


    def addMinorSignalData(self, xValues, values, markerType,labelText, colorName):
        intXvalues = np.arange(len(xValues))
        line = plt.plot(intXvalues, values, label=labelText)
        plt.setp(line, marker=markerType, linewidth=1, markersize=10, color=colorName)
        return


    def showPlot(self):
        plt.legend(loc=0)
        plt.autoscale(False)
        self.equidate_ax(plt.gcf(), plt.gca(), self.xValues)
        plt.grid()
        plt.show()
        return

    def savePlot(self, fileName, title):
        plt.gcf().suptitle(title)
        plt.gcf().set_size_inches(10, 5)
        plt.gcf().dpi = 200
        plt.axes().tick_params(axis='x',which='minor',bottom=True)
        plt.legend(loc=0)
        plt.autoscale(False)
        # hacks
        if isinstance(self.xValues[0], datetime):
            self.equidate_ax(plt.gcf(), plt.gca(), self.xValues)
        if len(self.xValues)<=30:
            plt.gca().set_xticks(range(1, len(self.xValues)))
        plt.grid()
        fileName = fileName.replace("\n", "")
        plt.savefig(fileName)

    def getPlot(self):
        return plt