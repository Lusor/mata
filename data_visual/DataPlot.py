import numpy as np
from matplotlib import ticker
from matplotlib.ticker import FuncFormatter

__author__ = 'VB7'

import matplotlib.pyplot as plt

class DataPlot:

    def equidate_ax(self, fig, ax, dates, fmt="%Y-%m-%d", label="Date"):
        #https://stackoverflow.com/questions/1273472/how-to-skip-empty-dates-weekends-in-a-financial-matplotlib-python-graph/2335781
        """
        Sets all relevant parameters for an equidistant date-x-axis.
        Tick Locators are not affected (set automatically)

        Args:
            fig: pyplot.figure instance
            ax: pyplot.axis instance (target axis)
            dates: iterable of datetime.date or datetime.datetime instances
            fmt: Display format of dates
            label: x-axis label
        Returns:
            None

        """
        N = len(dates)
        def format_date(index, pos):
            index = np.clip(int(index + 0.5), 0, N - 1)
            return dates[index].strftime(fmt)
        ax.xaxis.set_major_formatter(FuncFormatter(format_date))
        ax.set_xlabel(label)
        fig.autofmt_xdate()
        return
#   def __init__(self):
      #  figure = plt.figure(facecolor='white')
      #  ax1 = plt.axes(frameon=False)

    def initFigures(self, number):
     #   plt.subplots()
        # ax1 = plt.subplot(411)
        # ax2 = plt.subplot(412)
        # ax3 = plt.subplot(413)
        # ax4 = plt.subplot(414)
        return

    xValues = None
    def addMajorLineData(self, plotID, xValues, values, labelText, colorName='g'):

        intXvalues = np.arange(len(xValues))
        ax = plt.subplot(plotID)
        line = plt.plot(intXvalues, values, label=labelText)
        plt.setp(line, linewidth=1, color=colorName)  # linestyle='--'

        if labelText == 'MFI':
            timeSeriesSize = len(xValues)
            topLimitValues = [80]*timeSeriesSize
            topLimit = plt.plot(intXvalues, topLimitValues)
            plt.setp(topLimit, linewidth=1, color='r', linestyle='--')

            bottomLimitValues = [20]*timeSeriesSize
            bottomLimit = plt.plot(intXvalues, bottomLimitValues)
            plt.setp(bottomLimit, linewidth=1, color='r', linestyle='--')
        if labelText == 'MACD':
            timeSeriesSize = len(xValues)
            topLimitValues = [0]*timeSeriesSize
            topLimit = plt.plot(intXvalues, topLimitValues)
            plt.setp(topLimit, linewidth=1, color='r', linestyle='--')

        # draw a window size
        if labelText == 'MACD':
            windowSize = 50
            timeSeriesSize = len(xValues)
            topLimitValues = [None]*timeSeriesSize
            for i in range(timeSeriesSize):
                if (i/windowSize)%2 == 0:
                    topLimitValues[i] = 0;
            topLimit = plt.plot(intXvalues, topLimitValues)
            plt.setp(topLimit, linewidth=1, color='k')


        self.xValues = xValues

        return

    def addMinorLineData(self, plotID, xValues, values, labelText, colorName):
        intXvalues = np.arange(len(xValues))

        plt.subplot(plotID)
        line = plt.plot(intXvalues, values, label=labelText)
        plt.setp(line, linewidth=1, color=colorName)
        return


    def addMinorSignalData(self, plotID, xValues, values, markerType,labelText, colorName):
        intXvalues = np.arange(len(xValues))
        plt.subplot(plotID)
        line = plt.plot(intXvalues, values, label=labelText)
        plt.setp(line, marker=markerType, linewidth=1, markersize=10, color=colorName)
        return


    def showPlot(self):
        plt.autoscale(False)
        plt.subplot(411)
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid()
        plt.legend(loc=0)
        self.equidate_ax(plt.gcf(), plt.gca(), self.xValues)


        plt.subplot(412)
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid()
        plt.legend(loc=0)
        self.equidate_ax(plt.gcf(), plt.gca(), self.xValues)


        plt.subplot(413)
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid()
        plt.legend(loc=0)
        self.equidate_ax(plt.gcf(), plt.gca(), self.xValues)

        plt.subplot(414)
        plt.xlabel('Time')
        plt.ylabel('Value')
        plt.grid()
        plt.legend(loc=0)
        self.equidate_ax(plt.gcf(), plt.gca(), self.xValues)

        plt.show()
        return

    def getPlot(self):
        return plt