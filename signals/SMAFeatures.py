import math
from sqlalchemy import Enum

from signals.FeatureExtraction import FeatureExtraction
from signals.Signals import Signals
from signals.MACDFeatures import MACDFeatures


class SMA_FEATURES(Enum):

    # CROSSOVER
    SMA_CROSS_PRICE = "SMA_CROSS_PRICE"

    # UP-DOWN
    SMA_GROWS = "SMA_GROWS"
    SMA_DECREASES = "SMA_DECREASES"

class SMAFeatures:
    sma = []
    tickData = []

    CROSS_WINDOW = 0
    TREND_WINDOW = 3
    featuresList = [SMA_FEATURES.SMA_GROWS, SMA_FEATURES.SMA_DECREASES,
                    SMA_FEATURES.SMA_CROSS_PRICE]

    def __init__(self, _tickData, _sma):
        self.sma = _sma
        self.tickData = _tickData
        return

    # def setData(self, _sma, _tickData):
    #     self.sma = _sma
    #     self.tickData = _tickData
    #     return

    def getFeatures(self, eSMAFeatures = featuresList, trendWindow = TREND_WINDOW, crosWindow = CROSS_WINDOW):

        arSize = len(self.sma)
        ftrMap = {}

        trendSMALine = MACDFeatures.trendDetection(self.sma, trendWindow)
        sma_price_up, sma_price_down = Signals.lineCrossOverW_directions(self.sma, self.tickData, self.CROSS_WINDOW)


        if SMA_FEATURES.SMA_CROSS_PRICE in self.featuresList:
            ftrMap[SMA_FEATURES.SMA_CROSS_PRICE + "_up"] = sma_price_up
            ftrMap[SMA_FEATURES.SMA_CROSS_PRICE + "_down"] = sma_price_down

        for i in range(arSize):
            for f in range(0, len(eSMAFeatures)):
                feature = eSMAFeatures[f]

                # grows-decrease
                if feature == SMA_FEATURES.SMA_GROWS:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendSMALine[i] == 1 else False
                if feature == SMA_FEATURES.SMA_DECREASES:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendSMALine[i] == -1 else False

        return ftrMap