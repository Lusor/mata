import enum

from signals.FeatureExtraction import FeatureExtraction
from signals.Signals import Signals


class RSI_FEATURES(enum.Enum):

    # 70-30
    RSI_MORE_70 = "RSI_MORE_70"
    RSI_LESS_30 = "RSI_LESS_30"

    # level crosses
    RSI_CROSS_70 = "RSI_CROSS_70"
    RSI_CROSS_30 = "RSI_CROSS_30"


class RSIFeatures:
    rsi = []
    tickData = []

    CROSS_WINDOW = 0
    TREND_WINDOW = 3
    featuresList = [RSI_FEATURES.RSI_MORE_70, RSI_FEATURES.RSI_LESS_30,
                    RSI_FEATURES.RSI_CROSS_70, RSI_FEATURES.RSI_CROSS_30]

    def __init__(self, _tickData, _rsi):
        self.tickData = _tickData
        self.rsi = _rsi
        return

    def getFeatures(self, eRSIFeatures = featuresList):

        arSize = len(self.rsi)
        ftrMap = {}

        level_30 = [30.0]*arSize
        level_70 = [70.0] * arSize

        crosses_70_up, crosses_70_down = Signals.lineCrossOverW_directions(self.rsi, level_70, self.CROSS_WINDOW)
        crosses_30_up, crosses_30_down = Signals.lineCrossOverW_directions(self.rsi, level_30, self.CROSS_WINDOW)

        if RSI_FEATURES.RSI_CROSS_70 in self.featuresList:
            ftrMap[RSI_FEATURES.RSI_CROSS_70 + "_up"] = crosses_70_up
            ftrMap[RSI_FEATURES.RSI_CROSS_70 + "_down"] = crosses_70_down

        if RSI_FEATURES.RSI_CROSS_30 in self.featuresList:
            ftrMap[RSI_FEATURES.RSI_CROSS_30 + "_up"] = crosses_30_up
            ftrMap[RSI_FEATURES.RSI_CROSS_30 + "_down"] = crosses_30_down

        for i in range(arSize):
            for f in range(0, len(eRSIFeatures)):
                feature = eRSIFeatures[f]

                # more 70, less 30
                if feature == RSI_FEATURES.RSI_MORE_70:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.rsi[i] >= 70 else False
                if feature == RSI_FEATURES.RSI_LESS_30:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.rsi[i] <= 30 else False

        return ftrMap
