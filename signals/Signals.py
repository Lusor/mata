import math

__author__ = 'VB7'


class Signals:

    # detect line crosses
    @staticmethod
    def lineCrossOver(lineOne, lineTwo):
        size = len(lineOne)
        result = [0]*size

        startPosition = 0
        for i in range(0, size):
            if math.isnan(lineOne[i]) or math.isnan(lineTwo[i]):
                startPosition = i+1;

        if(startPosition >= size):
            return result
        currentComparision = lineOne[startPosition] > lineTwo[startPosition]
        for i in range(startPosition + 1, size):
            if lineOne[i] < lineTwo[i] and currentComparision == True:
                result[i] = -1
                currentComparision = False
                continue
            if lineOne[i] > lineTwo[i] and currentComparision == False:
                result[i] = 1
                currentComparision = True
                continue
        return result;

    @staticmethod
    def lineCrossOverW(line1, line2, confidentWindow):
        size = len(line1)
        result = Signals.lineCrossOver(line1, line2)

        for i in range(size):
            if result[i] != 0:
                sign = result[i]
                contrastInWindow = True

                for j in range(i-confidentWindow, i+confidentWindow):
                    if j<0 or j>=size:
                        continue
                    if sign == -1:
                        if j < i and line1[j] < line2[j]:
                            contrastInWindow = False
                            break
                        if j > i and line1[j] > line2[j]:
                            contrastInWindow = False
                            break
                    if sign == 1:
                        if j < i and line1[j] > line2[j]:
                            contrastInWindow = False
                            break
                        if j > i and line1[j] < line2[j]:
                            contrastInWindow = False
                            break
                if not contrastInWindow:
                    result[i] = 0

        return result


    @staticmethod
    def macdLineCross(macdLine, signalLine, signalPeriod, confidentWindow):
        return Signals.lineCrossOverW(macdLine, signalLine, confidentWindow)


    @staticmethod
    def lineCrossOverW_directions(line1, line2, confidentWindow):

        size = len(line1)
        # the first direction: -1
        # the second direction: 1
        lineCrosses = Signals.lineCrossOverW(line1, line2, confidentWindow)
        # firstDirection = [False]*size
        # secondDirection = [False]*size

        # List Comprehensions
        firstDirection =  [True if x==-1 else False for x in lineCrosses]
        secondDirection = [True if x== 1 else False for x in lineCrosses]
        return firstDirection, secondDirection


    # detect when the line crosses limits
    @staticmethod
    def levelBeingOutOfLimits(line, topLimit, bottomLevel):
        size = len(line)
        result = [0]*size
        #result[0] = 1 if line[0] > topLimit else result[0]
        #result[0] = 1 if line[0] < bottomLevel else result[0]

        for i in range(1, size):
            if line[i] < bottomLevel:
                result[i] = 1
            if line[i] > topLimit:
                result[i] = -1
        return result;


    # merge signals inside a certain window
    @staticmethod
    def signalJoin(signalsOne, signalsTwo):
        window=50

        size = len(signalsOne)
        result = [None]*size
        for i in range(0, size):
            if signalsOne[i] != 0:
                # search in area of
                isFoundSame = False
                for j in range(i-window, i+window):
                    if j<0 or j>=size:
                        continue
                    if signalsTwo[j] == signalsOne[i]:
                        isFoundSame = True
                        break
                if isFoundSame:
                    result[i] = signalsOne[i]
        return result

    # select signals and put it close to line
    @staticmethod
    def selectSignals(mainLine, signals, level):
        size = len(mainLine)
        result = [None]*size
        for i in range(size):
            if signals[i] == level:
                if level > 0:
                    result[i] = mainLine[i]*0.98
                else:
                    result[i] = mainLine[i]*1.02
        return result