
class FeatureExtraction:

    @staticmethod
    def getOrCreate(featuresMap, featureName, size):
        if featureName not in featuresMap:
            featuresMap[featureName] = [False] * size
        return featuresMap[featureName]