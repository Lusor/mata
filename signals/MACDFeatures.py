import math
from sqlalchemy import Enum

from signals.FeatureExtraction import FeatureExtraction
from signals.Signals import Signals


class MACD_FEATURES(Enum):

    # MACD negative
    MACD_NEGATIVE = "MACD_NEGATIVE"
    # MACD positive
    MACD_POSITIVE = "MACD_POSITIVE"
    # macd grows
    MACD_GROWS = "MACD_GROWS"
    # macd decreases
    MACD_DECREASES = "MACD_DECREASES"
    # MACD grows
    SIGNAL_LINE_GROWS = "SIGNAL_LINE_GROWS"
    # MACD decreases
    SIGNAL_LINE_DECREASES = "SIGNAL_LINE_DECREASES"

    # Signal line positive
    SIGNAL_LINE_POSITIVE = "SIGNAL_LINE_POSITIVE"
    # Signal line negative
    SIGNAL_LINE_NEGATIVE = "SIGNAL_LINE_NEGATIVE"

    # crossover signal and macd
    SIGNAL_MACD_CROSSES = "SIGNAL_MACD_CROSSES"


    # histogram positive
    HISTOGRAM_POSITIVE = "HISTOGRAM_POSITIVE"
    # histogram negative
    HISTOGRAM_NEGATIVE = "HISTOGRAM_NEGATIVE"


class MACDFeatures:
    macd = []
    macdsignal = []
    macdhist = []

    CROSS_WINDOW = 0
    TREND_WINDOW = 3
    THRESHOLD = 1

    featuresList = [MACD_FEATURES.MACD_POSITIVE, MACD_FEATURES.MACD_NEGATIVE,
                    MACD_FEATURES.SIGNAL_LINE_POSITIVE, MACD_FEATURES.SIGNAL_LINE_NEGATIVE,
                    MACD_FEATURES.HISTOGRAM_POSITIVE, MACD_FEATURES.HISTOGRAM_NEGATIVE,
                    MACD_FEATURES.MACD_GROWS, MACD_FEATURES.MACD_DECREASES,
                    MACD_FEATURES.SIGNAL_LINE_GROWS, MACD_FEATURES.SIGNAL_LINE_DECREASES,
                    MACD_FEATURES.SIGNAL_MACD_CROSSES]

    def __init__(self, _macd, _macdsignal, _macdhist):
        self.macd = _macd;
        self.macdsignal = _macdsignal;
        self.macdhist = _macdhist;

        return

    def getFeatures(self, eMACDFeatures = featuresList):

        arSize = len(self.macd)
        ftrMap = {}

        macd_signal_up, macd_signal_down = Signals.lineCrossOverW_directions(self.macd, self.macdsignal, self.CROSS_WINDOW)
        trendMACDLine = MACDFeatures.trendDetection(self.macd, self.TREND_WINDOW)
        trendSignalLine = MACDFeatures.trendDetection(self.macdsignal, self.TREND_WINDOW)

        # crosses
        if MACD_FEATURES.SIGNAL_MACD_CROSSES in self.featuresList:
            ftrMap[MACD_FEATURES.SIGNAL_MACD_CROSSES + "_up"] = macd_signal_up
            ftrMap[MACD_FEATURES.SIGNAL_MACD_CROSSES + "_down"] = macd_signal_down

        for i in range(arSize):
            for f in range(0, len(eMACDFeatures)):
                feature = eMACDFeatures[f]

                # positive/negative
                if feature == MACD_FEATURES.MACD_POSITIVE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.macd[i] >= 0 else False
                if feature == MACD_FEATURES.MACD_NEGATIVE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.macd[i] < 0 else False

                if feature == MACD_FEATURES.HISTOGRAM_POSITIVE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.macdhist[i] > 0 else False
                if feature == MACD_FEATURES.HISTOGRAM_NEGATIVE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.macdhist[i] < 0 else False

                if feature == MACD_FEATURES.SIGNAL_LINE_POSITIVE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.macdsignal[i] > 0 else False
                if feature == MACD_FEATURES.SIGNAL_LINE_NEGATIVE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.macdsignal[i] < 0 else False

                # grow/decrease
                if feature == MACD_FEATURES.MACD_GROWS:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendMACDLine[i] == 1 else False
                if feature == MACD_FEATURES.MACD_DECREASES:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendMACDLine[i] == -1 else False

                if feature == MACD_FEATURES.SIGNAL_LINE_GROWS:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendSignalLine[i] == 1 else False
                if feature == MACD_FEATURES.SIGNAL_LINE_DECREASES:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendSignalLine[i] == -1 else False

        return ftrMap


    @staticmethod
    def trendDetection(values, window):
        size = len(values)
        trends = [0.0]*size

        for i in range(size):
            if math.isnan(values[i]):
                continue

            trendCount = 0
            trendValue = 0.0
            for j in range(i - window, i + window):
                if j <= 0 or j >= size:
                    continue

                diff = values[j] - values[j-1]
                trendCount += 1 if diff > 0 else -1
                trendValue += diff

            if trendCount < -window and trendValue < 0:
                trends[i] = -1
            if trendCount > window and trendValue > 0:
                trends[i] = 1
        return trends