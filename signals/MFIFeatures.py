import math
from sqlalchemy import Enum

from signals.FeatureExtraction import FeatureExtraction
from signals.Signals import Signals
from signals.MACDFeatures import MACDFeatures


class MFI_FEATURES(Enum):

    # 80-20
    MFI_MORE_80 = "MFI_MORE_80"
    MFI_LESS_20 = "MFI_LESS_20"

    # 70-30
    MFI_MORE_70 = "MFI_MORE_70"
    MFI_LESS_30 = "MFI_LESS_30"

    # UP-DOWN
    MFI_GROWS = "MFI_GROWS"
    MFI_DECREASES = "MFI_DECREASES"

    # CROSSES
    MFI_CROSS_80 = "MFI_CROSS_80"
    MFI_CROSS_20 = "MFI_CROSS_20"

    MFI_CROSS_70 = "MFI_CROSS_70"
    MFI_CROSS_30 = "MFI_CROSS_30"

class MFIFeatures:
    mfi = []
    tickData = []

    TREND_WINDOW = 3
    CROSS_WINDOW = 0
    featuresList = [MFI_FEATURES.MFI_MORE_80, MFI_FEATURES.MFI_LESS_20,
                    MFI_FEATURES.MFI_MORE_70, MFI_FEATURES.MFI_LESS_30,
                    MFI_FEATURES.MFI_GROWS, MFI_FEATURES.MFI_DECREASES,
                    MFI_FEATURES.MFI_CROSS_80, MFI_FEATURES.MFI_CROSS_20,
                    MFI_FEATURES.MFI_CROSS_70, MFI_FEATURES.MFI_CROSS_30]

    def __init__(self, _tickData, _mfi):
        self.tickData = _tickData
        self.mfi = _mfi
        return

    def getFeatures(self, eMFIFeatures = featuresList, trendWindow = TREND_WINDOW,
                    crossWindow = CROSS_WINDOW):

        arSize = len(self.mfi)
        ftrMap = {}

        level_30 = [30.0]*arSize
        level_70 = [70.0] * arSize
        level_20 = [20.0]*arSize
        level_80 = [80.0] * arSize

        trendMFILine = MACDFeatures.trendDetection(self.mfi, trendWindow)

        crosses_70_up, crosses_70_down = Signals.lineCrossOverW_directions(self.mfi, level_70, self.CROSS_WINDOW)
        crosses_30_up, crosses_30_down = Signals.lineCrossOverW_directions(self.mfi, level_30, self.CROSS_WINDOW)

        crosses_80_up, crosses_80_down = Signals.lineCrossOverW_directions(self.mfi, level_80, self.CROSS_WINDOW)
        crosses_20_up, crosses_20_down = Signals.lineCrossOverW_directions(self.mfi, level_20, self.CROSS_WINDOW)


        if MFI_FEATURES.MFI_CROSS_80 in self.featuresList:
            ftrMap[MFI_FEATURES.MFI_CROSS_80 + "_up"] = crosses_80_up
            ftrMap[MFI_FEATURES.MFI_CROSS_80 + "_down"] = crosses_80_down

        if MFI_FEATURES.MFI_CROSS_20 in self.featuresList:
            ftrMap[MFI_FEATURES.MFI_CROSS_20 + "_up"] = crosses_20_up
            ftrMap[MFI_FEATURES.MFI_CROSS_20 + "_down"] = crosses_20_down

        if MFI_FEATURES.MFI_CROSS_70 in self.featuresList:
            ftrMap[MFI_FEATURES.MFI_CROSS_70 + "_up"] = crosses_70_up
            ftrMap[MFI_FEATURES.MFI_CROSS_70 + "_down"] = crosses_70_down

        if MFI_FEATURES.MFI_CROSS_30 in self.featuresList:
            ftrMap[MFI_FEATURES.MFI_CROSS_30 + "_up"] = crosses_30_up
            ftrMap[MFI_FEATURES.MFI_CROSS_30 + "_down"] = crosses_30_down


        for i in range(arSize):
            for f in range(0, len(eMFIFeatures)):
                feature = eMFIFeatures[f]

                # positive/negative
                if feature == MFI_FEATURES.MFI_MORE_80:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.mfi[i] >= 80 else False
                if feature == MFI_FEATURES.MFI_LESS_20:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.mfi[i] <= 20 else False

                if feature == MFI_FEATURES.MFI_MORE_70:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.mfi[i] >= 70 else False
                if feature == MFI_FEATURES.MFI_LESS_30:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.mfi[i] <= 30 else False

                if feature == MFI_FEATURES.MFI_GROWS:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendMFILine[i] == 1 else False
                if feature == MFI_FEATURES.MFI_DECREASES:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if trendMFILine[i] == -1 else False

        return ftrMap