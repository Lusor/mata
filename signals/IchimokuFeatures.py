import math
from sqlalchemy import Enum
from signals.FeatureExtraction import FeatureExtraction
from signals.Signals import Signals

class ICHIMOKU_FTR(Enum):

    # crosses
    CROSS_PRICE_REDLINE = "CROSS_PRICE_REDLINE"
    CROSS_PRICE_BLUELINE = "CROSS_PRICE_BLUELINE"
    CROSS_PRICE_SENKOU_A = "CROSS_PRICE_SENKOU_A"
    CROSS_PRICE_SENKOU_B = "CROSS_PRICE_SENKOU_B"
    CROSS_SENKOU_A_SENKOU_B = "CROSS_SENKOU_A_SENKOU_B"
    CROSS_TENKAN_KIJUN = "CROSS_TENKAN_KIJUN"

    # positioning, cloud
    PRICE_ABOVE_CLOUD = "PRICE_ABOVE_CLOUD"
    PRICE_IN_CLOUD = "PRICE_IN_CLOUD"
    PRICE_BELOW_CLOUD = "PRICE_BELOW_CLOUD"
    PRICE_CROSS_CLOUD_FROM_BOTTOM = "PRICE_CROSS_CLOUD_FROM_BOTTOM"
    PRICE_CROSS_CLOUD_FROM_TOP = "PRICE_CROSS_CLOUD_FROM_TOP"

    PRICE_MORE_TENKAN_KIJUN_SENKOU_A_SENKOU_B = "PRICE_MORE_TENKAN_KIJUN_SENKOU_A_SENKOU_B"
    PRICE_LESS_TENKAN_KIJUN_SENKOU_A_SENKOU_B = "PRICE_LESS_TENKAN_KIJUN_SENKOU_A_SENKOU_B"

    # cloud
    CLOUD_RED_COLOR= "CLOUD_RED_COLOR"
    CLOUD_GREEN_COLOR= "CLOUD_GREEN_COLOR"

    # the order
    RED_ABOVE_BLUE = "RED_ABOVE_BLUE"
    CHIKOU_ABOVE_PRICE = "CHIKOU_ABOVE_PRICE"
    CHIKOU_BELOW_PRICE = "CHIKOU_BELOW_PRICE"

    RED_ABOVE_BLUE_AND_ABOVE_CLOUD = "RED_ABOVE_BLUE_AND_ABOVE_CLOUD"
    RED_ABOVE_BLUE_BUT_BELOW_CLOUD = "RED_ABOVE_BLUE_BUT_BELOW_CLOUD"

    # for three lines strategy
    # should be like in plots
    PRICE_TENKAN_LESS_TENKAN_CLOUD = "PRICE_TENKAN_LESS_TENKAN_CLOUD"
    TENKAN_KIJUN_LESS_KIJUN_CLOUD = "TENKAN_KIJUN_LESS_KIJUN_CLOUD"

class IchimokuFeatures:
    EPSLN = 10.0/100000.0

    price = []
    red_tenkan = []
    blue_kijun = []
    green_chikou = []
    senkou_a = []
    senkou_b = []

    CROSS_WINDOW = 0
    TREND_WINDOW = 3
    featuresList = [ICHIMOKU_FTR.CROSS_PRICE_REDLINE, ICHIMOKU_FTR.CROSS_PRICE_BLUELINE, ICHIMOKU_FTR.CROSS_PRICE_SENKOU_A,
                    ICHIMOKU_FTR.CROSS_PRICE_SENKOU_B, ICHIMOKU_FTR.CROSS_SENKOU_A_SENKOU_B,
                    ICHIMOKU_FTR.PRICE_ABOVE_CLOUD, ICHIMOKU_FTR.PRICE_IN_CLOUD,
                    ICHIMOKU_FTR.PRICE_BELOW_CLOUD, ICHIMOKU_FTR.PRICE_CROSS_CLOUD_FROM_BOTTOM, ICHIMOKU_FTR.PRICE_CROSS_CLOUD_FROM_TOP,
                    ICHIMOKU_FTR.CLOUD_RED_COLOR, ICHIMOKU_FTR.CLOUD_GREEN_COLOR, ICHIMOKU_FTR.RED_ABOVE_BLUE,
                    ICHIMOKU_FTR.CHIKOU_ABOVE_PRICE,
                    ICHIMOKU_FTR.RED_ABOVE_BLUE_AND_ABOVE_CLOUD, ICHIMOKU_FTR.RED_ABOVE_BLUE_BUT_BELOW_CLOUD,
                    ICHIMOKU_FTR.CROSS_TENKAN_KIJUN]

    def __init__(self, _price, _red_tenkan, _blue_kijun, _green_chikou,
                 _senkou_a, _senkou_b,
                 tenkanPeriod, kijunPeriod, senkouPeriod):
        self.price = _price
        self.red_tenkan = _red_tenkan
        self.blue_kijun = _blue_kijun
        self.green_chikou = _green_chikou
        self.senkou_a = _senkou_a
        self.senkou_b = _senkou_b

        self.tenkanT = tenkanPeriod
        self.kijunT= kijunPeriod
        self.senkouT = senkouPeriod

        # define cloud
        self.cloud = [0]*len(self.price)
        for i in range(len(self.cloud)):
            value = None if math.isnan(self.senkou_a[i]) or math.isnan(self.senkou_b[i]) else \
                (self.senkou_a[i], self.senkou_b[i]) if self.senkou_a[i] < self.senkou_b[i] else (self.senkou_b[i], self.senkou_a[i])
            self.cloud[i] = value


        # TENKAN_HACK_PERIOD = 35
        # self.tenkanSellHack = [False] * len(self.price)
        # for i in range(len(self.price)):
        #     if i > kijunPeriod and self.cloud[i-kijunPeriod] != None:
        #         if self.red_tenkan[i] < (self.cloud[i-kijunPeriod])[0] and self.price[i] < (self.cloud[i-kijunPeriod])[0]:
        #             markerArr = [0] * TENKAN_HACK_PERIOD
        #             for j in range(len(markerArr)-1, -1, -1):
        #                 #print j
        #                 loc_index = i + (j+1-TENKAN_HACK_PERIOD)
        #                 #print loc_index
        #                 if self.cloud[loc_index-kijunPeriod] != None and self.red_tenkan[loc_index] <= (self.cloud[loc_index-kijunPeriod])[0]:
        #                     markerArr[j] = -1
        #                 elif self.cloud[loc_index-kijunPeriod] != None and self.red_tenkan[loc_index] >= (self.cloud[loc_index-kijunPeriod])[1]:
        #                     markerArr[j] = 1
        #             firstMinus = False
        #             secondPlus = False
        #             thirdMinus = False
        #             for j in range(len(markerArr)-1, -1, -1):
        #                 if markerArr[j] == -1 and firstMinus != True:
        #                     firstMinus = True
        #                 if markerArr[j] == 1 and firstMinus == True:
        #                     secondPlus = True
        #                 if markerArr[j] == -1 and firstMinus == True and secondPlus == True:
        #                     thirdMinus = True
        #                     break
        #             self.tenkanSellHack[i] = firstMinus and secondPlus and thirdMinus
        #
        #
        # self.tenkanBuyHack = [False] * len(self.price)
        # for i in range(len(self.price)):
        #     if i > kijunPeriod and self.cloud[i-kijunPeriod] != None:
        #         if self.red_tenkan[i] > (self.cloud[i-kijunPeriod])[1] and self.price[i] > (self.cloud[i-kijunPeriod])[1]:
        #             markerArr = [0] * TENKAN_HACK_PERIOD
        #             for j in range(len(markerArr)-1, -1, -1):
        #                 loc_index = i + (j+1-TENKAN_HACK_PERIOD)
        #                 print loc_index
        #                 if self.cloud[loc_index-kijunPeriod] != None and self.red_tenkan[loc_index] <= (self.cloud[loc_index-kijunPeriod])[0]:
        #                     markerArr[j] = -1
        #                 elif self.cloud[loc_index-kijunPeriod] != None and self.red_tenkan[loc_index] >= (self.cloud[loc_index-kijunPeriod])[1]:
        #                     markerArr[j] = 1
        #             firstPlus = False
        #             secondMinus = False
        #             thirdPlus = False
        #             for j in range(len(markerArr)-1, -1, -1):
        #                 if markerArr[j] == 1 and firstPlus != True:
        #                     firstPlus = True
        #                 if markerArr[j] == -1 and firstPlus == True:
        #                     secondMinus = True
        #                 if markerArr[j] == 1 and firstPlus == True and secondMinus == True:
        #                     thirdPlus = True
        #                     break
        #             self.tenkanBuyHack[i] = firstPlus and secondMinus and thirdPlus

        return

    def getFeatures(self, eIchimokuFeatures = featuresList):

        clT = self.kijunT
        arSize = len(self.price)
        ftrMap = {}

        # crosses
        if ICHIMOKU_FTR.CROSS_PRICE_REDLINE in eIchimokuFeatures:
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.CROSS_PRICE_REDLINE,
                                           self.price, self.red_tenkan, self.CROSS_WINDOW)
        if ICHIMOKU_FTR.CROSS_PRICE_BLUELINE in eIchimokuFeatures:
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.CROSS_PRICE_BLUELINE,
                                           self.price, self.blue_kijun, self.CROSS_WINDOW)
        if ICHIMOKU_FTR.CROSS_PRICE_SENKOU_A in eIchimokuFeatures:
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.CROSS_PRICE_SENKOU_A,
                                           self.price, self.senkou_a, self.CROSS_WINDOW)
        if ICHIMOKU_FTR.CROSS_PRICE_SENKOU_B in eIchimokuFeatures:
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.CROSS_PRICE_SENKOU_B,
                                           self.price, self.senkou_b, self.CROSS_WINDOW)
        if ICHIMOKU_FTR.CROSS_SENKOU_A_SENKOU_B in eIchimokuFeatures:
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.CROSS_SENKOU_A_SENKOU_B,
                                           self.senkou_a, self.senkou_b, self.CROSS_WINDOW)

        if ICHIMOKU_FTR.CROSS_TENKAN_KIJUN in eIchimokuFeatures:
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.CROSS_TENKAN_KIJUN,
                                           self.red_tenkan, self.blue_kijun, self.CROSS_WINDOW)

        # price cross the lower and upper border of the cloud
        if ICHIMOKU_FTR.PRICE_CROSS_CLOUD_FROM_BOTTOM in eIchimokuFeatures:
            cloud_lower_border = [x[0] for x in self.cloud]
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.PRICE_CROSS_CLOUD_FROM_BOTTOM,
                                           self.price, cloud_lower_border, self.CROSS_WINDOW)
        if ICHIMOKU_FTR.PRICE_CROSS_CLOUD_FROM_TOP in eIchimokuFeatures:
            cloud_upper_border = [x[1] for x in self.cloud]
            IchimokuFeatures.registerCross(ftrMap, ICHIMOKU_FTR.PRICE_CROSS_CLOUD_FROM_TOP,
                                           self.price, cloud_upper_border, self.CROSS_WINDOW)

        for i in range(arSize):
            for f in range(0, len(eIchimokuFeatures)):
                feature = eIchimokuFeatures[f]

                # price and cloud
                if feature == ICHIMOKU_FTR.PRICE_BELOW_CLOUD:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.cloud[i-clT] != None and self.price[i] < (self.cloud[i-clT])[0] else False
                if feature == ICHIMOKU_FTR.PRICE_IN_CLOUD:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if (self.cloud[i-clT] != None and self.price[i] >= (self.cloud[i-clT])[0] and self.price[i] <= (self.cloud[i-clT])[1]) else False
                if feature == ICHIMOKU_FTR.PRICE_ABOVE_CLOUD:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.cloud[i-clT] != None and self.price[i] > (self.cloud[i-clT])[1] else False

                if feature == ICHIMOKU_FTR.PRICE_TENKAN_LESS_TENKAN_CLOUD:
                    locRes = False
                    if(self.cloud[i-clT] != None):
                        # if not in the cloud
                        if self.red_tenkan[i] < (self.cloud[i-clT])[0] or self.red_tenkan[i] > (self.cloud[i-clT])[1]:
                            cloudValue = self.cloud[i-clT][0] if math.fabs(self.cloud[i-clT][0] - self.red_tenkan[i]) < math.fabs(self.cloud[i-clT][1] - self.red_tenkan[i]) else self.cloud[i-clT][1]
                            tenkanValue = self.red_tenkan[i]
                            priceValue = self.price[i]
                            locRes = True if math.fabs(priceValue-tenkanValue) < math.fabs(tenkanValue-cloudValue) else False
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = locRes

                if feature == ICHIMOKU_FTR.TENKAN_KIJUN_LESS_KIJUN_CLOUD:
                    locRes = False
                    if(self.cloud[i-clT] != None):
                        # if not in the cloud
                        if self.red_tenkan[i] < (self.cloud[i-clT])[0] or self.red_tenkan[i] > (self.cloud[i-clT])[1]:
                            cloudValue = self.cloud[i-clT][0] if math.fabs(self.cloud[i-clT][0] - self.blue_kijun[i]) < math.fabs(self.cloud[i-clT][1] - self.blue_kijun[i]) else self.cloud[i-clT][1]
                            tenkanValue = self.red_tenkan[i]
                            kijunValue = self.blue_kijun[i]
                            locRes = True if math.fabs(tenkanValue-kijunValue) < math.fabs(kijunValue-cloudValue) else False
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = locRes

                # cloud color
                if feature == ICHIMOKU_FTR.CLOUD_RED_COLOR:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.senkou_b[i] > self.senkou_a[i] else False
                if feature == ICHIMOKU_FTR.CLOUD_GREEN_COLOR:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.senkou_a[i] > self.senkou_b[i] else False

                # more
                if feature == ICHIMOKU_FTR.RED_ABOVE_BLUE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.red_tenkan[i] > self.blue_kijun[i] else False
                if feature == ICHIMOKU_FTR.CHIKOU_ABOVE_PRICE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.green_chikou[i] - self.price[i] >= self.EPSLN else False
                if feature == ICHIMOKU_FTR.CHIKOU_BELOW_PRICE:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.price[i] - self.green_chikou[i] >= self.EPSLN else False
                   # print str(i) + " "+ str(self.price[i]) + " " + str(self.green_chikou[i]) + " " + str(self.EPSLN)

                if feature == ICHIMOKU_FTR.RED_ABOVE_BLUE_AND_ABOVE_CLOUD:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.red_tenkan[i] > self.blue_kijun[i] \
                        and self.blue_kijun[i] > (self.cloud[i])[1] else False
                if feature == ICHIMOKU_FTR.RED_ABOVE_BLUE_BUT_BELOW_CLOUD:
                    FeatureExtraction.getOrCreate(ftrMap, feature, arSize)[i] = True if self.red_tenkan[i] > self.blue_kijun[i] \
                        and self.red_tenkan[i] < (self.cloud[i])[0] else False

                if feature == ICHIMOKU_FTR.PRICE_MORE_TENKAN_KIJUN_SENKOU_A_SENKOU_B and i >= clT:
                    FeatureExtraction.getOrCreate(ftrMap, feature,
                              arSize)[i] = True if self.price[i] > self.red_tenkan[i] and self.red_tenkan[i] > self.blue_kijun[i]\
                                                    and self.blue_kijun[i] > self.senkou_a[i - clT] and self.blue_kijun[i] > self.senkou_b[i - clT] else False

                if feature == ICHIMOKU_FTR.PRICE_LESS_TENKAN_KIJUN_SENKOU_A_SENKOU_B and i >= clT:
                    FeatureExtraction.getOrCreate(ftrMap, feature,
                              arSize)[i] = True if self.price[i] < self.red_tenkan[i] and self.red_tenkan[i] < self.blue_kijun[i] \
                                                   and self.blue_kijun[i] < self.senkou_a[i - clT] and self.blue_kijun[i] < self.senkou_b[i - clT] else False
        return ftrMap

    @staticmethod
    def registerCross(ftrMap, type, firstLine, secondLine, window):
            price_redline_up, price_redline_down = Signals.lineCrossOverW_directions(firstLine, secondLine, window)
            ftrMap[type + "_UP"] = price_redline_up
            ftrMap[type + "_DOWN"] = price_redline_down