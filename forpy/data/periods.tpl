<chart>
id=132143875106490258
symbol=EURUSD
period_type=1
period_size=1
tester=1
digits=5
tick_size=0.000000
position_time=0
scale_fix=0
scale_fixed_min=1.117800
scale_fixed_max=1.141700
scale_fix11=0
scale_bar=0
scale_bar_val=1.000000
scale=4
mode=1
fore=0
grid=1
volume=0
scroll=0
shift=1
shift_size=20.103093
fixed_pos=0.000000
ohlc=1
one_click=0
one_click_btn=1
bidline=1
askline=0
lastline=0
days=0
descriptions=0
tradelines=1
window_left=0
window_top=0
window_right=0
window_bottom=0
window_type=3
floating=0
floating_left=0
floating_top=0
floating_right=0
floating_bottom=0
floating_type=1
floating_toolbar=1
floating_tbstate=
background_color=16777215
foreground_color=0
barup_color=0
bardown_color=0
bullcandle_color=16777215
bearcandle_color=0
chartline_color=0
volumes_color=32768
grid_color=12632256
bidline_color=12632256
askline_color=12632256
lastline_color=12632256
stops_color=17919
windows_total=1

<window>
height=100.000000
objects=3

<indicator>
name=Main
path=
apply=1
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=0
fixed_height=-1
</indicator>


<object>
color = 11296515
type = 2
date1 = 1546832656
date2 = 1546834390
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546834541
date2 = 1546837251
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546837695
date2 = 1546848434
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546848876
date2 = 1546850963
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546851723
date2 = 1546851737
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546851824
date2 = 1546851912
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546851983
date2 = 1546852104
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546852174
date2 = 1546852462
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546854039
date2 = 1546854145
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546854260
date2 = 1546854297
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546854460
date2 = 1546876646
value1 = 1.14166

value2 = 1.14166

</object>
<object>
color = 11296515
type = 2
date1 = 1546876646
date2 = 1546876677
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546876774
date2 = 1546876816
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546876913
date2 = 1546882498
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546882565
date2 = 1546882578
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546882658
date2 = 1546882871
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546882946
date2 = 1546883179
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546883251
date2 = 1546905539
value1 = 1.14557

value2 = 1.14557

</object>
<object>
color = 11296515
type = 2
date1 = 1546912985
date2 = 1546914460
value1 = 1.14691

value2 = 1.14691

</object>
<object>
color = 11296515
type = 2
date1 = 1547051790
date2 = 1547053272
value1 = 1.14979

value2 = 1.14979

</object>
<object>
color = 11296515
type = 2
date1 = 1547053273
date2 = 1547055151
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1547055630
date2 = 1547055735
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1547058937
date2 = 1547060113
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1547061275
date2 = 1547061292
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1547061793
date2 = 1547061798
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1547061996
date2 = 1547061997
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1547062108
date2 = 1547067638
value1 = 1.15232

value2 = 1.15232

</object>
<object>
color = 11296515
type = 2
date1 = 1548267629
date2 = 1548267853
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548267914
date2 = 1548267914
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548269003
date2 = 1548269259
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548269355
date2 = 1548269356
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548269737
date2 = 1548269775
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548269939
date2 = 1548269968
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548270075
date2 = 1548270744
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548271013
date2 = 1548271036
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548271116
date2 = 1548273914
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548274115
date2 = 1548274120
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548274519
date2 = 1548274519
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548275104
date2 = 1548275413
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548275553
date2 = 1548275726
value1 = 1.13866

value2 = 1.13866

</object>
<object>
color = 11296515
type = 2
date1 = 1548288000
date2 = 1548321569
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548322739
date2 = 1548322742
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548326200
date2 = 1548326219
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548347136
date2 = 1548347142
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548347560
date2 = 1548347560
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548348065
date2 = 1548348077
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548348925
date2 = 1548349456
value1 = 1.13620

value2 = 1.13620

</object>
<object>
color = 11296515
type = 2
date1 = 1548633600
date2 = 1548636750
value1 = 1.13854

value2 = 1.13854

</object>
<object>
color = 11296515
type = 2
date1 = 1548636840
date2 = 1548694336
value1 = 1.13854

value2 = 1.13854

</object>
<object>
color = 11296515
type = 2
date1 = 1548694336
date2 = 1548714009
value1 = 1.14245

value2 = 1.14245

</object>
<object>
color = 11296515
type = 2
date1 = 1548714088
date2 = 1548719493
value1 = 1.14245

value2 = 1.14245

</object>
<object>
color = 11296515
type = 2
date1 = 1548719588
date2 = 1548719629
value1 = 1.14245

value2 = 1.14245

</object>
<object>
color = 11296515
type = 2
date1 = 1548882220
date2 = 1548882402
value1 = 1.14639

value2 = 1.14639

</object>
<object>
color = 11296515
type = 2
date1 = 1548882484
date2 = 1548882500
value1 = 1.14639

value2 = 1.14639

</object>
<object>
color = 11296515
type = 2
date1 = 1548882648
date2 = 1548884279
value1 = 1.14639

value2 = 1.14639

</object>
<object>
color = 11296515
type = 2
date1 = 1548884280
date2 = 1548884300
value1 = 1.14892

value2 = 1.14892

</object>
<object>
color = 11296515
type = 2
date1 = 1548884381
date2 = 1548884459
value1 = 1.14892

value2 = 1.14892

</object>
<object>
color = 11296515
type = 2
date1 = 1548884560
date2 = 1548885023
value1 = 1.14892

value2 = 1.14892

</object>
<object>
color = 11296515
type = 2
date1 = 1548885605
date2 = 1548885621
value1 = 1.14892

value2 = 1.14892

</object>
<object>
color = 11296515
type = 2
date1 = 1548885761
date2 = 1548885762
value1 = 1.14892

value2 = 1.14892

</object>
<object>
color = 11296515
type = 2
date1 = 1548892830
date2 = 1548892830
value1 = 1.14580

value2 = 1.14580

</object>
<object>
color = 11296515
type = 2
date1 = 1548892920
date2 = 1548900388
value1 = 1.14580

value2 = 1.14580

</object>
<object>
color = 11296515
type = 2
date1 = 1548900396
date2 = 1548900919
value1 = 1.14788

value2 = 1.14788

</object>
<object>
color = 11296515
type = 2
date1 = 1548901219
date2 = 1548902102
value1 = 1.14788

value2 = 1.14788

</object>
<object>
color = 11296515
type = 2
date1 = 1548904098
date2 = 1548905547
value1 = 1.14788

value2 = 1.14788

</object>
<object>
color = 1918177
type = 2
date1 = 1546439317
date2 = 1546439334
value1 = 1.14077

value2 = 1.14077

</object>
<object>
color = 1918177
type = 2
date1 = 1546439395
date2 = 1546439546
value1 = 1.14077

value2 = 1.14077

</object>
<object>
color = 1918177
type = 2
date1 = 1546439618
date2 = 1546439621
value1 = 1.14077

value2 = 1.14077

</object>
<object>
color = 1918177
type = 2
date1 = 1546441120
date2 = 1546441154
value1 = 1.14077

value2 = 1.14077

</object>
<object>
color = 1918177
type = 2
date1 = 1546441220
date2 = 1546441303
value1 = 1.14077

value2 = 1.14077

</object>
<object>
color = 1918177
type = 2
date1 = 1546441435
date2 = 1546443039
value1 = 1.14077

value2 = 1.14077

</object>
<object>
color = 1918177
type = 2
date1 = 1546443040
date2 = 1546443075
value1 = 1.13824

value2 = 1.13824

</object>
<object>
color = 1918177
type = 2
date1 = 1546443697
date2 = 1546443718
value1 = 1.13824

value2 = 1.13824

</object>
<object>
color = 1918177
type = 2
date1 = 1546444002
date2 = 1546444023
value1 = 1.13824

value2 = 1.13824

</object>
<object>
color = 1918177
type = 2
date1 = 1546444084
date2 = 1546449877
value1 = 1.13824

value2 = 1.13824

</object>
<object>
color = 1918177
type = 2
date1 = 1547424030
date2 = 1547449413
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547449620
date2 = 1547451392
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547451515
date2 = 1547451539
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547451810
date2 = 1547451862
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547453652
date2 = 1547459729
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547459816
date2 = 1547459846
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547459978
date2 = 1547460055
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547460160
date2 = 1547460267
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547460685
date2 = 1547460742
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547460860
date2 = 1547462847
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547462910
date2 = 1547463672
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547463785
date2 = 1547488528
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547488591
date2 = 1547492296
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547492556
date2 = 1547493143
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547493884
date2 = 1547494013
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547494146
date2 = 1547495300
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547495551
date2 = 1547495723
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547495958
date2 = 1547510310
value1 = 1.14792

value2 = 1.14792

</object>
<object>
color = 1918177
type = 2
date1 = 1547561320
date2 = 1547561727
value1 = 1.14286

value2 = 1.14286

</object>
<object>
color = 1918177
type = 2
date1 = 1547575538
date2 = 1547575597
value1 = 1.14286

value2 = 1.14286

</object>
<object>
color = 1918177
type = 2
date1 = 1547575743
date2 = 1547577841
value1 = 1.14286

value2 = 1.14286

</object>
<object>
color = 1918177
type = 2
date1 = 1547577845
date2 = 1547577861
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547577936
date2 = 1547577994
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547580184
date2 = 1547580198
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547580276
date2 = 1547580508
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547582015
date2 = 1547582058
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547582441
date2 = 1547582459
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547582534
date2 = 1547583376
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547583446
date2 = 1547584094
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547584836
date2 = 1547584936
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547585062
date2 = 1547585101
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547585246
date2 = 1547585309
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547585845
date2 = 1547585877
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547588379
date2 = 1547588383
value1 = 1.13976

value2 = 1.13976

</object>
<object>
color = 1918177
type = 2
date1 = 1547631391
date2 = 1547631615
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547631690
date2 = 1547631937
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547632538
date2 = 1547632827
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547644585
date2 = 1547644630
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547644707
date2 = 1547644874
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547644956
date2 = 1547645157
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547645375
date2 = 1547654604
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547654667
date2 = 1547654715
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547654795
date2 = 1547654896
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547655939
date2 = 1547655941
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547657861
date2 = 1547657957
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547658928
date2 = 1547659017
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547661325
date2 = 1547661722
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547677478
date2 = 1547677518
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547677713
date2 = 1547678001
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547678431
date2 = 1547679720
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547679840
date2 = 1547679855
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547679959
date2 = 1547679974
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547680273
date2 = 1547680273
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547680990
date2 = 1547683139
value1 = 1.13990

value2 = 1.13990

</object>
<object>
color = 1918177
type = 2
date1 = 1547700039
date2 = 1547700119
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547700254
date2 = 1547700414
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547701141
date2 = 1547701165
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547701266
date2 = 1547701938
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547702070
date2 = 1547702330
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547702430
date2 = 1547714279
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547714643
date2 = 1547715903
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547716136
date2 = 1547721635
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547721746
date2 = 1547721794
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547727212
date2 = 1547727323
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547727987
date2 = 1547727998
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547739045
date2 = 1547740156
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547742518
date2 = 1547742567
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547742640
date2 = 1547742681
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547742807
date2 = 1547742875
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547743298
date2 = 1547743405
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547743576
date2 = 1547743763
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547743836
date2 = 1547745266
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547745340
date2 = 1547745568
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547745634
date2 = 1547745638
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547745699
date2 = 1547745859
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547746288
date2 = 1547755513
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547756654
date2 = 1547761228
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547762318
date2 = 1547762669
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547762742
date2 = 1547764258
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547764536
date2 = 1547764690
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547765017
date2 = 1547765036
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547765129
date2 = 1547765439
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547767353
date2 = 1547767353
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1547767722
date2 = 1547769539
value1 = 1.14028

value2 = 1.14028

</object>
<object>
color = 1918177
type = 2
date1 = 1548028800
date2 = 1548028800
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548028950
date2 = 1548029010
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548029105
date2 = 1548029459
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548029820
date2 = 1548029845
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548080079
date2 = 1548080147
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548080251
date2 = 1548080252
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548080316
date2 = 1548080334
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548080673
date2 = 1548080673
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548081693
date2 = 1548082020
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548082227
date2 = 1548082409
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548082994
date2 = 1548088767
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548091379
date2 = 1548091379
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548091526
date2 = 1548092608
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548092855
date2 = 1548092881
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548092971
date2 = 1548092991
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548093555
date2 = 1548093556
value1 = 1.13645

value2 = 1.13645

</object>
<object>
color = 1918177
type = 2
date1 = 1548324925
date2 = 1548325096
value1 = 1.13667

value2 = 1.13667

</object>
<object>
color = 1918177
type = 2
date1 = 1548325199
date2 = 1548325347
value1 = 1.13667

value2 = 1.13667

</object>
<object>
color = 1918177
type = 2
date1 = 1548325418
date2 = 1548325437
value1 = 1.13667

value2 = 1.13667

</object>
<object>
color = 1918177
type = 2
date1 = 1548325801
date2 = 1548325828
value1 = 1.13667

value2 = 1.13667

</object>
<object>
color = 1918177
type = 2
date1 = 1548329074
date2 = 1548335483
value1 = 1.13667

value2 = 1.13667

</object>
<object>
color = 1918177
type = 2
date1 = 1548335484
date2 = 1548335547
value1 = 1.13459

value2 = 1.13459

</object>
<object>
color = 1918177
type = 2
date1 = 1548335659
date2 = 1548335794
value1 = 1.13459

value2 = 1.13459

</object>
<object>
color = 1918177
type = 2
date1 = 1548343634
date2 = 1548343640
value1 = 1.13459

value2 = 1.13459

</object>
<object>
color = 1918177
type = 2
date1 = 1548343939
date2 = 1548344127
value1 = 1.13459

value2 = 1.13459

</object>
<object>
color = 1918177
type = 2
date1 = 1548344189
date2 = 1548344259
value1 = 1.13459

value2 = 1.13459

</object>
<object>
color = 1918177
type = 2
date1 = 1548344259
date2 = 1548344308
value1 = 1.13251

value2 = 1.13251

</object>
<object>
color = 1918177
type = 2
date1 = 1548357447
date2 = 1548364345
value1 = 1.13251

value2 = 1.13251

</object>
<object>
color = 1918177
type = 2
date1 = 1549299089
date2 = 1549299118
value1 = 1.14303

value2 = 1.14303

</object>
<object>
color = 1918177
type = 2
date1 = 1549299201
date2 = 1549299626
value1 = 1.14303

value2 = 1.14303

</object>
<object>
color = 1918177
type = 2
date1 = 1549303041
date2 = 1549303168
value1 = 1.14303

value2 = 1.14303

</object>
<object>
color = 1918177
type = 2
date1 = 1549303241
date2 = 1549303695
value1 = 1.14303

value2 = 1.14303

</object>
</window></chart>