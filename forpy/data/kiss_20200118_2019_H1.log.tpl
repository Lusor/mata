<chart>
id=132143875106490258
symbol=EURUSD
period_type=1
period_size=1
tester=1
digits=5
tick_size=0.000000
position_time=0
scale_fix=0
scale_fixed_min=1.117800
scale_fixed_max=1.141700
scale_fix11=0
scale_bar=0
scale_bar_val=1.000000
scale=4
mode=1
fore=0
grid=1
volume=0
scroll=0
shift=1
shift_size=20.103093
fixed_pos=0.000000
ohlc=1
one_click=0
one_click_btn=1
bidline=1
askline=0
lastline=0
days=0
descriptions=0
tradelines=1
window_left=0
window_top=0
window_right=0
window_bottom=0
window_type=3
floating=0
floating_left=0
floating_top=0
floating_right=0
floating_bottom=0
floating_type=1
floating_toolbar=1
floating_tbstate=
background_color=16777215
foreground_color=0
barup_color=0
bardown_color=0
bullcandle_color=16777215
bearcandle_color=0
chartline_color=0
volumes_color=32768
grid_color=12632256
bidline_color=12632256
askline_color=12632256
lastline_color=12632256
stops_color=17919
windows_total=1

<window>
height=100.000000
objects=3

<indicator>
name=Main
path=
apply=1
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=0
fixed_height=-1
</indicator>


<object>
type=32
name=autotrade  # 2 sell 0.88 EURUSD at 1.14077
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1546439317
value1=1.14077
</object>

<object>
type=32
name=autotrade  # 3 sell 0.89 EURUSD at 1.14077
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1546442140
value1=1.14077
</object>

<object>
type=32
name=autotrade  # 4 sell modified 0.89 EURUSD at 1.13788
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1546443699
value1=1.13788
</object>

<object>
type=32
name=autotrade  # 5 sell 0.90 EURUSD at 1.13824
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1546446103
value1=1.13824
</object>

<object>
type=32
name=autotrade  # 6 sell modified 0.90 EURUSD at 1.13585
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1546449698
value1=1.13585
</object>

<object>
type=31
name=autotrade  # 7 buy 0.91 EURUSD at 1.14166
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1546832656
value1=1.14166
</object>

<object>
type=31
name=autotrade  # 8 buy 0.93 EURUSD at 1.14166
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1546856434
value1=1.14166
</object>

<object>
type=31
name=autotrade  # 9 buy modified 0.93 EURUSD at 1.14404
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1546859315
value1=1.14404
</object>

<object>
type=31
name=autotrade  # 10 buy 0.94 EURUSD at 1.14166
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1546876115
value1=1.14166
</object>

<object>
type=31
name=autotrade  # 11 buy modified 0.94 EURUSD at 1.14588
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1546877258
value1=1.14588
</object>

<object>
type=31
name=autotrade  # 12 buy 0.96 EURUSD at 1.14557
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1546893694
value1=1.14557
</object>

<object>
type=31
name=autotrade  # 13 buy modified 0.96 EURUSD at 1.14808
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1546896161
value1=1.14808
</object>

<object>
type=31
name=autotrade  # 14 buy 1.00 EURUSD at 1.15232
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1547058937
value1=1.15232
</object>

<object>
type=32
name=autotrade  # 15 sell 1.04 EURUSD at 1.14792
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547424030
value1=1.14792
</object>

<object>
type=32
name=autotrade  # 16 sell 1.06 EURUSD at 1.14792
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547457876
value1=1.14792
</object>

<object>
type=32
name=autotrade  # 17 sell 1.08 EURUSD at 1.14792
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547469916
value1=1.14792
</object>

<object>
type=32
name=autotrade  # 18 sell 1.10 EURUSD at 1.14792
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547489429
value1=1.14792
</object>

<object>
type=32
name=autotrade  # 19 sell 1.11 EURUSD at 1.14792
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547506704
value1=1.14792
</object>

<object>
type=32
name=autotrade  # 20 sell 1.14 EURUSD at 1.14286
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547561320
value1=1.14286
</object>

<object>
type=32
name=autotrade  # 21 sell 1.16 EURUSD at 1.14286
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547575591
value1=1.14286
</object>

<object>
type=32
name=autotrade  # 22 sell modified 1.16 EURUSD at 1.13982
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1547576738
value1=1.13982
</object>

<object>
type=32
name=autotrade  # 23 sell 1.18 EURUSD at 1.13976
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547580184
value1=1.13976
</object>

<object>
type=32
name=autotrade  # 24 sell 1.20 EURUSD at 1.13990
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547647933
value1=1.13990
</object>

<object>
type=32
name=autotrade  # 25 sell 1.23 EURUSD at 1.14028
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1547711409
value1=1.14028
</object>

<object>
type=32
name=autotrade  # 26 sell modified 1.23 EURUSD at 1.13789
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1547716290
value1=1.13789
</object>

<object>
type=32
name=autotrade  # 27 sell 1.25 EURUSD at 1.13645
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1548028800
value1=1.13645
</object>

<object>
type=31
name=autotrade  # 28 buy 1.26 EURUSD at 1.13866
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1548267629
value1=1.13866
</object>

<object>
type=32
name=autotrade  # 29 sell 1.27 EURUSD at 1.13667
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1548324925
value1=1.13667
</object>

<object>
type=32
name=autotrade  # 30 sell 1.27 EURUSD at 1.13667
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1548330039
value1=1.13667
</object>

<object>
type=32
name=autotrade  # 31 sell modified 1.27 EURUSD at 1.13414
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1548333002
value1=1.13414
</object>

<object>
type=32
name=autotrade  # 32 sell 1.27 EURUSD at 1.13459
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1548344010
value1=1.13459
</object>

<object>
type=31
name=autotrade  # 33 buy 1.38 EURUSD at 1.13854
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1548633600
value1=1.13854
</object>

<object>
type=31
name=autotrade  # 34 buy modified 1.38 EURUSD at 1.14093
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1548637295
value1=1.14093
</object>

<object>
type=31
name=autotrade  # 35 buy 1.40 EURUSD at 1.13854
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1548654661
value1=1.13854
</object>

<object>
type=31
name=autotrade  # 36 buy modified 1.40 EURUSD at 1.14241
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1548694335
value1=1.14241
</object>

<object>
type=31
name=autotrade  # 37 buy 1.40 EURUSD at 1.14245
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1548709200
value1=1.14245
</object>

<object>
type=31
name=autotrade  # 38 buy 1.41 EURUSD at 1.14639
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1548882220
value1=1.14639
</object>

<object>
type=31
name=autotrade  # 39 buy 1.43 EURUSD at 1.14639
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1548883679
value1=1.14639
</object>

<object>
type=31
name=autotrade  # 40 buy modified 1.43 EURUSD at 1.14984
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1548884860
value1=1.14984
</object>

<object>
type=32
name=autotrade  # 41 sell 1.45 EURUSD at 1.14303
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549299089
value1=1.14303
</object>

<object>
type=32
name=autotrade  # 42 sell 1.48 EURUSD at 1.14160
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549390409
value1=1.14160
</object>

<object>
type=32
name=autotrade  # 43 sell 1.50 EURUSD at 1.13976
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549441002
value1=1.13976
</object>

<object>
type=32
name=autotrade  # 44 sell 1.52 EURUSD at 1.13976
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549462658
value1=1.13976
</object>

<object>
type=32
name=autotrade  # 45 sell modified 1.52 EURUSD at 1.13736
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1549483283
value1=1.13736
</object>

<object>
type=32
name=autotrade  # 46 sell 1.52 EURUSD at 1.13818
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549515600
value1=1.13818
</object>

<object>
type=32
name=autotrade  # 47 sell modified 1.52 EURUSD at 1.13577
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1549530204
value1=1.13577
</object>

<object>
type=32
name=autotrade  # 48 sell 1.55 EURUSD at 1.13610
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549541223
value1=1.13610
</object>

<object>
type=32
name=autotrade  # 49 sell modified 1.55 EURUSD at 1.13332
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1549546234
value1=1.13332
</object>

<object>
type=32
name=autotrade  # 50 sell 1.58 EURUSD at 1.13179
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549845274
value1=1.13179
</object>

<object>
type=32
name=autotrade  # 51 sell 1.61 EURUSD at 1.13179
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549887271
value1=1.13179
</object>

<object>
type=32
name=autotrade  # 52 sell 1.63 EURUSD at 1.13179
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549896276
value1=1.13179
</object>

<object>
type=32
name=autotrade  # 53 sell modified 1.63 EURUSD at 1.12939
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1549901960
value1=1.12939
</object>

<object>
type=32
name=autotrade  # 54 sell 1.67 EURUSD at 1.12788
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549908914
value1=1.12788
</object>

<object>
type=32
name=autotrade  # 55 sell 1.69 EURUSD at 1.12750
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1549967981
value1=1.12750
</object>

<object>
type=31
name=autotrade  # 56 buy 1.68 EURUSD at 1.13150
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1550016000
value1=1.13150
</object>

<object>
type=31
name=autotrade  # 57 buy modified 1.68 EURUSD at 1.13390
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1550035524
value1=1.13390
</object>

<object>
type=31
name=autotrade  # 58 buy 1.72 EURUSD at 1.13380
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1550621012
value1=1.13380
</object>

<object>
type=31
name=autotrade  # 59 buy 1.74 EURUSD at 1.13380
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1550631816
value1=1.13380
</object>

<object>
type=31
name=autotrade  # 60 buy 1.76 EURUSD at 1.13380
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1550658132
value1=1.13380
</object>

<object>
type=31
name=autotrade  # 61 buy 1.80 EURUSD at 1.13380
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1550686361
value1=1.13380
</object>

<object>
type=31
name=autotrade  # 62 buy modified 1.80 EURUSD at 1.13620
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1550687734
value1=1.13620
</object>

<object>
type=32
name=autotrade  # 63 sell 1.80 EURUSD at 1.13452
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1550736090
value1=1.13452
</object>

<object>
type=32
name=autotrade  # 64 sell modified 1.80 EURUSD at 1.13213
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1550740420
value1=1.13213
</object>

<object>
type=31
name=autotrade  # 65 buy 1.79 EURUSD at 1.13888
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1551217350
value1=1.13888
</object>

<object>
type=31
name=autotrade  # 66 buy 1.79 EURUSD at 1.13853
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1551274057
value1=1.13853
</object>

<object>
type=31
name=autotrade  # 67 buy 1.79 EURUSD at 1.13892
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1551367400
value1=1.13892
</object>

<object>
type=32
name=autotrade  # 68 sell 1.93 EURUSD at 1.13124
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1551806322
value1=1.13124
</object>

<object>
type=32
name=autotrade  # 69 sell 1.93 EURUSD at 1.12953
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1551886322
value1=1.12953
</object>

<object>
type=31
name=autotrade  # 70 buy 1.93 EURUSD at 1.12986
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1551923183
value1=1.12986
</object>

<object>
type=32
name=autotrade  # 71 sell 1.94 EURUSD at 1.12963
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1551969987
value1=1.12963
</object>

<object>
type=31
name=autotrade  # 72 buy 2.19 EURUSD at 1.12975
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1552494959
value1=1.12975
</object>

<object>
type=31
name=autotrade  # 73 buy modified 2.19 EURUSD at 1.13214
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1552510536
value1=1.13214
</object>

<object>
type=31
name=autotrade  # 74 buy 2.22 EURUSD at 1.13042
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1552558624
value1=1.13042
</object>

<object>
type=31
name=autotrade  # 75 buy 2.26 EURUSD at 1.13190
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1552867200
value1=1.13190
</object>

<object>
type=31
name=autotrade  # 76 buy modified 2.26 EURUSD at 1.13429
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1552899212
value1=1.13429
</object>

<object>
type=31
name=autotrade  # 77 buy 2.28 EURUSD at 1.13190
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1552916681
value1=1.13190
</object>

<object>
type=31
name=autotrade  # 78 buy modified 2.28 EURUSD at 1.13568
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1552917805
value1=1.13568
</object>

<object>
type=31
name=autotrade  # 79 buy 2.35 EURUSD at 1.13752
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1553112046
value1=1.13752
</object>

<object>
type=31
name=autotrade  # 80 buy modified 2.35 EURUSD at 1.14151
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1553113220
value1=1.14151
</object>

<object>
type=31
name=autotrade  # 81 buy 2.34 EURUSD at 1.13909
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1553126400
value1=1.13909
</object>

<object>
type=32
name=autotrade  # 82 sell 2.37 EURUSD at 1.12912
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553251661
value1=1.12912
</object>

<object>
type=32
name=autotrade  # 83 sell 2.41 EURUSD at 1.12912
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553273548
value1=1.12912
</object>

<object>
type=32
name=autotrade  # 84 sell 2.44 EURUSD at 1.12842
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553644800
value1=1.12842
</object>

<object>
type=32
name=autotrade  # 85 sell modified 2.44 EURUSD at 1.12602
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1553656773
value1=1.12602
</object>

<object>
type=32
name=autotrade  # 86 sell 2.48 EURUSD at 1.12589
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553705240
value1=1.12589
</object>

<object>
type=32
name=autotrade  # 87 sell 2.52 EURUSD at 1.12589
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553727967
value1=1.12589
</object>

<object>
type=32
name=autotrade  # 88 sell 2.57 EURUSD at 1.12575
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553769265
value1=1.12575
</object>

<object>
type=32
name=autotrade  # 89 sell 2.68 EURUSD at 1.12367
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553789070
value1=1.12367
</object>

<object>
type=32
name=autotrade  # 90 sell 2.71 EURUSD at 1.12367
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553796599
value1=1.12367
</object>

<object>
type=32
name=autotrade  # 91 sell 2.75 EURUSD at 1.12367
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1553812259
value1=1.12367
</object>

<object>
type=32
name=autotrade  # 92 sell 2.79 EURUSD at 1.12079
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1554145417
value1=1.12079
</object>

<object>
type=31
name=autotrade  # 93 buy 2.83 EURUSD at 1.12399
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1554291938
value1=1.12399
</object>

<object>
type=31
name=autotrade  # 94 buy 2.86 EURUSD at 1.12399
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1554296139
value1=1.12399
</object>

<object>
type=31
name=autotrade  # 95 buy 2.92 EURUSD at 1.12384
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1554746007
value1=1.12384
</object>

<object>
type=31
name=autotrade  # 96 buy modified 2.92 EURUSD at 1.12624
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1554769236
value1=1.12624
</object>

<object>
type=31
name=autotrade  # 97 buy 2.97 EURUSD at 1.12629
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1554812524
value1=1.12629
</object>

<object>
type=32
name=autotrade  # 98 sell 3.01 EURUSD at 1.12411
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1554912579
value1=1.12411
</object>

<object>
type=31
name=autotrade  # 99 buy 3.00 EURUSD at 1.12641
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1554948304
value1=1.12641
</object>

<object>
type=31
name=autotrade  # 100 buy 3.00 EURUSD at 1.12971
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1555286430
value1=1.12971
</object>

<object>
type=31
name=autotrade  # 101 buy 3.00 EURUSD at 1.12971
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1555340400
value1=1.12971
</object>

<object>
type=32
name=autotrade  # 102 sell 3.18 EURUSD at 1.12465
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1555622423
value1=1.12465
</object>

<object>
type=32
name=autotrade  # 103 sell 3.31 EURUSD at 1.12137
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1556039618
value1=1.12137
</object>

<object>
type=32
name=autotrade  # 104 sell 3.37 EURUSD at 1.12025
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1556103761
value1=1.12025
</object>

<object>
type=32
name=autotrade  # 105 sell 3.44 EURUSD at 1.12025
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1556123179
value1=1.12025
</object>

<object>
type=32
name=autotrade  # 106 sell 3.65 EURUSD at 1.11736
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1556170936
value1=1.11736
</object>

<object>
type=32
name=autotrade  # 107 sell modified 3.65 EURUSD at 1.11464
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556188633
value1=1.11464
</object>

<object>
type=31
name=autotrade  # 108 buy 3.70 EURUSD at 1.11600
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1556528611
value1=1.11600
</object>

<object>
type=31
name=autotrade  # 109 buy 3.78 EURUSD at 1.11600
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1556567017
value1=1.11600
</object>

<object>
type=31
name=autotrade  # 110 buy modified 3.78 EURUSD at 1.11839
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556572716
value1=1.11839
</object>

<object>
type=31
name=autotrade  # 111 buy 3.87 EURUSD at 1.11887
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1556630738
value1=1.11887
</object>

<object>
type=31
name=autotrade  # 112 buy modified 3.87 EURUSD at 1.12145
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556634871
value1=1.12145
</object>

<object>
type=31
name=autotrade  # 113 buy 3.91 EURUSD at 1.11887
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1556642921
value1=1.11887
</object>

<object>
type=31
name=autotrade  # 114 buy modified 3.91 EURUSD at 1.12128
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556668830
value1=1.12128
</object>

<object>
type=31
name=autotrade  # 115 buy 3.97 EURUSD at 1.12293
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1556713277
value1=1.12293
</object>

<object>
type=31
name=autotrade  # 116 buy 4.02 EURUSD at 1.12293
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1556730545
value1=1.12293
</object>

<object>
type=31
name=autotrade  # 117 buy modified 4.02 EURUSD at 1.12532
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556744415
value1=1.12532
</object>

<object>
type=32
name=autotrade  # 118 sell 4.04 EURUSD at 1.12183
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1556755200
value1=1.12183
</object>

<object>
type=32
name=autotrade  # 119 sell modified 4.04 EURUSD at 1.11943
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556791055
value1=1.11943
</object>

<object>
type=32
name=autotrade  # 120 sell 4.04 EURUSD at 1.11975
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1556815779
value1=1.11975
</object>

<object>
type=32
name=autotrade  # 121 sell modified 4.04 EURUSD at 1.11736
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1556821299
value1=1.11736
</object>

<object>
type=32
name=autotrade  # 122 sell 4.04 EURUSD at 1.11952
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1557400288
value1=1.11952
</object>

<object>
type=32
name=autotrade  # 123 sell 4.04 EURUSD at 1.12050
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1557933371
value1=1.12050
</object>

<object>
type=32
name=autotrade  # 124 sell 4.04 EURUSD at 1.12073
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1558023085
value1=1.12073
</object>

<object>
type=32
name=autotrade  # 125 sell modified 4.04 EURUSD at 1.11834
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1558026746
value1=1.11834
</object>

<object>
type=32
name=autotrade  # 126 sell 4.05 EURUSD at 1.11865
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1558045838
value1=1.11865
</object>

<object>
type=32
name=autotrade  # 127 sell modified 4.05 EURUSD at 1.11626
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1558096276
value1=1.11626
</object>

<object>
type=32
name=autotrade  # 128 sell 4.05 EURUSD at 1.11600
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1558598621
value1=1.11600
</object>

<object>
type=32
name=autotrade  # 129 sell modified 4.05 EURUSD at 1.11362
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1558605998
value1=1.11362
</object>

<object>
type=32
name=autotrade  # 130 sell 4.07 EURUSD at 1.11392
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1558629936
value1=1.11392
</object>

<object>
type=32
name=autotrade  # 131 sell 4.06 EURUSD at 1.11362
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1559154281
value1=1.11362
</object>

<object>
type=32
name=autotrade  # 132 sell 4.06 EURUSD at 1.11477
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1559235583
value1=1.11477
</object>

<object>
type=31
name=autotrade  # 133 buy 4.43 EURUSD at 1.12427
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559642312
value1=1.12427
</object>

<object>
type=31
name=autotrade  # 134 buy 4.52 EURUSD at 1.12427
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559658479
value1=1.12427
</object>

<object>
type=31
name=autotrade  # 135 buy 4.61 EURUSD at 1.12427
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559667441
value1=1.12427
</object>

<object>
type=31
name=autotrade  # 136 buy 4.71 EURUSD at 1.12427
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559683336
value1=1.12427
</object>

<object>
type=31
name=autotrade  # 137 buy modified 4.71 EURUSD at 1.12667
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1559725531
value1=1.12667
</object>

<object>
type=31
name=autotrade  # 138 buy 4.77 EURUSD at 1.12903
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559748321
value1=1.12903
</object>

<object>
type=32
name=autotrade  # 139 sell 4.80 EURUSD at 1.12389
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1559779200
value1=1.12389
</object>

<object>
type=31
name=autotrade  # 140 buy 4.97 EURUSD at 1.12796
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559836407
value1=1.12796
</object>

<object>
type=31
name=autotrade  # 141 buy 5.03 EURUSD at 1.12796
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559836579
value1=1.12796
</object>

<object>
type=31
name=autotrade  # 142 buy 5.11 EURUSD at 1.12796
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559836958
value1=1.12796
</object>

<object>
type=31
name=autotrade  # 143 buy 5.20 EURUSD at 1.12796
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1559847134
value1=1.12796
</object>

<object>
type=31
name=autotrade  # 144 buy 5.34 EURUSD at 1.13135
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1560124800
value1=1.13135
</object>

<object>
type=32
name=autotrade  # 145 sell 5.44 EURUSD at 1.13118
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1560384000
value1=1.13118
</object>

<object>
type=32
name=autotrade  # 146 sell modified 5.44 EURUSD at 1.12869
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1560434075
value1=1.12869
</object>

<object>
type=32
name=autotrade  # 147 sell 5.51 EURUSD at 1.12910
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1560445296
value1=1.12910
</object>

<object>
type=32
name=autotrade  # 148 sell 5.54 EURUSD at 1.12374
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1560527427
value1=1.12374
</object>

<object>
type=32
name=autotrade  # 149 sell 5.61 EURUSD at 1.12374
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1560535536
value1=1.12374
</object>

<object>
type=32
name=autotrade  # 150 sell modified 5.61 EURUSD at 1.12134
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1560539915
value1=1.12134
</object>

<object>
type=31
name=autotrade  # 151 buy 5.72 EURUSD at 1.11955
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1560988800
value1=1.11955
</object>

<object>
type=31
name=autotrade  # 152 buy modified 5.72 EURUSD at 1.12291
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1560992426
value1=1.12291
</object>

<object>
type=31
name=autotrade  # 153 buy modified 5.72 EURUSD at 1.12530
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1561001675
value1=1.12530
</object>

<object>
type=31
name=autotrade  # 154 buy 5.78 EURUSD at 1.12988
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1561092570
value1=1.12988
</object>

<object>
type=31
name=autotrade  # 155 buy 5.85 EURUSD at 1.12988
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1561112563
value1=1.12988
</object>

<object>
type=31
name=autotrade  # 156 buy 5.95 EURUSD at 1.12988
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1561125031
value1=1.12988
</object>

<object>
type=31
name=autotrade  # 157 buy modified 5.95 EURUSD at 1.13227
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1561132536
value1=1.13227
</object>

<object>
type=31
name=autotrade  # 158 buy 6.04 EURUSD at 1.13245
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1561144818
value1=1.13245
</object>

<object>
type=32
name=autotrade  # 159 sell 6.51 EURUSD at 1.13627
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1561521940
value1=1.13627
</object>

<object>
type=32
name=autotrade  # 160 sell 6.60 EURUSD at 1.13627
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1561567347
value1=1.13627
</object>

<object>
type=31
name=autotrade  # 161 buy 6.68 EURUSD at 1.13654
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1561639591
value1=1.13654
</object>

<object>
type=32
name=autotrade  # 162 sell 6.85 EURUSD at 1.13506
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1561956342
value1=1.13506
</object>

<object>
type=32
name=autotrade  # 163 sell 7.03 EURUSD at 1.13506
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1561973176
value1=1.13506
</object>

<object>
type=32
name=autotrade  # 164 sell modified 7.03 EURUSD at 1.13232
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1561975452
value1=1.13232
</object>

<object>
type=32
name=autotrade  # 165 sell 7.36 EURUSD at 1.12951
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562108105
value1=1.12951
</object>

<object>
type=32
name=autotrade  # 166 sell 7.47 EURUSD at 1.12925
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562198400
value1=1.12925
</object>

<object>
type=32
name=autotrade  # 167 sell 7.57 EURUSD at 1.12925
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562256034
value1=1.12925
</object>

<object>
type=32
name=autotrade  # 168 sell 7.61 EURUSD at 1.12235
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562344767
value1=1.12235
</object>

<object>
type=32
name=autotrade  # 169 sell 7.72 EURUSD at 1.12235
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562348900
value1=1.12235
</object>

<object>
type=32
name=autotrade  # 170 sell 7.83 EURUSD at 1.12235
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562350658
value1=1.12235
</object>

<object>
type=32
name=autotrade  # 171 sell 7.95 EURUSD at 1.12281
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1562613479
value1=1.12281
</object>

<object>
type=31
name=autotrade  # 172 buy 7.93 EURUSD at 1.12346
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1562772689
value1=1.12346
</object>

<object>
type=31
name=autotrade  # 173 buy modified 7.93 EURUSD at 1.12587
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1562784148
value1=1.12587
</object>

<object>
type=32
name=autotrade  # 174 sell 8.05 EURUSD at 1.12406
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1563283158
value1=1.12406
</object>

<object>
type=32
name=autotrade  # 175 sell modified 8.05 EURUSD at 1.12160
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1563291026
value1=1.12160
</object>

<object>
type=31
name=autotrade  # 176 buy 8.17 EURUSD at 1.12087
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1563408000
value1=1.12087
</object>

<object>
type=31
name=autotrade  # 177 buy modified 8.17 EURUSD at 1.12327
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1563422006
value1=1.12327
</object>

<object>
type=32
name=autotrade  # 178 sell 8.56 EURUSD at 1.11855
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1563893922
value1=1.11855
</object>

<object>
type=32
name=autotrade  # 179 sell modified 8.56 EURUSD at 1.11616
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1563899945
value1=1.11616
</object>

<object>
type=32
name=autotrade  # 180 sell 8.69 EURUSD at 1.11855
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1563925952
value1=1.11855
</object>

<object>
type=32
name=autotrade  # 181 sell modified 8.69 EURUSD at 1.11520
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1563928840
value1=1.11520
</object>

<object>
type=32
name=autotrade  # 182 sell 8.90 EURUSD at 1.11369
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1563965189
value1=1.11369
</object>

<object>
type=32
name=autotrade  # 183 sell 9.04 EURUSD at 1.11492
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1564052490
value1=1.11492
</object>

<object>
type=31
name=autotrade  # 184 buy 9.02 EURUSD at 1.11455
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1564065941
value1=1.11455
</object>

<object>
type=31
name=autotrade  # 185 buy 9.52 EURUSD at 1.11455
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1564070381
value1=1.11455
</object>

<object>
type=31
name=autotrade  # 186 buy modified 9.52 EURUSD at 1.11736
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1564073359
value1=1.11736
</object>

<object>
type=32
name=autotrade  # 187 sell 9.57 EURUSD at 1.11122
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1564606959
value1=1.11122
</object>

<object>
type=32
name=autotrade  # 188 sell 9.59 EURUSD at 1.10869
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1564609201
value1=1.10869
</object>

<object>
type=32
name=autotrade  # 189 sell 9.60 EURUSD at 1.10869
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1564609862
value1=1.10869
</object>

<object>
type=32
name=autotrade  # 190 sell 9.59 EURUSD at 1.10869
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1564611425
value1=1.10869
</object>

<object>
type=32
name=autotrade  # 191 sell 9.60 EURUSD at 1.10869
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1564613336
value1=1.10869
</object>

<object>
type=32
name=autotrade  # 192 sell modified 9.60 EURUSD at 1.10630
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1564614975
value1=1.10630
</object>

<object>
type=31
name=autotrade  # 193 buy 10.51 EURUSD at 1.11796
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1565025735
value1=1.11796
</object>

<object>
type=31
name=autotrade  # 194 buy 10.65 EURUSD at 1.11796
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1565030969
value1=1.11796
</object>

<object>
type=31
name=autotrade  # 195 buy modified 10.65 EURUSD at 1.12035
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1565034386
value1=1.12035
</object>

<object>
type=31
name=autotrade  # 196 buy 11.03 EURUSD at 1.12069
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1565064980
value1=1.12069
</object>

<object>
type=31
name=autotrade  # 197 buy 11.35 EURUSD at 1.12128
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1565263538
value1=1.12128
</object>

<object>
type=32
name=autotrade  # 198 sell 11.38 EURUSD at 1.12085
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1565275032
value1=1.12085
</object>

<object>
type=32
name=autotrade  # 199 sell modified 11.38 EURUSD at 1.11846
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1565279248
value1=1.11846
</object>

<object>
type=31
name=autotrade  # 200 buy 11.35 EURUSD at 1.12128
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1565285924
value1=1.12128
</object>

<object>
type=32
name=autotrade  # 201 sell 11.41 EURUSD at 1.11711
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1565606796
value1=1.11711
</object>

<object>
type=32
name=autotrade  # 202 sell 12.25 EURUSD at 1.11430
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1565807010
value1=1.11430
</object>

<object>
type=32
name=autotrade  # 203 sell 12.49 EURUSD at 1.11548
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1565887394
value1=1.11548
</object>

<object>
type=32
name=autotrade  # 204 sell modified 12.49 EURUSD at 1.11083
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1565889400
value1=1.11083
</object>

<object>
type=32
name=autotrade  # 205 sell 12.54 EURUSD at 1.10862
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1565953319
value1=1.10862
</object>

<object>
type=32
name=autotrade  # 206 sell 12.77 EURUSD at 1.10862
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1565970456
value1=1.10862
</object>

<object>
type=32
name=autotrade  # 207 sell 12.95 EURUSD at 1.10978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1566446187
value1=1.10978
</object>

<object>
type=31
name=autotrade  # 208 buy 12.92 EURUSD at 1.10984
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1566471977
value1=1.10984
</object>

<object>
type=32
name=autotrade  # 209 sell 12.95 EURUSD at 1.10978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1566476795
value1=1.10978
</object>

<object>
type=32
name=autotrade  # 210 sell modified 12.95 EURUSD at 1.10736
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1566481214
value1=1.10736
</object>

<object>
type=32
name=autotrade  # 211 sell 42.33 EURUSD at 1.10783
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1567015059
value1=1.10783
</object>

<object>
type=32
name=autotrade  # 212 sell 43.13 EURUSD at 1.10400
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1567157462
value1=1.10400
</object>

<object>
type=32
name=autotrade  # 213 sell 43.60 EURUSD at 1.10400
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1567160429
value1=1.10400
</object>

<object>
type=32
name=autotrade  # 214 sell 44.24 EURUSD at 1.10400
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1567187678
value1=1.10400
</object>

<object>
type=32
name=autotrade  # 215 sell modified 44.24 EURUSD at 1.09886
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1567190200
value1=1.09886
</object>

<object>
type=32
name=autotrade  # 216 sell 45.95 EURUSD at 1.10014
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1567199884
value1=1.10014
</object>

<object>
type=31
name=autotrade  # 217 buy 45.89 EURUSD at 1.09858
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567594178
value1=1.09858
</object>

<object>
type=31
name=autotrade  # 218 buy 46.43 EURUSD at 1.09858
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567596506
value1=1.09858
</object>

<object>
type=31
name=autotrade  # 219 buy modified 46.43 EURUSD at 1.10031
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1567598307
value1=1.10031
</object>

<object>
type=31
name=autotrade  # 220 buy modified 46.43 EURUSD at 1.10090
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1567598378
value1=1.10090
</object>

<object>
type=31
name=autotrade  # 221 buy 47.30 EURUSD at 1.10101
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567602073
value1=1.10101
</object>

<object>
type=31
name=autotrade  # 222 buy 47.84 EURUSD at 1.10101
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567609291
value1=1.10101
</object>

<object>
type=31
name=autotrade  # 223 buy 48.45 EURUSD at 1.10101
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567613104
value1=1.10101
</object>

<object>
type=31
name=autotrade  # 224 buy 49.83 EURUSD at 1.10101
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567618410
value1=1.10101
</object>

<object>
type=31
name=autotrade  # 225 buy modified 49.83 EURUSD at 1.10299
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1567620336
value1=1.10299
</object>

<object>
type=31
name=autotrade  # 226 buy 50.00 EURUSD at 1.10101
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567634232
value1=1.10101
</object>

<object>
type=31
name=autotrade  # 227 buy modified 50.00 EURUSD at 1.10324
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1567641600
value1=1.10324
</object>

<object>
type=31
name=autotrade  # 228 buy 50.00 EURUSD at 1.10731
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1567702595
value1=1.10731
</object>

<object>
type=32
name=autotrade  # 229 sell 50.00 EURUSD at 1.10139
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1568210016
value1=1.10139
</object>

<object>
type=32
name=autotrade  # 230 sell modified 50.00 EURUSD at 1.10079
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568211996
value1=1.10079
</object>

<object>
type=32
name=autotrade  # 231 sell modified 50.00 EURUSD at 1.10017
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568213431
value1=1.10017
</object>

<object>
type=32
name=autotrade  # 232 sell 50.00 EURUSD at 1.10139
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1568219459
value1=1.10139
</object>

<object>
type=32
name=autotrade  # 233 sell modified 50.00 EURUSD at 1.09959
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568221240
value1=1.09959
</object>

<object>
type=32
name=autotrade  # 234 sell modified 50.00 EURUSD at 1.09899
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568221744
value1=1.09899
</object>

<object>
type=31
name=autotrade  # 235 buy 50.00 EURUSD at 1.10864
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1568367041
value1=1.10864
</object>

<object>
type=31
name=autotrade  # 236 buy 50.00 EURUSD at 1.10864
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1568368722
value1=1.10864
</object>

<object>
type=31
name=autotrade  # 237 buy 50.00 EURUSD at 1.10864
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1568375215
value1=1.10864
</object>

<object>
type=32
name=autotrade  # 238 sell 50.00 EURUSD at 1.10283
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1568649876
value1=1.10283
</object>

<object>
type=32
name=autotrade  # 239 sell modified 50.00 EURUSD at 1.10207
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568652755
value1=1.10207
</object>

<object>
type=32
name=autotrade  # 240 sell modified 50.00 EURUSD at 1.10148
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568652999
value1=1.10148
</object>

<object>
type=32
name=autotrade  # 241 sell 50.00 EURUSD at 1.10283
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1568653925
value1=1.10283
</object>

<object>
type=32
name=autotrade  # 242 sell modified 50.00 EURUSD at 1.10056
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1568656380
value1=1.10056
</object>

<object>
type=32
name=autotrade  # 243 sell 50.00 EURUSD at 1.10004
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1568656547
value1=1.10004
</object>

<object>
type=32
name=autotrade  # 244 sell 50.00 EURUSD at 1.10119
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1568999980
value1=1.10119
</object>

<object>
type=32
name=autotrade  # 245 sell 50.00 EURUSD at 1.10119
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569002080
value1=1.10119
</object>

<object>
type=32
name=autotrade  # 246 sell 50.00 EURUSD at 1.10119
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569002561
value1=1.10119
</object>

<object>
type=32
name=autotrade  # 247 sell 50.00 EURUSD at 1.10119
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569005455
value1=1.10119
</object>

<object>
type=32
name=autotrade  # 248 sell 50.00 EURUSD at 1.09594
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569434165
value1=1.09594
</object>

<object>
type=32
name=autotrade  # 249 sell 50.00 EURUSD at 1.09594
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569435581
value1=1.09594
</object>

<object>
type=32
name=autotrade  # 250 sell modified 50.00 EURUSD at 1.09520
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1569437822
value1=1.09520
</object>

<object>
type=32
name=autotrade  # 251 sell modified 50.00 EURUSD at 1.09461
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1569438483
value1=1.09461
</object>

<object>
type=32
name=autotrade  # 252 sell 50.00 EURUSD at 1.09594
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569448506
value1=1.09594
</object>

<object>
type=32
name=autotrade  # 253 sell modified 50.00 EURUSD at 1.09437
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1569456179
value1=1.09437
</object>

<object>
type=32
name=autotrade  # 254 sell 50.00 EURUSD at 1.09205
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569545304
value1=1.09205
</object>

<object>
type=32
name=autotrade  # 255 sell 50.00 EURUSD at 1.09205
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569555510
value1=1.09205
</object>

<object>
type=32
name=autotrade  # 256 sell 50.00 EURUSD at 1.09205
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1569576394
value1=1.09205
</object>

<object>
type=31
name=autotrade  # 257 buy 50.00 EURUSD at 1.11245
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1571315919
value1=1.11245
</object>

<object>
type=31
name=autotrade  # 258 buy 50.00 EURUSD at 1.11402
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1571409136
value1=1.11402
</object>

<object>
type=31
name=autotrade  # 259 buy modified 50.00 EURUSD at 1.11460
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1571413474
value1=1.11460
</object>

<object>
type=31
name=autotrade  # 260 buy 50.00 EURUSD at 1.11402
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1571425261
value1=1.11402
</object>

<object>
type=31
name=autotrade  # 261 buy modified 50.00 EURUSD at 1.11563
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1571428168
value1=1.11563
</object>

<object>
type=31
name=autotrade  # 262 buy modified 50.00 EURUSD at 1.11621
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1571437383
value1=1.11621
</object>

<object>
type=31
name=autotrade  # 263 buy 50.00 EURUSD at 1.11402
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1571442940
value1=1.11402
</object>

<object>
type=32
name=autotrade  # 264 sell 50.00 EURUSD at 1.11159
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1571835444
value1=1.11159
</object>

<object>
type=32
name=autotrade  # 265 sell 50.00 EURUSD at 1.11159
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1571839452
value1=1.11159
</object>

<object>
type=32
name=autotrade  # 266 sell 50.00 EURUSD at 1.10914
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1572021941
value1=1.10914
</object>

<object>
type=32
name=autotrade  # 267 sell 50.00 EURUSD at 1.10914
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1572027747
value1=1.10914
</object>

<object>
type=32
name=autotrade  # 268 sell 50.00 EURUSD at 1.10914
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1572038497
value1=1.10914
</object>

<object>
type=31
name=autotrade  # 269 buy 50.00 EURUSD at 1.11256
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1572469299
value1=1.11256
</object>

<object>
type=31
name=autotrade  # 270 buy modified 50.00 EURUSD at 1.11476
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1572470710
value1=1.11476
</object>

<object>
type=32
name=autotrade  # 271 sell 50.00 EURUSD at 1.11014
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1572970096
value1=1.11014
</object>

<object>
type=32
name=autotrade  # 272 sell modified 50.00 EURUSD at 1.10886
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1572973214
value1=1.10886
</object>

<object>
type=32
name=autotrade  # 273 sell modified 50.00 EURUSD at 1.10818
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1572973283
value1=1.10818
</object>

<object>
type=32
name=autotrade  # 274 sell modified 50.00 EURUSD at 1.10759
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1572973317
value1=1.10759
</object>

<object>
type=32
name=autotrade  # 275 sell 50.00 EURUSD at 1.10732
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1572977255
value1=1.10732
</object>

<object>
type=32
name=autotrade  # 276 sell modified 50.00 EURUSD at 1.10674
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1572978782
value1=1.10674
</object>

<object>
type=32
name=autotrade  # 277 sell 50.00 EURUSD at 1.10339
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1573218636
value1=1.10339
</object>

<object>
type=32
name=autotrade  # 278 sell 50.00 EURUSD at 1.10339
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1573231719
value1=1.10339
</object>

<object>
type=32
name=autotrade  # 279 sell modified 50.00 EURUSD at 1.10236
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1573233997
value1=1.10236
</object>

<object>
type=31
name=autotrade  # 280 buy 50.00 EURUSD at 1.10297
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1573824435
value1=1.10297
</object>

<object>
type=31
name=autotrade  # 281 buy 50.00 EURUSD at 1.10297
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1573831901
value1=1.10297
</object>

<object>
type=31
name=autotrade  # 282 buy modified 50.00 EURUSD at 1.10355
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1573833364
value1=1.10355
</object>

<object>
type=31
name=autotrade  # 283 buy modified 50.00 EURUSD at 1.10416
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1573833931
value1=1.10416
</object>

<object>
type=31
name=autotrade  # 284 buy 50.00 EURUSD at 1.10297
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1573841519
value1=1.10297
</object>

<object>
type=31
name=autotrade  # 285 buy modified 50.00 EURUSD at 1.10519
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1573847954
value1=1.10519
</object>

<object>
type=31
name=autotrade  # 286 buy 50.00 EURUSD at 1.10841
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1574100998
value1=1.10841
</object>

<object>
type=32
name=autotrade  # 287 sell 50.00 EURUSD at 1.10499
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1574441185
value1=1.10499
</object>

<object>
type=32
name=autotrade  # 288 sell modified 50.00 EURUSD at 1.10437
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1574442964
value1=1.10437
</object>

<object>
type=32
name=autotrade  # 289 sell 50.00 EURUSD at 1.09897
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1575040205
value1=1.09897
</object>

<object>
type=31
name=autotrade  # 290 buy 50.00 EURUSD at 1.10554
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1575306042
value1=1.10554
</object>

<object>
type=31
name=autotrade  # 291 buy modified 50.00 EURUSD at 1.10701
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1575307453
value1=1.10701
</object>

<object>
type=31
name=autotrade  # 292 buy 50.00 EURUSD at 1.10994
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1575475200
value1=1.10994
</object>

<object>
type=31
name=autotrade  # 293 buy modified 50.00 EURUSD at 1.11063
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1575478827
value1=1.11063
</object>

<object>
type=31
name=autotrade  # 294 buy modified 50.00 EURUSD at 1.11133
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1575478841
value1=1.11133
</object>

<object>
type=32
name=autotrade  # 295 sell 50.00 EURUSD at 1.10645
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1575651660
value1=1.10645
</object>

<object>
type=31
name=autotrade  # 296 buy 50.00 EURUSD at 1.11554
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1576196851
value1=1.11554
</object>

<object>
type=31
name=autotrade  # 297 buy 50.00 EURUSD at 1.11110
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1577421633
value1=1.11110
</object>

<object>
type=31
name=autotrade  # 298 buy modified 50.00 EURUSD at 1.11168
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577423800
value1=1.11168
</object>

<object>
type=31
name=autotrade  # 299 buy modified 50.00 EURUSD at 1.11229
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577439330
value1=1.11229
</object>

<object>
type=31
name=autotrade  # 300 buy modified 50.00 EURUSD at 1.11287
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577440817
value1=1.11287
</object>

<object>
type=31
name=autotrade  # 301 buy modified 50.00 EURUSD at 1.11346
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577441321
value1=1.11346
</object>

<object>
type=31
name=autotrade  # 302 buy 50.00 EURUSD at 1.11110
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1577449404
value1=1.11110
</object>

<object>
type=31
name=autotrade  # 303 buy modified 50.00 EURUSD at 1.11399
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577450824
value1=1.11399
</object>

<object>
type=31
name=autotrade  # 304 buy modified 50.00 EURUSD at 1.11459
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577451581
value1=1.11459
</object>

<object>
type=31
name=autotrade  # 305 buy 50.00 EURUSD at 1.11110
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1577455838
value1=1.11110
</object>

<object>
type=31
name=autotrade  # 306 buy modified 50.00 EURUSD at 1.11520
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577458524
value1=1.11520
</object>

<object>
type=31
name=autotrade  # 307 buy 50.00 EURUSD at 1.11496
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1577461286
value1=1.11496
</object>

<object>
type=31
name=autotrade  # 308 buy modified 50.00 EURUSD at 1.11565
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577465006
value1=1.11565
</object>

<object>
type=31
name=autotrade  # 309 buy modified 50.00 EURUSD at 1.11624
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577466281
value1=1.11624
</object>

<object>
type=31
name=autotrade  # 310 buy 50.00 EURUSD at 1.11496
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1577469278
value1=1.11496
</object>

<object>
type=31
name=autotrade  # 311 buy modified 50.00 EURUSD at 1.11764
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577475605
value1=1.11764
</object>

<object>
type=31
name=autotrade  # 312 buy modified 50.00 EURUSD at 1.11822
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1577480614
value1=1.11822
</object>

<object>
type=31
name=autotrade  # 313 buy 50.00 EURUSD at 1.12143
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1577731575
value1=1.12143
</object>

</window></chart>