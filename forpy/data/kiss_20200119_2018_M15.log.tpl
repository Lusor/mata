<chart>
id=132143875106490258
symbol=EURUSD
period_type=1
period_size=1
tester=1
digits=5
tick_size=0.000000
position_time=0
scale_fix=0
scale_fixed_min=1.117800
scale_fixed_max=1.141700
scale_fix11=0
scale_bar=0
scale_bar_val=1.000000
scale=4
mode=1
fore=0
grid=1
volume=0
scroll=0
shift=1
shift_size=20.103093
fixed_pos=0.000000
ohlc=1
one_click=0
one_click_btn=1
bidline=1
askline=0
lastline=0
days=0
descriptions=0
tradelines=1
window_left=0
window_top=0
window_right=0
window_bottom=0
window_type=3
floating=0
floating_left=0
floating_top=0
floating_right=0
floating_bottom=0
floating_type=1
floating_toolbar=1
floating_tbstate=
background_color=16777215
foreground_color=0
barup_color=0
bardown_color=0
bullcandle_color=16777215
bearcandle_color=0
chartline_color=0
volumes_color=32768
grid_color=12632256
bidline_color=12632256
askline_color=12632256
lastline_color=12632256
stops_color=17919
windows_total=1

<window>
height=100.000000
objects=3

<indicator>
name=Main
path=
apply=1
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=0
fixed_height=-1
</indicator>


<object>
type=31
name=autotrade  # 2 buy 0.97 EURUSD at 1.20264
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1514887200
value1=1.20264
</object>

<object>
type=31
name=autotrade  # 3 buy modified 0.97 EURUSD at 1.20423
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1514892996
value1=1.20423
</object>

<object>
type=27
name=autotrade  # 4 order canceled 0.97 EURUSD at 1.20423
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1514893528
value1=1.20423
</object>

<object>
type=27
name=autotrade  # 5 order canceled 0.97 EURUSD at 1.20423
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1514893529
value1=1.20423
</object>

<object>
type=31
name=autotrade  # 6 buy 0.97 EURUSD at 1.20474
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1514937600
value1=1.20474
</object>

<object>
type=28
name=autotrade  # 7 stop loss triggered 0.97 EURUSD at 1.20602
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515078928
value1=1.20602
</object>

<object>
type=32
name=autotrade  # 8 sell 0.98 EURUSD at 1.20274
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515173980
value1=1.20274
</object>

<object>
type=28
name=autotrade  # 9 stop loss triggered 0.98 EURUSD at 1.20205
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515398961
value1=1.20205
</object>

<object>
type=32
name=autotrade  # 10 sell 0.99 EURUSD at 1.20373
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515398961
value1=1.20373
</object>

<object>
type=32
name=autotrade  # 11 sell modified 0.99 EURUSD at 1.20215
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1515402087
value1=1.20215
</object>

<object>
type=32
name=autotrade  # 12 sell modified 0.99 EURUSD at 1.20057
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1515405460
value1=1.20057
</object>

<object>
type=28
name=autotrade  # 13 stop loss triggered 0.99 EURUSD at 1.19575
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515587911
value1=1.19575
</object>

<object>
type=31
name=autotrade  # 14 buy 1.06 EURUSD at 1.19820
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1515588342
value1=1.19820
</object>

<object>
type=31
name=autotrade  # 15 buy modified 1.06 EURUSD at 1.19978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1515589410
value1=1.19978
</object>

<object>
type=28
name=autotrade  # 16 stop loss triggered 1.06 EURUSD at 1.20064
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515592199
value1=1.20064
</object>

<object>
type=31
name=autotrade  # 17 buy 1.07 EURUSD at 1.19982
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1515592288
value1=1.19982
</object>

<object>
type=28
name=autotrade  # 18 stop loss triggered 1.07 EURUSD at 1.20047
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515682717
value1=1.20047
</object>

<object>
type=31
name=autotrade  # 19 buy 1.08 EURUSD at 1.20062
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1515682899
value1=1.20062
</object>

<object>
type=28
name=autotrade  # 20 stop loss triggered 1.08 EURUSD at 1.20149
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515683312
value1=1.20149
</object>

<object>
type=31
name=autotrade  # 21 buy 1.09 EURUSD at 1.20062
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1515683312
value1=1.20062
</object>

<object>
type=31
name=autotrade  # 22 buy modified 1.09 EURUSD at 1.20221
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1515684641
value1=1.20221
</object>

<object>
type=28
name=autotrade  # 23 stop loss triggered 1.09 EURUSD at 1.20325
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515687821
value1=1.20325
</object>

<object>
type=31
name=autotrade  # 24 buy 1.10 EURUSD at 1.20062
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1515687821
value1=1.20062
</object>

<object>
type=31
name=autotrade  # 25 buy modified 1.10 EURUSD at 1.20340
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1515689641
value1=1.20340
</object>

<object>
type=27
name=autotrade  # 26 order canceled 1.10 EURUSD at 1.20340
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1515689645
value1=1.20340
</object>

<object>
type=31
name=autotrade  # 27 buy 1.08 EURUSD at 1.22516
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1516148426
value1=1.22516
</object>

<object>
type=31
name=autotrade  # 28 buy modified 1.08 EURUSD at 1.22674
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1516156953
value1=1.22674
</object>

<object>
type=31
name=autotrade  # 29 buy modified 1.08 EURUSD at 1.22832
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1516157224
value1=1.22832
</object>

<object>
type=31
name=autotrade  # 30 buy modified 1.08 EURUSD at 1.22991
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1516157248
value1=1.22991
</object>

<object>
type=27
name=autotrade  # 31 order canceled 1.08 EURUSD at 1.22991
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516157318
value1=1.22991
</object>

<object>
type=32
name=autotrade  # 32 sell 1.08 EURUSD at 1.22416
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516320082
value1=1.22416
</object>

<object>
type=28
name=autotrade  # 33 stop loss triggered 1.08 EURUSD at 1.22308
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516385490
value1=1.22308
</object>

<object>
type=31
name=autotrade  # 34 buy 1.10 EURUSD at 1.22660
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1516674747
value1=1.22660
</object>

<object>
type=28
name=autotrade  # 35 stop loss triggered 1.10 EURUSD at 1.22855
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516731000
value1=1.22855
</object>

<object>
type=31
name=autotrade  # 36 buy 1.12 EURUSD at 1.22877
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1516731255
value1=1.22877
</object>

<object>
type=28
name=autotrade  # 37 stop loss triggered 1.12 EURUSD at 1.23008
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516761207
value1=1.23008
</object>

<object>
type=31
name=autotrade  # 38 buy 1.14 EURUSD at 1.22990
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1516765590
value1=1.22990
</object>

<object>
type=31
name=autotrade  # 39 buy modified 1.14 EURUSD at 1.23148
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1516773991
value1=1.23148
</object>

<object>
type=28
name=autotrade  # 40 stop loss triggered 1.14 EURUSD at 1.23218
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516789061
value1=1.23218
</object>

<object>
type=31
name=autotrade  # 41 buy 1.15 EURUSD at 1.23152
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1516789122
value1=1.23152
</object>

<object>
type=28
name=autotrade  # 42 stop loss triggered 1.15 EURUSD at 1.23349
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1516795896
value1=1.23349
</object>

<object>
type=32
name=autotrade  # 43 sell 1.17 EURUSD at 1.23836
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517270400
value1=1.23836
</object>

<object>
type=28
name=autotrade  # 44 stop loss triggered 1.17 EURUSD at 1.23766
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517281201
value1=1.23766
</object>

<object>
type=32
name=autotrade  # 45 sell 1.18 EURUSD at 1.23836
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517281201
value1=1.23836
</object>

<object>
type=28
name=autotrade  # 46 stop loss triggered 1.18 EURUSD at 1.23725
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517289003
value1=1.23725
</object>

<object>
type=31
name=autotrade  # 47 buy 1.19 EURUSD at 1.24505
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1517328098
value1=1.24505
</object>

<object>
type=28
name=autotrade  # 48 stop loss triggered 1.19 EURUSD at 1.24612
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517414544
value1=1.24612
</object>

<object>
type=31
name=autotrade  # 49 buy 1.21 EURUSD at 1.24443
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1517483262
value1=1.24443
</object>

<object>
type=28
name=autotrade  # 50 stop loss triggered 1.21 EURUSD at 1.24514
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517497037
value1=1.24514
</object>

<object>
type=31
name=autotrade  # 51 buy 1.22 EURUSD at 1.24443
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1517497037
value1=1.24443
</object>

<object>
type=28
name=autotrade  # 52 stop loss triggered 1.22 EURUSD at 1.24849
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517571014
value1=1.24849
</object>

<object>
type=32
name=autotrade  # 53 sell 1.28 EURUSD at 1.24445
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517788800
value1=1.24445
</object>

<object>
type=28
name=autotrade  # 54 stop loss triggered 1.28 EURUSD at 1.24223
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517851542
value1=1.24223
</object>

<object>
type=32
name=autotrade  # 55 sell 1.32 EURUSD at 1.24159
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517853810
value1=1.24159
</object>

<object>
type=28
name=autotrade  # 56 stop loss triggered 1.32 EURUSD at 1.24050
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517868327
value1=1.24050
</object>

<object>
type=32
name=autotrade  # 57 sell 1.35 EURUSD at 1.23876
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517961604
value1=1.23876
</object>

<object>
type=28
name=autotrade  # 58 stop loss triggered 1.35 EURUSD at 1.23756
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517999617
value1=1.23756
</object>

<object>
type=32
name=autotrade  # 59 sell 1.37 EURUSD at 1.23876
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1517999617
value1=1.23876
</object>

<object>
type=28
name=autotrade  # 60 stop loss triggered 1.37 EURUSD at 1.23788
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1518001231
value1=1.23788
</object>

<object>
type=32
name=autotrade  # 61 sell 1.38 EURUSD at 1.23876
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1518001410
value1=1.23876
</object>

<object>
type=32
name=autotrade  # 62 sell modified 1.38 EURUSD at 1.23715
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1518002501
value1=1.23715
</object>

<object>
type=32
name=autotrade  # 63 sell modified 1.38 EURUSD at 1.23557
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1518006579
value1=1.23557
</object>

<object>
type=32
name=autotrade  # 64 sell modified 1.38 EURUSD at 1.23395
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1518019637
value1=1.23395
</object>

<object>
type=28
name=autotrade  # 65 stop loss triggered 1.38 EURUSD at 1.22890
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1518081158
value1=1.22890
</object>

<object>
type=31
name=autotrade  # 66 buy 1.49 EURUSD at 1.22851
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1518480000
value1=1.22851
</object>

<object>
type=31
name=autotrade  # 67 buy 1.66 EURUSD at 1.23581
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1518583979
value1=1.23581
</object>

<object>
type=31
name=autotrade  # 68 buy modified 1.66 EURUSD at 1.23764
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1518586182
value1=1.23764
</object>

<object>
type=28
name=autotrade  # 69 stop loss triggered 1.66 EURUSD at 1.23869
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1518630460
value1=1.23869
</object>

<object>
type=31
name=autotrade  # 70 buy 1.68 EURUSD at 1.23743
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1518630460
value1=1.23743
</object>

<object>
type=27
name=autotrade  # 71 order canceled 1.68 EURUSD at 1.23743
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1518630582
value1=1.23743
</object>

<object>
type=27
name=autotrade  # 72 order canceled 1.68 EURUSD at 1.23743
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1518630583
value1=1.23743
</object>

<object>
type=32
name=autotrade  # 73 sell 1.68 EURUSD at 1.24038
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519090159
value1=1.24038
</object>

<object>
type=28
name=autotrade  # 74 stop loss triggered 1.68 EURUSD at 1.23951
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519100034
value1=1.23951
</object>

<object>
type=32
name=autotrade  # 75 sell 1.70 EURUSD at 1.24038
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519100034
value1=1.24038
</object>

<object>
type=32
name=autotrade  # 76 sell modified 1.70 EURUSD at 1.23880
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1519113880
value1=1.23880
</object>

<object>
type=28
name=autotrade  # 77 stop loss triggered 1.70 EURUSD at 1.23808
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519118562
value1=1.23808
</object>

<object>
type=32
name=autotrade  # 78 sell 1.72 EURUSD at 1.23821
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519118562
value1=1.23821
</object>

<object>
type=28
name=autotrade  # 79 stop loss triggered 1.72 EURUSD at 1.23644
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519123237
value1=1.23644
</object>

<object>
type=32
name=autotrade  # 80 sell 1.77 EURUSD at 1.23504
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519171200
value1=1.23504
</object>

<object>
type=32
name=autotrade  # 81 sell modified 1.77 EURUSD at 1.23345
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1519191640
value1=1.23345
</object>

<object>
type=28
name=autotrade  # 82 stop loss triggered 1.77 EURUSD at 1.23228
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519206637
value1=1.23228
</object>

<object>
type=32
name=autotrade  # 83 sell 1.79 EURUSD at 1.23342
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519206637
value1=1.23342
</object>

<object>
type=28
name=autotrade  # 84 stop loss triggered 1.79 EURUSD at 1.23248
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519217525
value1=1.23248
</object>

<object>
type=32
name=autotrade  # 85 sell 1.82 EURUSD at 1.23180
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519224016
value1=1.23180
</object>

<object>
type=28
name=autotrade  # 86 stop loss triggered 1.82 EURUSD at 1.23115
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519238917
value1=1.23115
</object>

<object>
type=32
name=autotrade  # 87 sell 1.84 EURUSD at 1.23180
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519249841
value1=1.23180
</object>

<object>
type=32
name=autotrade  # 88 sell modified 1.84 EURUSD at 1.23017
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1519250732
value1=1.23017
</object>

<object>
type=27
name=autotrade  # 89 order canceled 1.84 EURUSD at 1.23017
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519250799
value1=1.23017
</object>

<object>
type=32
name=autotrade  # 90 sell 1.84 EURUSD at 1.22978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519257600
value1=1.22978
</object>

<object>
type=32
name=autotrade  # 91 sell modified 1.84 EURUSD at 1.22820
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1519265134
value1=1.22820
</object>

<object>
type=28
name=autotrade  # 92 stop loss triggered 1.84 EURUSD at 1.22714
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519292404
value1=1.22714
</object>

<object>
type=32
name=autotrade  # 93 sell 1.87 EURUSD at 1.22978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519292404
value1=1.22978
</object>

<object>
type=28
name=autotrade  # 94 stop loss triggered 1.87 EURUSD at 1.22896
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519307857
value1=1.22896
</object>

<object>
type=32
name=autotrade  # 95 sell 1.88 EURUSD at 1.22978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519307857
value1=1.22978
</object>

<object>
type=28
name=autotrade  # 96 stop loss triggered 1.88 EURUSD at 1.22899
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519310191
value1=1.22899
</object>

<object>
type=32
name=autotrade  # 97 sell 1.90 EURUSD at 1.22978
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519310191
value1=1.22978
</object>

<object>
type=28
name=autotrade  # 98 stop loss triggered 1.90 EURUSD at 1.22897
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1519379176
value1=1.22897
</object>

<object>
type=31
name=autotrade  # 99 buy 1.92 EURUSD at 1.23483
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1519649008
value1=1.23483
</object>

<object>
type=28
name=autotrade  # 100 stop loss triggered 1.92 EURUSD at 1.23556
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520209157
value1=1.23556
</object>

<object>
type=31
name=autotrade  # 101 buy 1.91 EURUSD at 1.23394
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1520209157
value1=1.23394
</object>

<object>
type=28
name=autotrade  # 102 stop loss triggered 1.91 EURUSD at 1.23472
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520304087
value1=1.23472
</object>

<object>
type=31
name=autotrade  # 103 buy 1.93 EURUSD at 1.23532
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1520305297
value1=1.23532
</object>

<object>
type=28
name=autotrade  # 104 stop loss triggered 1.93 EURUSD at 1.24026
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520431240
value1=1.24026
</object>

<object>
type=32
name=autotrade  # 105 sell 2.05 EURUSD at 1.23737
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520515746
value1=1.23737
</object>

<object>
type=32
name=autotrade  # 106 sell 2.31 EURUSD at 1.23318
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520812860
value1=1.23318
</object>

<object>
type=28
name=autotrade  # 107 stop loss triggered 2.31 EURUSD at 1.23118
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520870477
value1=1.23118
</object>

<object>
type=31
name=autotrade  # 108 buy 2.37 EURUSD at 1.23352
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1520899298
value1=1.23352
</object>

<object>
type=28
name=autotrade  # 109 stop loss triggered 2.37 EURUSD at 1.23425
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520945177
value1=1.23425
</object>

<object>
type=31
name=autotrade  # 110 buy 2.39 EURUSD at 1.23352
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1520945177
value1=1.23352
</object>

<object>
type=28
name=autotrade  # 111 stop loss triggered 2.39 EURUSD at 1.23597
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1520954802
value1=1.23597
</object>

<object>
type=31
name=autotrade  # 112 buy 2.46 EURUSD at 1.23716
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1520985600
value1=1.23716
</object>

<object>
type=31
name=autotrade  # 113 buy modified 2.46 EURUSD at 1.23876
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1520993763
value1=1.23876
</object>

<object>
type=28
name=autotrade  # 114 stop loss triggered 2.46 EURUSD at 1.24015
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522069857
value1=1.24015
</object>

<object>
type=31
name=autotrade  # 115 buy 2.44 EURUSD at 1.24064
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1522070309
value1=1.24064
</object>

<object>
type=28
name=autotrade  # 116 stop loss triggered 2.44 EURUSD at 1.24420
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522149166
value1=1.24420
</object>

<object>
type=32
name=autotrade  # 117 sell 2.55 EURUSD at 1.24111
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522227719
value1=1.24111
</object>

<object>
type=32
name=autotrade  # 118 sell modified 2.55 EURUSD at 1.23950
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1522229622
value1=1.23950
</object>

<object>
type=28
name=autotrade  # 119 stop loss triggered 2.55 EURUSD at 1.23863
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522232920
value1=1.23863
</object>

<object>
type=32
name=autotrade  # 120 sell 2.58 EURUSD at 1.23949
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522232925
value1=1.23949
</object>

<object>
type=28
name=autotrade  # 121 stop loss triggered 2.58 EURUSD at 1.23866
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522252577
value1=1.23866
</object>

<object>
type=32
name=autotrade  # 122 sell 2.61 EURUSD at 1.23949
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522252720
value1=1.23949
</object>

<object>
type=32
name=autotrade  # 123 sell modified 2.61 EURUSD at 1.23739
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1522254099
value1=1.23739
</object>

<object>
type=27
name=autotrade  # 124 order canceled 2.61 EURUSD at 1.23739
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522254441
value1=1.23739
</object>

<object>
type=27
name=autotrade  # 125 order canceled 2.61 EURUSD at 1.23739
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522254442
value1=1.23739
</object>

<object>
type=32
name=autotrade  # 126 sell 2.63 EURUSD at 1.23346
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522281720
value1=1.23346
</object>

<object>
type=32
name=autotrade  # 127 sell modified 2.63 EURUSD at 1.23147
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1522292846
value1=1.23147
</object>

<object>
type=28
name=autotrade  # 128 stop loss triggered 2.63 EURUSD at 1.23056
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522320996
value1=1.23056
</object>

<object>
type=32
name=autotrade  # 129 sell 2.66 EURUSD at 1.22970
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522723194
value1=1.22970
</object>

<object>
type=28
name=autotrade  # 130 stop loss triggered 2.66 EURUSD at 1.22760
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522778475
value1=1.22760
</object>

<object>
type=32
name=autotrade  # 131 sell 2.74 EURUSD at 1.22753
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522778490
value1=1.22753
</object>

<object>
type=28
name=autotrade  # 132 stop loss triggered 2.74 EURUSD at 1.22672
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522783658
value1=1.22672
</object>

<object>
type=32
name=autotrade  # 133 sell 2.77 EURUSD at 1.22753
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522783658
value1=1.22753
</object>

<object>
type=28
name=autotrade  # 134 stop loss triggered 2.77 EURUSD at 1.22680
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522836398
value1=1.22680
</object>

<object>
type=32
name=autotrade  # 135 sell 2.80 EURUSD at 1.22764
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522836435
value1=1.22764
</object>

<object>
type=28
name=autotrade  # 136 stop loss triggered 2.80 EURUSD at 1.22633
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522923875
value1=1.22633
</object>

<object>
type=32
name=autotrade  # 137 sell 2.86 EURUSD at 1.22389
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522950077
value1=1.22389
</object>

<object>
type=28
name=autotrade  # 138 stop loss triggered 2.86 EURUSD at 1.22322
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522961340
value1=1.22322
</object>

<object>
type=32
name=autotrade  # 139 sell 2.88 EURUSD at 1.22389
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1522961348
value1=1.22389
</object>

<object>
type=28
name=autotrade  # 140 stop loss triggered 2.88 EURUSD at 1.22274
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1523012020
value1=1.22274
</object>

<object>
type=31
name=autotrade  # 141 buy 2.91 EURUSD at 1.22740
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1523232000
value1=1.22740
</object>

<object>
type=31
name=autotrade  # 142 buy 3.25 EURUSD at 1.23416
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1523404830
value1=1.23416
</object>

<object>
type=31
name=autotrade  # 143 buy modified 3.25 EURUSD at 1.23574
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1523434360
value1=1.23574
</object>

<object>
type=31
name=autotrade  # 144 buy modified 3.25 EURUSD at 1.23737
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1523442220
value1=1.23737
</object>

<object>
type=28
name=autotrade  # 145 stop loss triggered 3.25 EURUSD at 1.23806
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1523457005
value1=1.23806
</object>

<object>
type=31
name=autotrade  # 146 buy 3.27 EURUSD at 1.23740
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1523457365
value1=1.23740
</object>

<object>
type=28
name=autotrade  # 147 stop loss triggered 3.27 EURUSD at 1.23844
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1523460508
value1=1.23844
</object>

<object>
type=31
name=autotrade  # 148 buy 3.32 EURUSD at 1.23740
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1523460508
value1=1.23740
</object>

<object>
type=28
name=autotrade  # 149 stop loss triggered 3.32 EURUSD at 1.23829
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1523469451
value1=1.23829
</object>

<object>
type=31
name=autotrade  # 150 buy 3.35 EURUSD at 1.23740
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1523469616
value1=1.23740
</object>

<object>
type=28
name=autotrade  # 151 stop loss triggered 3.35 EURUSD at 1.23838
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1523896829
value1=1.23838
</object>

<object>
type=31
name=autotrade  # 152 buy 3.37 EURUSD at 1.23808
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1523896864
value1=1.23808
</object>

<object>
type=28
name=autotrade  # 153 stop loss triggered 3.37 EURUSD at 1.23964
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1523962948
value1=1.23964
</object>

<object>
type=32
name=autotrade  # 154 sell 3.44 EURUSD at 1.23622
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524046216
value1=1.23622
</object>

<object>
type=28
name=autotrade  # 155 stop loss triggered 3.44 EURUSD at 1.23526
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524053504
value1=1.23526
</object>

<object>
type=32
name=autotrade  # 156 sell 3.48 EURUSD at 1.23622
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524053558
value1=1.23622
</object>

<object>
type=28
name=autotrade  # 157 stop loss triggered 3.48 EURUSD at 1.23462
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524167819
value1=1.23462
</object>

<object>
type=32
name=autotrade  # 158 sell 3.57 EURUSD at 1.23605
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524182520
value1=1.23605
</object>

<object>
type=32
name=autotrade  # 159 sell modified 3.57 EURUSD at 1.23439
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1524215681
value1=1.23439
</object>

<object>
type=32
name=autotrade  # 160 sell 4.03 EURUSD at 1.22961
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524441600
value1=1.22961
</object>

<object>
type=32
name=autotrade  # 161 sell modified 4.03 EURUSD at 1.22801
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1524452858
value1=1.22801
</object>

<object>
type=32
name=autotrade  # 162 sell 4.56 EURUSD at 1.22289
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524641721
value1=1.22289
</object>

<object>
type=32
name=autotrade  # 163 sell modified 4.56 EURUSD at 1.22128
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1524650970
value1=1.22128
</object>

<object>
type=32
name=autotrade  # 164 sell modified 4.56 EURUSD at 1.21968
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1524654016
value1=1.21968
</object>

<object>
type=28
name=autotrade  # 165 stop loss triggered 4.56 EURUSD at 1.21864
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524669195
value1=1.21864
</object>

<object>
type=32
name=autotrade  # 166 sell 4.64 EURUSD at 1.21965
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524669203
value1=1.21965
</object>

<object>
type=32
name=autotrade  # 167 sell modified 4.64 EURUSD at 1.21804
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1524670839
value1=1.21804
</object>

<object>
type=28
name=autotrade  # 168 stop loss triggered 4.64 EURUSD at 1.21706
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524708873
value1=1.21706
</object>

<object>
type=32
name=autotrade  # 169 sell 4.71 EURUSD at 1.21959
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524708873
value1=1.21959
</object>

<object>
type=32
name=autotrade  # 170 sell modified 4.71 EURUSD at 1.21698
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1524739983
value1=1.21698
</object>

<object>
type=28
name=autotrade  # 171 stop loss triggered 4.71 EURUSD at 1.21621
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524754900
value1=1.21621
</object>

<object>
type=32
name=autotrade  # 172 sell 4.77 EURUSD at 1.21573
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524756903
value1=1.21573
</object>

<object>
type=28
name=autotrade  # 173 stop loss triggered 4.77 EURUSD at 1.21279
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1524772708
value1=1.21279
</object>

<object>
type=32
name=autotrade  # 174 sell 4.98 EURUSD at 1.21054
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525089360
value1=1.21054
</object>

<object>
type=28
name=autotrade  # 175 stop loss triggered 4.98 EURUSD at 1.20977
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525096714
value1=1.20977
</object>

<object>
type=32
name=autotrade  # 176 sell 5.03 EURUSD at 1.21054
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525096714
value1=1.21054
</object>

<object>
type=32
name=autotrade  # 177 sell modified 5.03 EURUSD at 1.20896
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1525098814
value1=1.20896
</object>

<object>
type=28
name=autotrade  # 178 stop loss triggered 5.03 EURUSD at 1.20790
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525107929
value1=1.20790
</object>

<object>
type=32
name=autotrade  # 179 sell 5.11 EURUSD at 1.20768
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525109859
value1=1.20768
</object>

<object>
type=32
name=autotrade  # 180 sell 5.80 EURUSD at 1.20046
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525219204
value1=1.20046
</object>

<object>
type=28
name=autotrade  # 181 stop loss triggered 5.80 EURUSD at 1.19980
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525252779
value1=1.19980
</object>

<object>
type=32
name=autotrade  # 182 sell 5.86 EURUSD at 1.20046
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525261779
value1=1.20046
</object>

<object>
type=28
name=autotrade  # 183 stop loss triggered 5.86 EURUSD at 1.19793
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525294947
value1=1.19793
</object>

<object>
type=32
name=autotrade  # 184 sell 6.08 EURUSD at 1.19687
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525305600
value1=1.19687
</object>

<object>
type=28
name=autotrade  # 185 stop loss triggered 6.08 EURUSD at 1.19595
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525370800
value1=1.19595
</object>

<object>
type=32
name=autotrade  # 186 sell 6.15 EURUSD at 1.19687
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525370800
value1=1.19687
</object>

<object>
type=28
name=autotrade  # 187 stop loss triggered 6.15 EURUSD at 1.19615
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525448490
value1=1.19615
</object>

<object>
type=32
name=autotrade  # 188 sell 6.21 EURUSD at 1.19667
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525448490
value1=1.19667
</object>

<object>
type=28
name=autotrade  # 189 stop loss triggered 6.21 EURUSD at 1.19391
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525453538
value1=1.19391
</object>

<object>
type=32
name=autotrade  # 190 sell 6.46 EURUSD at 1.19441
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525684109
value1=1.19441
</object>

<object>
type=28
name=autotrade  # 191 stop loss triggered 6.46 EURUSD at 1.19337
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525689540
value1=1.19337
</object>

<object>
type=32
name=autotrade  # 192 sell 6.56 EURUSD at 1.19441
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525689540
value1=1.19441
</object>

<object>
type=32
name=autotrade  # 193 sell modified 6.56 EURUSD at 1.19283
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1525701564
value1=1.19283
</object>

<object>
type=32
name=autotrade  # 194 sell modified 6.56 EURUSD at 1.19125
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1525704433
value1=1.19125
</object>

<object>
type=28
name=autotrade  # 195 stop loss triggered 6.56 EURUSD at 1.18744
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525806689
value1=1.18744
</object>

<object>
type=32
name=autotrade  # 196 sell 6.94 EURUSD at 1.18851
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1525824000
value1=1.18851
</object>

<object>
type=32
name=autotrade  # 197 sell modified 6.94 EURUSD at 1.18669
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1525838159
value1=1.18669
</object>

<object>
type=32
name=autotrade  # 198 sell modified 6.94 EURUSD at 1.18511
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1525857031
value1=1.18511
</object>

<object>
type=32
name=autotrade  # 199 sell modified 6.94 EURUSD at 1.18352
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1525861118
value1=1.18352
</object>

<object>
type=28
name=autotrade  # 200 stop loss triggered 6.94 EURUSD at 1.18283
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526403506
value1=1.18283
</object>

<object>
type=32
name=autotrade  # 201 sell 7.06 EURUSD at 1.18528
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526428800
value1=1.18528
</object>

<object>
type=32
name=autotrade  # 202 sell modified 7.06 EURUSD at 1.18370
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1526433722
value1=1.18370
</object>

<object>
type=28
name=autotrade  # 203 stop loss triggered 7.06 EURUSD at 1.17992
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526488117
value1=1.17992
</object>

<object>
type=32
name=autotrade  # 204 sell 7.45 EURUSD at 1.18098
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526515534
value1=1.18098
</object>

<object>
type=28
name=autotrade  # 205 stop loss triggered 7.45 EURUSD at 1.18023
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526549780
value1=1.18023
</object>

<object>
type=32
name=autotrade  # 206 sell 7.52 EURUSD at 1.18098
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526549780
value1=1.18098
</object>

<object>
type=28
name=autotrade  # 207 stop loss triggered 7.52 EURUSD at 1.18028
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526551816
value1=1.18028
</object>

<object>
type=32
name=autotrade  # 208 sell 7.60 EURUSD at 1.18098
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526551816
value1=1.18098
</object>

<object>
type=28
name=autotrade  # 209 stop loss triggered 7.60 EURUSD at 1.17939
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526566576
value1=1.17939
</object>

<object>
type=32
name=autotrade  # 210 sell 7.77 EURUSD at 1.18098
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526566576
value1=1.18098
</object>

<object>
type=28
name=autotrade  # 211 stop loss triggered 7.77 EURUSD at 1.18004
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526572350
value1=1.18004
</object>

<object>
type=32
name=autotrade  # 212 sell 7.86 EURUSD at 1.18098
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526572350
value1=1.18098
</object>

<object>
type=28
name=autotrade  # 213 stop loss triggered 7.86 EURUSD at 1.17983
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526577581
value1=1.17983
</object>

<object>
type=32
name=autotrade  # 214 sell 7.99 EURUSD at 1.18098
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526577581
value1=1.18098
</object>

<object>
type=28
name=autotrade  # 215 stop loss triggered 7.99 EURUSD at 1.17800
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526670507
value1=1.17800
</object>

<object>
type=32
name=autotrade  # 216 sell 8.34 EURUSD at 1.17710
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526861625
value1=1.17710
</object>

<object>
type=28
name=autotrade  # 217 stop loss triggered 8.34 EURUSD at 1.17581
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526877990
value1=1.17581
</object>

<object>
type=32
name=autotrade  # 218 sell 8.50 EURUSD at 1.17710
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1526877990
value1=1.17710
</object>

<object>
type=32
name=autotrade  # 219 sell modified 8.50 EURUSD at 1.17542
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1526893237
value1=1.17542
</object>

<object>
type=32
name=autotrade  # 220 sell modified 8.50 EURUSD at 1.17380
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1526896782
value1=1.17380
</object>

<object>
type=28
name=autotrade  # 221 stop loss triggered 8.50 EURUSD at 1.17189
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527074021
value1=1.17189
</object>

<object>
type=32
name=autotrade  # 222 sell 8.78 EURUSD at 1.17348
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527120000
value1=1.17348
</object>

<object>
type=32
name=autotrade  # 223 sell modified 8.78 EURUSD at 1.17046
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1527136957
value1=1.17046
</object>

<object>
type=28
name=autotrade  # 224 stop loss triggered 8.78 EURUSD at 1.16966
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527244174
value1=1.16966
</object>

<object>
type=32
name=autotrade  # 225 sell 8.90 EURUSD at 1.16863
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527465661
value1=1.16863
</object>

<object>
type=28
name=autotrade  # 226 stop loss triggered 8.90 EURUSD at 1.16740
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527510691
value1=1.16740
</object>

<object>
type=32
name=autotrade  # 227 sell 9.05 EURUSD at 1.16863
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527510691
value1=1.16863
</object>

<object>
type=32
name=autotrade  # 228 sell modified 9.05 EURUSD at 1.16705
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1527512538
value1=1.16705
</object>

<object>
type=32
name=autotrade  # 229 sell modified 9.05 EURUSD at 1.16541
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1527512759
value1=1.16541
</object>

<object>
type=28
name=autotrade  # 230 stop loss triggered 9.05 EURUSD at 1.16451
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527520348
value1=1.16451
</object>

<object>
type=32
name=autotrade  # 231 sell 9.19 EURUSD at 1.16577
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527520348
value1=1.16577
</object>

<object>
type=32
name=autotrade  # 232 sell modified 9.19 EURUSD at 1.16419
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1527521595
value1=1.16419
</object>

<object>
type=32
name=autotrade  # 233 sell modified 9.19 EURUSD at 1.16254
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1527522201
value1=1.16254
</object>

<object>
type=27
name=autotrade  # 234 order canceled 9.19 EURUSD at 1.16254
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527522989
value1=1.16254
</object>

<object>
type=27
name=autotrade  # 235 order canceled 9.19 EURUSD at 1.16254
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527522990
value1=1.16254
</object>

<object>
type=32
name=autotrade  # 236 sell 9.20 EURUSD at 1.16455
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1527552150
value1=1.16455
</object>

<object>
type=32
name=autotrade  # 237 sell modified 9.20 EURUSD at 1.16296
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1527566908
value1=1.16296
</object>

<object>
type=31
name=autotrade  # 238 buy 10.34 EURUSD at 1.17062
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528107701
value1=1.17062
</object>

<object>
type=28
name=autotrade  # 239 stop loss triggered 10.34 EURUSD at 1.17212
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528119639
value1=1.17212
</object>

<object>
type=31
name=autotrade  # 240 buy 10.54 EURUSD at 1.17062
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528119639
value1=1.17062
</object>

<object>
type=31
name=autotrade  # 241 buy modified 10.54 EURUSD at 1.17273
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528124371
value1=1.17273
</object>

<object>
type=27
name=autotrade  # 242 order canceled 10.54 EURUSD at 1.17273
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528124416
value1=1.17273
</object>

<object>
type=27
name=autotrade  # 243 order canceled 10.54 EURUSD at 1.17273
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528124417
value1=1.17273
</object>

<object>
type=31
name=autotrade  # 244 buy 10.57 EURUSD at 1.16727
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528156920
value1=1.16727
</object>

<object>
type=31
name=autotrade  # 245 buy modified 10.57 EURUSD at 1.16921
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528167638
value1=1.16921
</object>

<object>
type=28
name=autotrade  # 246 stop loss triggered 10.57 EURUSD at 1.16995
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528193788
value1=1.16995
</object>

<object>
type=31
name=autotrade  # 247 buy 10.67 EURUSD at 1.16944
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528193788
value1=1.16944
</object>

<object>
type=28
name=autotrade  # 248 stop loss triggered 10.67 EURUSD at 1.17049
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528199561
value1=1.17049
</object>

<object>
type=31
name=autotrade  # 249 buy 10.83 EURUSD at 1.16944
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528199561
value1=1.16944
</object>

<object>
type=28
name=autotrade  # 250 stop loss triggered 10.83 EURUSD at 1.17101
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528227940
value1=1.17101
</object>

<object>
type=31
name=autotrade  # 251 buy 11.05 EURUSD at 1.17051
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528243320
value1=1.17051
</object>

<object>
type=31
name=autotrade  # 252 buy modified 11.05 EURUSD at 1.17209
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528253056
value1=1.17209
</object>

<object>
type=28
name=autotrade  # 253 stop loss triggered 11.05 EURUSD at 1.17273
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528276467
value1=1.17273
</object>

<object>
type=31
name=autotrade  # 254 buy 11.14 EURUSD at 1.17213
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528276757
value1=1.17213
</object>

<object>
type=31
name=autotrade  # 255 buy modified 11.14 EURUSD at 1.17414
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528277526
value1=1.17414
</object>

<object>
type=28
name=autotrade  # 256 stop loss triggered 11.14 EURUSD at 1.17489
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528279521
value1=1.17489
</object>

<object>
type=31
name=autotrade  # 257 buy 11.24 EURUSD at 1.17375
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528279521
value1=1.17375
</object>

<object>
type=28
name=autotrade  # 258 stop loss triggered 11.24 EURUSD at 1.17462
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528280382
value1=1.17462
</object>

<object>
type=31
name=autotrade  # 259 buy 11.38 EURUSD at 1.17375
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528280441
value1=1.17375
</object>

<object>
type=31
name=autotrade  # 260 buy modified 11.38 EURUSD at 1.17569
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528281642
value1=1.17569
</object>

<object>
type=28
name=autotrade  # 261 stop loss triggered 11.38 EURUSD at 1.17656
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528290698
value1=1.17656
</object>

<object>
type=31
name=autotrade  # 262 buy 11.50 EURUSD at 1.17537
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528290698
value1=1.17537
</object>

<object>
type=27
name=autotrade  # 263 order canceled 11.50 EURUSD at 1.17537
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528294516
value1=1.17537
</object>

<object>
type=27
name=autotrade  # 264 order canceled 11.50 EURUSD at 1.17537
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528294517
value1=1.17537
</object>

<object>
type=31
name=autotrade  # 265 buy 11.49 EURUSD at 1.17559
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528329720
value1=1.17559
</object>

<object>
type=31
name=autotrade  # 266 buy modified 11.49 EURUSD at 1.17717
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528334551
value1=1.17717
</object>

<object>
type=31
name=autotrade  # 267 buy modified 11.49 EURUSD at 1.17875
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528345284
value1=1.17875
</object>

<object>
type=28
name=autotrade  # 268 stop loss triggered 11.49 EURUSD at 1.17962
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528362042
value1=1.17962
</object>

<object>
type=31
name=autotrade  # 269 buy 11.60 EURUSD at 1.17945
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528362940
value1=1.17945
</object>

<object>
type=28
name=autotrade  # 270 stop loss triggered 11.60 EURUSD at 1.18156
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528371938
value1=1.18156
</object>

<object>
type=31
name=autotrade  # 271 buy 11.96 EURUSD at 1.17817
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528761720
value1=1.17817
</object>

<object>
type=28
name=autotrade  # 272 stop loss triggered 11.96 EURUSD at 1.17881
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528798995
value1=1.17881
</object>

<object>
type=31
name=autotrade  # 273 buy 12.06 EURUSD at 1.17817
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528798995
value1=1.17817
</object>

<object>
type=31
name=autotrade  # 274 buy modified 12.06 EURUSD at 1.17975
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528801482
value1=1.17975
</object>

<object>
type=28
name=autotrade  # 275 stop loss triggered 12.06 EURUSD at 1.18077
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528971925
value1=1.18077
</object>

<object>
type=31
name=autotrade  # 276 buy 12.13 EURUSD at 1.17931
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1528971925
value1=1.17931
</object>

<object>
type=31
name=autotrade  # 277 buy modified 12.13 EURUSD at 1.18095
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528974081
value1=1.18095
</object>

<object>
type=31
name=autotrade  # 278 buy modified 12.13 EURUSD at 1.18253
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1528987530
value1=1.18253
</object>

<object>
type=27
name=autotrade  # 279 order canceled 12.13 EURUSD at 1.18253
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1528987541
value1=1.18253
</object>

<object>
type=32
name=autotrade  # 280 sell 12.36 EURUSD at 1.16024
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529280000
value1=1.16024
</object>

<object>
type=28
name=autotrade  # 281 stop loss triggered 12.36 EURUSD at 1.15899
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529301997
value1=1.15899
</object>

<object>
type=32
name=autotrade  # 282 sell 12.57 EURUSD at 1.16024
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529301997
value1=1.16024
</object>

<object>
type=32
name=autotrade  # 283 sell modified 12.57 EURUSD at 1.15864
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1529315737
value1=1.15864
</object>

<object>
type=28
name=autotrade  # 284 stop loss triggered 12.57 EURUSD at 1.15794
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529403725
value1=1.15794
</object>

<object>
type=32
name=autotrade  # 285 sell 12.72 EURUSD at 1.15840
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529403725
value1=1.15840
</object>

<object>
type=28
name=autotrade  # 286 stop loss triggered 12.72 EURUSD at 1.15579
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529414019
value1=1.15579
</object>

<object>
type=32
name=autotrade  # 287 sell 13.20 EURUSD at 1.15809
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529478876
value1=1.15809
</object>

<object>
type=28
name=autotrade  # 288 stop loss triggered 13.20 EURUSD at 1.15725
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529489381
value1=1.15725
</object>

<object>
type=32
name=autotrade  # 289 sell 13.35 EURUSD at 1.15809
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529489561
value1=1.15809
</object>

<object>
type=28
name=autotrade  # 290 stop loss triggered 13.35 EURUSD at 1.15599
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529493388
value1=1.15599
</object>

<object>
type=32
name=autotrade  # 291 sell 13.79 EURUSD at 1.15462
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529575121
value1=1.15462
</object>

<object>
type=28
name=autotrade  # 292 stop loss triggered 13.79 EURUSD at 1.15278
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529584061
value1=1.15278
</object>

<object>
type=32
name=autotrade  # 293 sell 14.14 EURUSD at 1.15462
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529584061
value1=1.15462
</object>

<object>
type=28
name=autotrade  # 294 stop loss triggered 14.14 EURUSD at 1.15388
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529591439
value1=1.15388
</object>

<object>
type=32
name=autotrade  # 295 sell 14.30 EURUSD at 1.15462
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529591439
value1=1.15462
</object>

<object>
type=28
name=autotrade  # 296 stop loss triggered 14.30 EURUSD at 1.15397
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529594141
value1=1.15397
</object>

<object>
type=32
name=autotrade  # 297 sell 14.42 EURUSD at 1.15462
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1529594141
value1=1.15462
</object>

<object>
type=28
name=autotrade  # 298 stop loss triggered 14.42 EURUSD at 1.15373
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1530179080
value1=1.15373
</object>

<object>
type=31
name=autotrade  # 299 buy 14.58 EURUSD at 1.16150
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1530250759
value1=1.16150
</object>

<object>
type=28
name=autotrade  # 300 stop loss triggered 14.58 EURUSD at 1.16219
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1530251200
value1=1.16219
</object>

<object>
type=31
name=autotrade  # 301 buy 14.73 EURUSD at 1.16267
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1530251377
value1=1.16267
</object>

<object>
type=28
name=autotrade  # 302 stop loss triggered 14.73 EURUSD at 1.16377
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1530252239
value1=1.16377
</object>

<object>
type=32
name=autotrade  # 303 sell 14.95 EURUSD at 1.16366
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1530578846
value1=1.16366
</object>

<object>
type=28
name=autotrade  # 304 stop loss triggered 14.95 EURUSD at 1.16292
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1530593727
value1=1.16292
</object>

<object>
type=32
name=autotrade  # 305 sell 15.11 EURUSD at 1.16366
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1530593727
value1=1.16366
</object>

<object>
type=28
name=autotrade  # 306 stop loss triggered 15.11 EURUSD at 1.16253
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531482975
value1=1.16253
</object>

<object>
type=31
name=autotrade  # 307 buy 15.40 EURUSD at 1.16937
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1531730553
value1=1.16937
</object>

<object>
type=28
name=autotrade  # 308 stop loss triggered 15.40 EURUSD at 1.17089
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531755278
value1=1.17089
</object>

<object>
type=31
name=autotrade  # 309 buy 15.71 EURUSD at 1.16937
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1531755278
value1=1.16937
</object>

<object>
type=31
name=autotrade  # 310 buy modified 15.71 EURUSD at 1.17095
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1531810635
value1=1.17095
</object>

<object>
type=31
name=autotrade  # 311 buy 15.70 EURUSD at 1.17087
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1531817100
value1=1.17087
</object>

<object>
type=27
name=autotrade  # 312 order canceled 15.70 EURUSD at 1.17087
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531819178
value1=1.17087
</object>

<object>
type=27
name=autotrade  # 313 order canceled 15.70 EURUSD at 1.17087
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531819179
value1=1.17087
</object>

<object>
type=32
name=autotrade  # 314 sell 15.77 EURUSD at 1.16801
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531872120
value1=1.16801
</object>

<object>
type=32
name=autotrade  # 315 sell modified 15.77 EURUSD at 1.16643
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1531879503
value1=1.16643
</object>

<object>
type=28
name=autotrade  # 316 stop loss triggered 15.77 EURUSD at 1.16332
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531931907
value1=1.16332
</object>

<object>
type=32
name=autotrade  # 317 sell 16.52 EURUSD at 1.16201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1531994861
value1=1.16201
</object>

<object>
type=32
name=autotrade  # 318 sell modified 16.52 EURUSD at 1.16042
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1532009019
value1=1.16042
</object>

<object>
type=32
name=autotrade  # 319 sell modified 16.52 EURUSD at 1.15884
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1532014229
value1=1.15884
</object>

<object>
type=28
name=autotrade  # 320 stop loss triggered 16.52 EURUSD at 1.15755
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1533294843
value1=1.15755
</object>

<object>
type=32
name=autotrade  # 321 sell 17.09 EURUSD at 1.15720
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1533513600
value1=1.15720
</object>

<object>
type=32
name=autotrade  # 322 sell modified 17.09 EURUSD at 1.15562
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1533561365
value1=1.15562
</object>

<object>
type=32
name=autotrade  # 323 sell modified 17.09 EURUSD at 1.15403
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1533563464
value1=1.15403
</object>

<object>
type=28
name=autotrade  # 324 stop loss triggered 17.09 EURUSD at 1.15337
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1533854912
value1=1.15337
</object>

<object>
type=32
name=autotrade  # 325 sell 17.37 EURUSD at 1.15316
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1533855385
value1=1.15316
</object>

<object>
type=28
name=autotrade  # 326 stop loss triggered 17.37 EURUSD at 1.15246
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1533869726
value1=1.15246
</object>

<object>
type=32
name=autotrade  # 327 sell 17.78 EURUSD at 1.13879
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1534247799
value1=1.13879
</object>

<object>
type=28
name=autotrade  # 328 stop loss triggered 17.78 EURUSD at 1.13417
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1534356581
value1=1.13417
</object>

<object>
type=31
name=autotrade  # 329 buy 18.99 EURUSD at 1.13697
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1534394200
value1=1.13697
</object>

<object>
type=28
name=autotrade  # 330 stop loss triggered 18.99 EURUSD at 1.13797
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1534403798
value1=1.13797
</object>

<object>
type=31
name=autotrade  # 331 buy 19.25 EURUSD at 1.13697
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1534403798
value1=1.13697
</object>

<object>
type=31
name=autotrade  # 332 buy modified 19.25 EURUSD at 1.13857
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1534408777
value1=1.13857
</object>

<object>
type=28
name=autotrade  # 333 stop loss triggered 19.25 EURUSD at 1.13967
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1534439835
value1=1.13967
</object>

<object>
type=31
name=autotrade  # 334 buy 19.54 EURUSD at 1.13697
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1534439835
value1=1.13697
</object>

<object>
type=28
name=autotrade  # 335 stop loss triggered 19.54 EURUSD at 1.13787
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1534484353
value1=1.13787
</object>

<object>
type=31
name=autotrade  # 336 buy 19.68 EURUSD at 1.14109
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1534723200
value1=1.14109
</object>

<object>
type=28
name=autotrade  # 337 stop loss triggered 19.68 EURUSD at 1.14174
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1534760802
value1=1.14174
</object>

<object>
type=31
name=autotrade  # 338 buy 19.90 EURUSD at 1.14109
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1534760802
value1=1.14109
</object>

<object>
type=31
name=autotrade  # 339 buy 22.22 EURUSD at 1.15791
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535118459
value1=1.15791
</object>

<object>
type=28
name=autotrade  # 340 stop loss triggered 22.22 EURUSD at 1.15891
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535122947
value1=1.15891
</object>

<object>
type=31
name=autotrade  # 341 buy 22.50 EURUSD at 1.15908
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535126719
value1=1.15908
</object>

<object>
type=28
name=autotrade  # 342 stop loss triggered 22.50 EURUSD at 1.16147
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535131650
value1=1.16147
</object>

<object>
type=31
name=autotrade  # 343 buy 23.21 EURUSD at 1.15926
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535328000
value1=1.15926
</object>

<object>
type=31
name=autotrade  # 344 buy modified 23.21 EURUSD at 1.16116
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1535329182
value1=1.16116
</object>

<object>
type=31
name=autotrade  # 345 buy modified 23.21 EURUSD at 1.16275
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1535338464
value1=1.16275
</object>

<object>
type=31
name=autotrade  # 346 buy modified 23.21 EURUSD at 1.16433
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1535338661
value1=1.16433
</object>

<object>
type=28
name=autotrade  # 347 stop loss triggered 23.21 EURUSD at 1.16498
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535388998
value1=1.16498
</object>

<object>
type=31
name=autotrade  # 348 buy 23.35 EURUSD at 1.16498
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535389865
value1=1.16498
</object>

<object>
type=27
name=autotrade  # 349 order canceled 23.35 EURUSD at 1.16498
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535390608
value1=1.16498
</object>

<object>
type=27
name=autotrade  # 350 order canceled 23.35 EURUSD at 1.16498
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535390609
value1=1.16498
</object>

<object>
type=31
name=autotrade  # 351 buy 23.26 EURUSD at 1.16868
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535500920
value1=1.16868
</object>

<object>
type=28
name=autotrade  # 352 stop loss triggered 23.26 EURUSD at 1.16969
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535563289
value1=1.16969
</object>

<object>
type=31
name=autotrade  # 353 buy 23.59 EURUSD at 1.16868
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535563360
value1=1.16868
</object>

<object>
type=28
name=autotrade  # 354 stop loss triggered 23.59 EURUSD at 1.16946
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535567420
value1=1.16946
</object>

<object>
type=31
name=autotrade  # 355 buy 23.85 EURUSD at 1.16868
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1535567762
value1=1.16868
</object>

<object>
type=31
name=autotrade  # 356 buy modified 23.85 EURUSD at 1.17026
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1535598396
value1=1.17026
</object>

<object>
type=28
name=autotrade  # 357 stop loss triggered 23.85 EURUSD at 1.17097
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535624976
value1=1.17097
</object>

<object>
type=32
name=autotrade  # 358 sell 24.16 EURUSD at 1.16676
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535645128
value1=1.16676
</object>

<object>
type=28
name=autotrade  # 359 stop loss triggered 24.16 EURUSD at 1.16564
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535647178
value1=1.16564
</object>

<object>
type=32
name=autotrade  # 360 sell 24.55 EURUSD at 1.16676
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535647178
value1=1.16676
</object>

<object>
type=28
name=autotrade  # 361 stop loss triggered 24.55 EURUSD at 1.16604
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535652719
value1=1.16604
</object>

<object>
type=32
name=autotrade  # 362 sell 24.79 EURUSD at 1.16676
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535652719
value1=1.16676
</object>

<object>
type=32
name=autotrade  # 363 sell modified 24.79 EURUSD at 1.16517
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1535656416
value1=1.16517
</object>

<object>
type=28
name=autotrade  # 364 stop loss triggered 24.79 EURUSD at 1.16391
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535734709
value1=1.16391
</object>

<object>
type=32
name=autotrade  # 365 sell 25.38 EURUSD at 1.16065
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535932800
value1=1.16065
</object>

<object>
type=28
name=autotrade  # 366 stop loss triggered 25.38 EURUSD at 1.15994
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535970218
value1=1.15994
</object>

<object>
type=32
name=autotrade  # 367 sell 25.62 EURUSD at 1.16065
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1535970218
value1=1.16065
</object>

<object>
type=28
name=autotrade  # 368 stop loss triggered 25.62 EURUSD at 1.15672
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536086319
value1=1.15672
</object>

<object>
type=31
name=autotrade  # 369 buy 27.06 EURUSD at 1.15855
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536121181
value1=1.15855
</object>

<object>
type=28
name=autotrade  # 370 stop loss triggered 27.06 EURUSD at 1.16124
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536168059
value1=1.16124
</object>

<object>
type=31
name=autotrade  # 371 buy 28.00 EURUSD at 1.16179
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536171518
value1=1.16179
</object>

<object>
type=28
name=autotrade  # 372 stop loss triggered 28.00 EURUSD at 1.16244
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536179125
value1=1.16244
</object>

<object>
type=31
name=autotrade  # 373 buy 28.26 EURUSD at 1.16179
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536179585
value1=1.16179
</object>

<object>
type=31
name=autotrade  # 374 buy modified 28.26 EURUSD at 1.16339
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1536202898
value1=1.16339
</object>

<object>
type=28
name=autotrade  # 375 stop loss triggered 28.26 EURUSD at 1.16438
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536248550
value1=1.16438
</object>

<object>
type=31
name=autotrade  # 376 buy 28.62 EURUSD at 1.16254
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536248550
value1=1.16254
</object>

<object>
type=28
name=autotrade  # 377 stop loss triggered 28.62 EURUSD at 1.16356
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536256705
value1=1.16356
</object>

<object>
type=31
name=autotrade  # 378 buy 29.05 EURUSD at 1.16254
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536256705
value1=1.16254
</object>

<object>
type=28
name=autotrade  # 379 stop loss triggered 29.05 EURUSD at 1.16367
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536315027
value1=1.16367
</object>

<object>
type=32
name=autotrade  # 380 sell 29.59 EURUSD at 1.15851
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536334422
value1=1.15851
</object>

<object>
type=28
name=autotrade  # 381 stop loss triggered 29.59 EURUSD at 1.15739
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536336639
value1=1.15739
</object>

<object>
type=32
name=autotrade  # 382 sell 30.08 EURUSD at 1.15734
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536337220
value1=1.15734
</object>

<object>
type=28
name=autotrade  # 383 stop loss triggered 30.08 EURUSD at 1.15659
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536348088
value1=1.15659
</object>

<object>
type=32
name=autotrade  # 384 sell 30.44 EURUSD at 1.15661
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536537600
value1=1.15661
</object>

<object>
type=32
name=autotrade  # 385 sell modified 30.44 EURUSD at 1.15501
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1536563704
value1=1.15501
</object>

<object>
type=28
name=autotrade  # 386 stop loss triggered 30.44 EURUSD at 1.15385
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536573992
value1=1.15385
</object>

<object>
type=31
name=autotrade  # 387 buy 30.82 EURUSD at 1.15924
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536624120
value1=1.15924
</object>

<object>
type=28
name=autotrade  # 388 stop loss triggered 30.82 EURUSD at 1.16177
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536666263
value1=1.16177
</object>

<object>
type=31
name=autotrade  # 389 buy 31.89 EURUSD at 1.15914
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536710520
value1=1.15914
</object>

<object>
type=28
name=autotrade  # 390 stop loss triggered 31.89 EURUSD at 1.16201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536781647
value1=1.16201
</object>

<object>
type=31
name=autotrade  # 391 buy 33.10 EURUSD at 1.16238
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536782609
value1=1.16238
</object>

<object>
type=28
name=autotrade  # 392 stop loss triggered 33.10 EURUSD at 1.16328
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536815203
value1=1.16328
</object>

<object>
type=31
name=autotrade  # 393 buy 33.33 EURUSD at 1.16326
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1536816605
value1=1.16326
</object>

<object>
type=28
name=autotrade  # 394 stop loss triggered 33.33 EURUSD at 1.16801
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1536939279
value1=1.16801
</object>

<object>
type=32
name=autotrade  # 395 sell 35.51 EURUSD at 1.16396
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1537142400
value1=1.16396
</object>

<object>
type=28
name=autotrade  # 396 stop loss triggered 35.51 EURUSD at 1.16293
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1537178977
value1=1.16293
</object>

<object>
type=32
name=autotrade  # 397 sell 36.01 EURUSD at 1.16396
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1537178977
value1=1.16396
</object>

<object>
type=28
name=autotrade  # 398 stop loss triggered 36.01 EURUSD at 1.16277
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538129798
value1=1.16277
</object>

<object>
type=32
name=autotrade  # 399 sell 37.01 EURUSD at 1.16230
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538352000
value1=1.16230
</object>

<object>
type=32
name=autotrade  # 400 sell modified 37.01 EURUSD at 1.16071
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1538368325
value1=1.16071
</object>

<object>
type=32
name=autotrade  # 401 sell modified 37.01 EURUSD at 1.15913
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1538387052
value1=1.15913
</object>

<object>
type=28
name=autotrade  # 402 stop loss triggered 37.01 EURUSD at 1.15828
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538416587
value1=1.15828
</object>

<object>
type=32
name=autotrade  # 403 sell 37.58 EURUSD at 1.15944
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538416587
value1=1.15944
</object>

<object>
type=32
name=autotrade  # 404 sell modified 37.58 EURUSD at 1.15786
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1538418630
value1=1.15786
</object>

<object>
type=28
name=autotrade  # 405 stop loss triggered 37.58 EURUSD at 1.15412
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538492619
value1=1.15412
</object>

<object>
type=32
name=autotrade  # 406 sell 39.71 EURUSD at 1.15584
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538524800
value1=1.15584
</object>

<object>
type=28
name=autotrade  # 407 stop loss triggered 39.71 EURUSD at 1.15512
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538578890
value1=1.15512
</object>

<object>
type=32
name=autotrade  # 408 sell 40.12 EURUSD at 1.15584
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538579429
value1=1.15584
</object>

<object>
type=32
name=autotrade  # 409 sell modified 40.12 EURUSD at 1.15400
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1538582236
value1=1.15400
</object>

<object>
type=28
name=autotrade  # 410 stop loss triggered 40.12 EURUSD at 1.15339
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538586330
value1=1.15339
</object>

<object>
type=32
name=autotrade  # 411 sell 40.55 EURUSD at 1.15422
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538587859
value1=1.15422
</object>

<object>
type=28
name=autotrade  # 412 stop loss triggered 40.55 EURUSD at 1.15013
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538655496
value1=1.15013
</object>

<object>
type=32
name=autotrade  # 413 sell 43.05 EURUSD at 1.15201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538697750
value1=1.15201
</object>

<object>
type=27
name=autotrade  # 414 order canceled 43.05 EURUSD at 1.15201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538727242
value1=1.15201
</object>

<object>
type=27
name=autotrade  # 415 order canceled 43.05 EURUSD at 1.15201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538727244
value1=1.15201
</object>

<object>
type=27
name=autotrade  # 416 order canceled 43.05 EURUSD at 1.15201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538727246
value1=1.15201
</object>

<object>
type=27
name=autotrade  # 417 order canceled 43.05 EURUSD at 1.15201
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1538727248
value1=1.15201
</object>

<object>
type=31
name=autotrade  # 418 buy 42.93 EURUSD at 1.15442
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1538758418
value1=1.15442
</object>

<object>
type=28
name=autotrade  # 419 stop loss triggered 42.93 EURUSD at 1.15510
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539233540
value1=1.15510
</object>

<object>
type=31
name=autotrade  # 420 buy 42.78 EURUSD at 1.15305
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1539233540
value1=1.15305
</object>

<object>
type=31
name=autotrade  # 421 buy modified 42.78 EURUSD at 1.15616
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1539240757
value1=1.15616
</object>

<object>
type=28
name=autotrade  # 422 stop loss triggered 42.78 EURUSD at 1.15712
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539268418
value1=1.15712
</object>

<object>
type=31
name=autotrade  # 423 buy 43.28 EURUSD at 1.15691
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1539268483
value1=1.15691
</object>

<object>
type=28
name=autotrade  # 424 stop loss triggered 43.28 EURUSD at 1.15836
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539272305
value1=1.15836
</object>

<object>
type=31
name=autotrade  # 425 buy 44.12 EURUSD at 1.15691
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1539272305
value1=1.15691
</object>

<object>
type=28
name=autotrade  # 426 stop loss triggered 44.12 EURUSD at 1.15890
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539335728
value1=1.15890
</object>

<object>
type=32
name=autotrade  # 427 sell 45.44 EURUSD at 1.15454
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539561720
value1=1.15454
</object>

<object>
type=28
name=autotrade  # 428 stop loss triggered 45.44 EURUSD at 1.15388
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539786388
value1=1.15388
</object>

<object>
type=32
name=autotrade  # 429 sell 45.96 EURUSD at 1.15431
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539787295
value1=1.15431
</object>

<object>
type=28
name=autotrade  # 430 stop loss triggered 45.96 EURUSD at 1.15347
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539800491
value1=1.15347
</object>

<object>
type=32
name=autotrade  # 431 sell 46.51 EURUSD at 1.15431
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539804455
value1=1.15431
</object>

<object>
type=32
name=autotrade  # 432 sell modified 46.51 EURUSD at 1.15273
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1539806851
value1=1.15273
</object>

<object>
type=32
name=autotrade  # 433 sell modified 46.51 EURUSD at 1.15114
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1539815164
value1=1.15114
</object>

<object>
type=27
name=autotrade  # 434 order canceled 46.51 EURUSD at 1.15114
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539818960
value1=1.15114
</object>

<object>
type=27
name=autotrade  # 435 order canceled 46.51 EURUSD at 1.15114
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539818961
value1=1.15114
</object>

<object>
type=27
name=autotrade  # 436 order canceled 46.51 EURUSD at 1.15114
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539818962
value1=1.15114
</object>

<object>
type=32
name=autotrade  # 437 sell 46.64 EURUSD at 1.15368
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539820950
value1=1.15368
</object>

<object>
type=32
name=autotrade  # 438 sell modified 46.64 EURUSD at 1.15100
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1539821720
value1=1.15100
</object>

<object>
type=32
name=autotrade  # 439 sell modified 46.64 EURUSD at 1.14940
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1539854562
value1=1.14940
</object>

<object>
type=28
name=autotrade  # 440 stop loss triggered 46.64 EURUSD at 1.14862
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539890499
value1=1.14862
</object>

<object>
type=32
name=autotrade  # 441 sell 47.22 EURUSD at 1.14982
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539890499
value1=1.14982
</object>

<object>
type=32
name=autotrade  # 442 sell modified 47.22 EURUSD at 1.14823
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1539891329
value1=1.14823
</object>

<object>
type=27
name=autotrade  # 443 order canceled 47.22 EURUSD at 1.14823
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539895957
value1=1.14823
</object>

<object>
type=27
name=autotrade  # 444 order canceled 47.22 EURUSD at 1.14823
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539895958
value1=1.14823
</object>

<object>
type=27
name=autotrade  # 445 order canceled 47.22 EURUSD at 1.14823
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1539895959
value1=1.14823
</object>

<object>
type=31
name=autotrade  # 446 buy 47.09 EURUSD at 1.15080
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=11296515
selectable=0
date1=1540166400
value1=1.15080
</object>

<object>
type=28
name=autotrade  # 447 stop loss triggered 47.09 EURUSD at 1.15283
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1540206327
value1=1.15283
</object>

<object>
type=32
name=autotrade  # 448 sell 48.68 EURUSD at 1.14749
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1540252800
value1=1.14749
</object>

<object>
type=32
name=autotrade  # 449 sell modified 48.68 EURUSD at 1.14588
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1540285397
value1=1.14588
</object>

<object>
type=28
name=autotrade  # 450 stop loss triggered 48.68 EURUSD at 1.14428
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1540377059
value1=1.14428
</object>

<object>
type=32
name=autotrade  # 451 sell 49.98 EURUSD at 1.14393
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1540377628
value1=1.14393
</object>

<object>
type=32
name=autotrade  # 452 sell modified 49.98 EURUSD at 1.14235
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=36095
selectable=0
date1=1540380940
value1=1.14235
</object>

<object>
type=28
name=autotrade  # 453 stop loss triggered 49.98 EURUSD at 1.14097
descr=EURUSD:PERIOD_H1:14:14:200:200:
color=1918177
selectable=0
date1=1540388682
value1=1.14097
</object>

</window></chart>