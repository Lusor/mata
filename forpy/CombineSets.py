from datetime import timedelta, datetime
import io
import math
import time
import pandas as pd

def applyExcelFormat(line):
    line = line.replace(';', '\t') \
        .replace(',', '\t')
    line = line.replace('.', ",")
    return line;


orderFilename = "data/2019_M15.csv"
marketFilename = "data/collection_2019_M1.csv"

marketMap = {}
marketHeader = ""

with open(marketFilename) as fp:
    marketHeader = fp.readline()
    line = fp.readline()
    while line:
        parts = line.split(';')
        marketMap[parts[0]] = line.replace(parts[0] + ";", "").replace("\n", "")
        line = fp.readline()
    # print(line)

print(len(marketMap))

buyFile = open("data/buy.csv", "w+")
sellFile = open("data/sell.csv", "w+")
allFile = open("data/all.csv", "w+")

with open(orderFilename) as fp:
    orderHeader = fp.readline().replace("\n", "")
    globalheader = orderHeader + ";" + marketHeader.replace("date;", "")
    allFile.write(globalheader)
    sellFile.write(globalheader)
    buyFile.write(globalheader)
    line = fp.readline()
    while line:
        line = line.replace("\n", "")
        parts = line.split(';')
        if parts[0] in ('sell', 'buy'):
            orderDatetime = datetime.strptime(parts[1], '%Y.%m.%d %H:%M:%S')
            orderDatetime = orderDatetime.replace(second=0) + timedelta(minutes=15)
            changedDatetime = orderDatetime.strftime('%Y.%m.%d %H:%M:%S')
            if changedDatetime not in marketMap.keys():
                print(changedDatetime + " not found")
            else:
                newLine = line + ";" + marketMap[changedDatetime] + "\n"
                allFile.write(applyExcelFormat(newLine))
                if parts[0] == 'sell':
                    sellFile.write(applyExcelFormat(newLine))
                if parts[0] == 'buy':
                    buyFile.write(applyExcelFormat(newLine))
                print(newLine)
        line = fp.readline()
allFile.close()
sellFile.close()
buyFile.close()