import io
import math
import time
from datetime import datetime, timedelta

def exportToMTTemplate(dealList, filename):

    with io.open('../data/empty.tpl', 'r', encoding='utf16') as f:
        content = f.readlines()

    index = 1
    greenColor = '32768'
    for line in dealList:
        elements = line.split('\t')
        type = elements[0]
        datetimeStr = elements[1]
        volume = elements[2]
        openPrice = elements[3]

        symbol = 'EURUSD'

        openDate = datetime.strptime(datetimeStr, '%Y.%m.%d %H:%M:%S')

        openDateUnix = time.mktime((openDate + timedelta(hours=3)).timetuple())
        openDateUnixStr = str("%.f" % openDateUnix)
        colorBuy = 11296515
        colorSell = 1918177
        colorOrange = 36095 #14822282


        isBuy = "buy" in type.lower()
        isStopLoss = 'stop loss triggered' in type.lower()
        isCanceled = 'order canceled' in type.lower() or 'order expired' in type.lower()

        index = index+1
        content.append("<object>" + "\n")

        if isCanceled:
            content.append("type=27" + "\n")
        elif isStopLoss:
            content.append("type=28" + "\n")
        elif isBuy:
            content.append("type=31" + "\n")
        else:
            content.append("type=32" + "\n")
        content.append("name=autotrade  # " + str(index) + " " + type + " " + str(volume) +" " + symbol + " at " + str(openPrice) +"\n")
        content.append("descr=" +symbol+":PERIOD_H1:14:14:200:200:" + "\n")
        currentColor = str(colorBuy) if isBuy else str(colorSell)
        if 'modified' in type.lower():
            currentColor = str(colorOrange)
        content.append("color=" + currentColor + "\n")
        content.append("selectable=0" + "\n")
        content.append("date1=" + openDateUnixStr + "\n")
        content.append("value1=" + str(openPrice) + "\n")
        content.append("</object>" + "\n\n")

    content.append("</window>")
    content.append("</chart>")

    with open(filename + ".tpl", "w") as myfile:
        for line in content:
            myfile.write(line)
    return


exampleLine = 'DO	0	12:05:44.334	Trade	2019.01.02 14:28:37   sell limit 0.88 EURUSD at 1.14077 sl: 1.64077 tp: 1.11953 (1.14034 / 1.14036 / 1.14034)'

#filename = 'C:/Users/VB-7/AppData/Roaming/MetaQuotes/Tester/9EB2973C469D24060397BB5158EA73A5/Agent-127.0.0.1-3000/Logs/20200112_10_дней.log'
logFilename = "data/kiss_20200118_2019_M15.log"

list = []
lastline = ""
with open(logFilename) as fp:
    line = fp.readline()
    while line:
       # print(line)
        if ("sell limit" in line or "buy limit" in line) \
                and "triggered" not in line and "order modified" not in line and "order performed" not in line and "[done]" not in line\
                and "order expired" not in line and "order canceled" not in line:
            arr = line.split('\t')
            elements = arr[4].split(' ')
            curline = "{}\t{}\t{}\t{}\t{}\t{}".format(elements[4], elements[0] + ' ' + elements[1], elements[6], elements[9], elements[11], elements[13])
            if(lastline != curline):
                lastline = curline
                print(curline)
                list.append(curline)


 #  order modified [#37 sell limit 0.89 EURUSD at 1.13788]


        if ("sell limit" in line or "buy limit" in line) and "order modified" in line \
                    and "order expired" not in line and "order canceled" not in line and "[done]" not in line:
                arr = line.split('\t')
                elements = arr[4].split(' ')
                curline = "{}\t{}\t{}\t{}\t{}\t{}".format(elements[7] + ' modified', elements[0] + ' ' + elements[1], elements[9], elements[12].replace("]\n", ""), '', '')
                if(lastline != curline):
                    lastline = curline
                    print(curline)
                    list.append(curline)

# stop loss triggered #68 sell 0.90 EURUSD 1.13588 sl: 1.13474 tp: 1.11464 [#83 buy 0.90 EURUSD at 1.13474]
        if "stop loss triggered" in line:
            arr = line.split('\t')
            elements = arr[4].split(' ')
            curline = "{}\t{}\t{}\t{}\t{}\t{}".format('stop loss triggered', elements[0] + ' ' + elements[1], elements[9],
                                                 elements[13].replace("]\n", ""), '', '')
            if (lastline != curline):
                lastline = curline
                print(curline)
                list.append(curline)

        if "order canceled" in line:
            arr = line.split('\t')
            elements = arr[4].split(' ')
            curline = "{}\t{}\t{}\t{}\t{}\t{}".format('order canceled', elements[0] + ' ' + elements[1], elements[9],
                                                 elements[12].replace("]\n", ""), '', '')
            if (lastline != curline):
                lastline = curline
                print(curline)
                list.append(curline)

        if "order expired" in line:
            arr = line.split('\t')
            elements = arr[4].split(' ')
            curline = "{}\t{}\t{}\t{}\t{}\t{}".format('order expired', elements[0] + ' ' + elements[1], elements[9],
                                                 elements[12].replace("]\n", ""), '', '')
            if (lastline != curline):
                lastline = curline
                print(curline)
                list.append(curline)
        line = fp.readline()

   # exportToMTTemplate(list, logFilename)