from datetime import datetime

import joblib
import numpy as np
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import cross_val_score, ShuffleSplit
from sklearn.tree import DecisionTreeClassifier

from classifiers.DTClassifier import DTClassifier
from classifiers.RFClassifier import RFClassifier
from classifiers.SVMClassifier import SVMClassifier
from data_analysis.DataPreparation import DataPreparation
from data_loader.DatasetLayoutLoad import DatasetLayoutLoad
from data_loader.SQLiteAdapter import SQLiteAdapter


def testAccuracy(df, y, testPart, featureNumber):
    max_depths = np.linspace(1, featureNumber, featureNumber, endpoint=True)

    binaryLabling = [0 if i == 0 else 1 for i in y]
    train_results = []
    test_results = []
    (x_train, x_test, y_train, y_test) = DTClassifier.split(df, binaryLabling, testPart)
    for max_depth in max_depths:
        dt = DecisionTreeClassifier(max_depth=max_depth)
        dt.fit(x_train, y_train)

        train_pred = dt.predict(x_train)

        false_positive_rate, true_positive_rate, thresholds = roc_curve(y_train, train_pred)
        roc_auc = auc(false_positive_rate, true_positive_rate)
        # Add auc score to previous train results
        train_results.append(roc_auc)

        y_pred = dt.predict(x_test)

        false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, y_pred)
        roc_auc = auc(false_positive_rate, true_positive_rate)
        # Add auc score to previous test results
        test_results.append(roc_auc)

    from matplotlib.legend_handler import HandlerLine2D
    import matplotlib.pyplot as plt

    line1, = plt.plot(max_depths, train_results, 'b', label="Train AUC")
    line2, = plt.plot(max_depths, test_results, 'r', label="Test AUC")

    plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})

    plt.ylabel("AUC score")
    plt.xlabel("Tree depth")
    plt.show()
    return

print 'start'

crossID = 1
timeframe = 1440
startDate = datetime(2016, 8, 1)
endDate = datetime(2016, 12, 15)

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

preparation = DataPreparation()
preparation.setData(low, high, close, volume)
dfSet = preparation.formDataSet()

print len(dfSet)

layoutLoad = DatasetLayoutLoad()
layoutLoad.load("../data/layout.xlsx")
QUALITY_TYPES = [1, 2]
times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]
yLabling = layoutLoad.transformToLayout(times, QUALITY_TYPES)
binaryLabling = [0 if i == 0 else 1 for i in yLabling]


print "Started: learning phase"

features = list(dfSet.columns)

for testPercent in range(1, 9):
    dtCls = SVMClassifier({"max_depth": len(features)})
    acc = dtCls.trainAndTest(dfSet, binaryLabling, testPercent / 10.0)
    print "Accuracy  for " + str(testPercent / 10.0) + "% :" + str(acc)

#testAccuracy(dfSet, binaryLabling, 0.3, len(features))

#print SVMClassifier.svc_param_selection(dfSet, binaryLabling, 5)

#dtCls = SVMClassifier({"max_depth": 2})
#cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
#print cross_val_score(dtCls.svmCls, dfSet, binaryLabling, cv=3, scoring='f1_macro')

#DTClassifier.visualize_tree(dtCls.treeCls, features)



dtRFCls = DTClassifier({})
dtRFCls.trainAndTest(dfSet, binaryLabling, 0.3)
#print dtRFCls.findBestParameters(dfSet, binaryLabling)


prediction = dtCls.predict(dfSet)
for i in range(len(times)):
    if prediction[i] != 0:
        print times[i]

#model = joblib.dump(dtCls, 'svm-cls.dat')

print 'finish'