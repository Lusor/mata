from datetime import datetime

from data_analysis.DataPreparation import DataPreparation
from data_loader.DatasetLayoutLoad import DatasetLayoutLoad
from data_loader.SQLiteAdapter import SQLiteAdapter


class DataPreparator:

    @staticmethod
    def loadData(startDate, endDate, crossID = 1, timeframe = 1440):

        #startDate = datetime(2016, 8, 1)
        #endDate = datetime(2017, 6, 1)

        adapter = SQLiteAdapter()
        adapter.connect("sqlite:///../data/findb.s3db")

        quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

        close = [x.Close for x in quotes]
        high = [x.High for x in quotes]
        low = [x.Low for x in quotes]
        volume = [x.Volume for x in quotes]

        preparation = DataPreparation()
        preparation.setData(low, high, close, volume)
        dfSet = preparation.formDataSet()

        print len(dfSet)

        layoutLoad = DatasetLayoutLoad()
        layoutLoad.load("../data/layout.xlsx")
        QUALITY_TYPES = [1, 2]
        times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]
        yLabling = layoutLoad.transformToLayout(times, QUALITY_TYPES)
        #binaryLabling = [0 if i == 0 else 1 for i in yLabling]
        return dfSet, yLabling