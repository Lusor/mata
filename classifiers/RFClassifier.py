from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV

from classifiers.DTClassifier import DTClassifier


class RFClassifier:

    def __init__(self, paramMap):
        self.rfClf = RandomForestClassifier()  # n_estimators=10



    def trainCls(self, X_train, Y_train):
        self.gsClf.fit(X_train, Y_train)
        return

    def testCls(self, X_test, Y_test):
        return self.gsClf.score(X=X_test, y=Y_test)

    def findBestParameters(self, X_train, Y_train):
        # hyper parameters set
        params = {'criterion': ['gini', 'entropy'],
                  'n_estimators': [10, 15, 20, 25, 30],
                  'min_samples_leaf': [1, 2, 3],
                  'min_samples_split': [3, 4, 5, 6, 7],
                  'random_state': [123],
                  'n_jobs': [1]}
        # Making models with hyper parameters sets
        gsClf = GridSearchCV(self.rfClf, param_grid=params, n_jobs=1)
        print gsClf.fit(X_train, Y_train)
        print "Best Hyper Parameters:",  gsClf.best_params_
        print "Best Hyper estimator:", gsClf.best_estimator_
        print "Best Hyper score:", gsClf.best_score_
        return

    def trainAndTest(self, df, y, testPart):
        (X_train, X_test, Y_train, Y_test) = DTClassifier.split(df, y, testPart)
        self.trainCls(X_train, Y_train)
        acc = self.testCls(X_test, Y_test)
        return acc

