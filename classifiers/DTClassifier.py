from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, export_graphviz
import subprocess


class DTClassifier:

    def __init__(self, paramMap):

        if "max_depth" in paramMap:
            max_depth = int(paramMap["max_depth"])
        else:
            max_depth = 10

        self.treeCls = DecisionTreeClassifier(max_depth=max_depth)  #max_depth=5, random_state=17

    @staticmethod
    def split(df, y, testPart):
        X_train, X_test, Y_train, Y_test = train_test_split(df.values, y, test_size=testPart,
            random_state=17)

        #knn = KNeighborsClassifier(n_neighbors=10)
        # knn.fit(X_train, Y_train)
        return (X_train, X_test, Y_train, Y_test)


    def trainCls(self, X_train, Y_train):
        self.treeCls.fit(X_train, Y_train)
        return

    def testCls(self, X_test, Y_test):
        return self.treeCls.score(X=X_test, y=Y_test)


    def trainAndTest(self, df, y, testPart):
        (X_train, X_test, Y_train, Y_test) = DTClassifier.split(df, y, testPart)
        self.trainCls(X_train, Y_train)
        acc = self.testCls(X_test, Y_test)
        return acc


    @staticmethod
    def visualize_tree(tree, feature_names):
        """Create tree png using graphviz.

        Args
        ----
        tree -- scikit-learn DecsisionTree.
        feature_names -- list of feature names.
        """
        with open("dt.dot", 'w') as f:
            export_graphviz(tree, out_file=f,
                            feature_names=feature_names)

        command = ["../../graphviz-2.38/release/bin/dot", "-Tpng", "dt.dot", "-o", "dt.png"]
        try:
            subprocess.check_call(command)
        except:
            exit("Could not run dot, ie graphviz, to "
                 "produce visualization")