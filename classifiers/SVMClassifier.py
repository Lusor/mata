from sklearn import svm
from sklearn.model_selection import train_test_split, GridSearchCV


class SVMClassifier:

    # https://medium.com/all-things-ai/in-depth-parameter-tuning-for-svc-758215394769
    # https://towardsdatascience.com/https-medium-com-pupalerushikesh-svm-f4b42800e989
    # https://medium.com/@aneesha/svm-parameter-tuning-in-scikit-learn-using-gridsearchcv-2413c02125a0
    def __init__(self, paramMap):
        # for 0.3: 0.9996194824961948
        # kernels = ['linear', 'rbf', 'poly']
        # for rbf: gammas = [0.1, 1, 10, 100]
        #          cs = [0.1, 1, 10, 100, 1000]
        # for poly degrees = [0, 1, 2, 3, 4, 5, 6]
        self.svmCls = svm.SVC(kernel='rbf', C=1, gamma=0.001)

    @staticmethod
    def split(df, y, testPart):
        X_train, X_test, Y_train, Y_test = train_test_split(df.values, y, test_size=testPart,
            random_state=17)

        #knn = KNeighborsClassifier(n_neighbors=10)
        # knn.fit(X_train, Y_train)
        return (X_train, X_test, Y_train, Y_test)


    def trainCls(self, X_train, Y_train):
        self.svmCls.fit(X_train, Y_train)
        return

    def testCls(self, X_test, Y_test):
        return self.svmCls.score(X=X_test, y=Y_test)


    def trainAndTest(self, df, y, testPart):
        y = [float(i) for i in y]
        (X_train, X_test, Y_train, Y_test) = SVMClassifier.split(df, y, testPart)
        self.trainCls(X_train, Y_train)
        acc = self.testCls(X_test, Y_test)
        return acc

    def predict(self, df):
        return self.svmCls.predict(df)

    @staticmethod
    def svc_param_selection(X, y, nfolds):
   #     Cs = [0.001, 0.01, 0.1, 1, 10]
   #     gammas = [0.001, 0.01, 0.1, 1, 10]
   #     param_grid = {'C': Cs, 'gamma': gammas}

        param_grid = [
            {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
            {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']},
        ]

        grid_search = GridSearchCV(svm.SVC(kernel='rbf'), param_grid, cv=nfolds)
        grid_search.fit(X, y)
        grid_search.best_params_
        return grid_search.best_params_