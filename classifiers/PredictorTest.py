from datetime import datetime

import joblib

from data_analysis.DataPreparation import DataPreparation
from data_loader.SQLiteAdapter import SQLiteAdapter

crossID = 1
timeframe = 60
startDate = datetime(2016, 8, 1)
endDate = datetime(2017, 6, 1)

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

preparation = DataPreparation()
preparation.setData(low, high, close, volume)
dfSet = preparation.formDataSet()

print len(dfSet)

times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]

clf = joblib.load('svm-cls.dat')
prediction = clf.predict(dfSet)

print prediction

for i in range(len(times)):
    if prediction[i] != 0:
        print times[i]