from datetime import datetime
import numpy
import talib
from data_loader.SQLiteAdapter import SQLiteAdapter
from indicators.ichimoku import Ichimoku
from signals.Signals import Signals


__author__ = 'VB7'

crossID= 1
timeframe = 60
startDate = datetime(2005, 1, 1)
endDate = datetime(2015, 5, 1)

#currencyData = DukascopyCSV.read('EURUSD_Candlestick_1_M_BID_01.03.2018-31.03.2018.csv')

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")


quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

#--------------
ichimoku = Ichimoku()
ichimokuValues = ichimoku.compute(high, low, close)


print ichimokuValues["tenkan_sen"]