from datetime import datetime

import numpy
import pandas as pd
import talib
import os.path

from classifiers.DTClassifier import DTClassifier
from data_loader.DatasetLayoutLoad import DatasetLayoutLoad
from data_loader.SQLiteAdapter import SQLiteAdapter
from data_visual.DataPlot import DataPlot
from data_visual.SimplePlot import SimplePlot
from indicators.ichimoku import Ichimoku
from signals.Signals import Signals

__author__ = 'VB7'

crossID= 1
timeframe = 1440
startDate = datetime(2016, 8, 1)
endDate = datetime(2017, 7, 1)

#currencyData = DukascopyCSV.read('EURUSD_Candlestick_1_M_BID_01.03.2018-31.03.2018.csv')

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

dataPlot = DataPlot()
dataPlot.initFigures(3)

print len(quotes)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

#times = [datetime.combine(x.Date, x.Time) for x in quotes]
times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]

ma100 = talib.SMA(numpy.asarray(close), timeperiod=100)
ma200 = talib.SMA(numpy.asarray(close), timeperiod=200)

mfi = talib.MFI(numpy.asarray(high), numpy.asarray(low), numpy.asarray(close), numpy.asarray(volume), timeperiod=14)
macd, macdsignal, macdhist = talib.MACD(numpy.asarray(close), fastperiod=12, slowperiod=26, signalperiod=9)

#where_are_NaNs = numpy.isnan(ma100)
#where_are_NaNs = numpy.isnan(ma200)
#sma[where_are_NaNs] = 0

ma100Cross = Signals.lineCrossOver(close, ma100)
ma200Cross = Signals.lineCrossOver(close, ma200)
mfiCross = Signals.levelBeingOutOfLimits(mfi, 80, 20)
macdSignals = Signals.macdLineCross(macd, macdsignal, 9, 9)

macdSellSignals = Signals.selectSignals(macd, macdSignals, -1)
macdBuySignals = Signals.selectSignals(macd, macdSignals, +1)



# signals
signalJoin = Signals.signalJoin(ma100Cross, ma200Cross)
signalJoin = Signals.signalJoin(signalJoin, mfiCross)
signalJoin = Signals.signalJoin(signalJoin, macdSignals)

sellSignals = Signals.selectSignals(close, signalJoin, -1)
buySignals = Signals.selectSignals(close, signalJoin, +1)

#print 'ma100Cross'
#print ma100Cross

#print 'ma200Cross'
#print ma200Cross



dataPlot.addMajorLineData(411, times, close, 'EURUSD')
dataPlot.addMinorLineData(411, times, ma100, 'MA100', 'r')
dataPlot.addMinorLineData(411, times, ma200, 'MA200', 'b')
# signals
dataPlot.addMinorSignalData(411, times, sellSignals, 'v','sell signals', 'm')
dataPlot.addMinorSignalData(411, times, buySignals, '^','buy signals', 'm')


dataPlot.addMajorLineData(412, times, mfi, 'MFI')

dataPlot.addMajorLineData(413, times, macd, 'MACD')
dataPlot.addMinorLineData(413, times, macdsignal, 'MACD-signal', 'r')
dataPlot.addMinorSignalData(413, times, macdSellSignals, 'v','sell signals', 'm')
dataPlot.addMinorSignalData(413, times, macdBuySignals, '^','buy signals', 'm')



# ichimoku clouds
ichimoku = Ichimoku()
ichimokuValues = ichimoku.compute(high, low, close)

dataPlot.addMajorLineData(414, times, close, 'EURUSD', 'k')
dataPlot.addMajorLineData(414, times, ichimokuValues["tenkan_sen"], 'tenkan_sen', 'r')
dataPlot.addMinorLineData(414, times, ichimokuValues["kijun_sen"], 'kijun_sen', 'b')
dataPlot.addMinorLineData(414, times, ichimokuValues["senkou_span_a"], 'senkou_span_a', 'g')
dataPlot.addMinorLineData(414, times, ichimokuValues["senkou_span_b"], 'senkou_span_b', 'r')
dataPlot.addMinorLineData(414, times, ichimokuValues["chikou_span"], 'chikou_span', 'g')

xxx = numpy.arange(len(times))

dataPlot.getPlot().fill_between(xxx,
  ichimokuValues["senkou_span_a"], ichimokuValues["senkou_span_b"],
  where= ichimokuValues["senkou_span_a"] > ichimokuValues["senkou_span_b"],  color='g', alpha=0.7)

dataPlot.getPlot().fill_between(xxx,
  ichimokuValues["senkou_span_a"], ichimokuValues["senkou_span_b"],
  where= ichimokuValues["senkou_span_b"] > ichimokuValues["senkou_span_a"],  color='r', alpha=0.7)

dataPlot.showPlot()




# create a dataset for classification
trainPercentage = 0.1
nSamples = len(times)
nFeatures = 4


# data[0] = macdSignals[0:trainSize]
# data[1] = ma100Cross[0:trainSize]
# data[2] = mfiCross[0:trainSize]
# data[3] = mfiCross[0:trainSize]



df = pd.DataFrame(data = numpy.zeros((nSamples, nFeatures)), columns=['macd', 'ma100Cross', 'ma200Cross', 'mfiCross'])

df['macd'] = macdSignals
df['ma100Cross'] = ma100Cross
df['ma200Cross'] = ma200Cross
df['mfiCross'] = mfiCross

#y = numpy.zeros(nSamples)
# t1 0 1 0 0 -1
# t2
# t3
#print df


layoutLoad = DatasetLayoutLoad()
layoutLoad.load("../data/layout.xlsx")
QUALITY_TYPES = [1, 2]
yLabling = layoutLoad.transformToLayout(times, QUALITY_TYPES)

# check
for i in range (0, len(yLabling)):
    if yLabling[i] != 0:
        print yLabling[i], times[i]

print "Started: learning phase"
dtCls = DTClassifier()
acc = dtCls.trainAndTest(df, yLabling, 0.3)
print "Accuracy  " + str(acc)


#features  = list(df.columns[:4])
#DTClassifier.visualize_tree(tree, features)


testDF = pd.DataFrame()
testDF['col1'] = [1,2,3,4]
testDF['col2'] = [6,7,8,8]
print "Finished: learning phase"