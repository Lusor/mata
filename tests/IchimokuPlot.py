from datetime import datetime

import numpy
import talib
import pandas as pd

from data_loader.SQLiteAdapter import SQLiteAdapter
from data_visual.DataPlot import DataPlot
from data_visual.SimplePlot import SimplePlot
from indicators.ichimoku import Ichimoku
from signals.IchimokuFeatures import IchimokuFeatures, ICHIMOKU_FTR
from signals.MACDFeatures import MACDFeatures
from signals.Signals import Signals


def transform(signal):
    signal = [float(x) / 100.0 + 1.34 for x in signal]
    signal = pd.Series(signal)
    return signal


crossID = 1
timeframe = 15
startDate = datetime(2014, 1, 1)
endDate = datetime(2014, 1, 10)

# startDate = datetime(2016, 8, 1)
# endDate = datetime(2016, 12, 31)

# currencyData = DukascopyCSV.read('EURUSD_Candlestick_1_M_BID_01.03.2018-31.03.2018.csv')

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

dataPlot = DataPlot()
dataPlot.initFigures(3)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

# times = [datetime.combine(x.Date, x.Time) for x in quotes]
times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]

macd, macdsignal, macdhist = talib.MACD(numpy.asarray(close), fastperiod=12, slowperiod=26, signalperiod=9)

crosses = Signals.lineCrossOverW(macd, macdsignal, 0)
print crosses
# macdSignals = Signals.macdLineCross(macd, macdsignal, 0, 0)
macdSellSignals = Signals.selectSignals(macd, crosses, -1)
macdBuySignals = Signals.selectSignals(macd, crosses, +1)

# macdFeatures = MACDFeatures()
# macdFeatures.setData(macd, macdsignal, macdhist)
# features = macdFeatures.getFeatures()
#
# trend = MACDFeatures.trendDetection(macd, 3)
# print trend
# trend = [x*0.01 for x in trend]

ichimoku = Ichimoku()
ichimokuValues = ichimoku.compute(high, low, close)

senkou_a = ichimokuValues["senkou_span_a"].shift(26)
senkou_b = ichimokuValues["senkou_span_b"].shift(26)
chikou = ichimokuValues["chikou_span"].shift(-26)

ichimokuFeaturesObj = IchimokuFeatures(close, ichimokuValues["tenkan_sen"],
                                       ichimokuValues["kijun_sen"], ichimokuValues["chikou_span"],
                                       ichimokuValues["senkou_span_a"], ichimokuValues["senkou_span_b"])
ichimokuFeatures = ichimokuFeaturesObj.getFeatures(
    [ICHIMOKU_FTR.PRICE_IN_CLOUD, ICHIMOKU_FTR.PRICE_TENKAN_LESS_TENKAN_CLOUD, ICHIMOKU_FTR.TENKAN_KIJUN_LESS_KIJUN_CLOUD])
priceInCloud = ichimokuFeatures[ICHIMOKU_FTR.PRICE_IN_CLOUD]
priceInCloud = transform(priceInCloud)

PRICE_TENKAN_LESS_TENKAN_CLOUD = transform(ichimokuFeatures[ICHIMOKU_FTR.PRICE_TENKAN_LESS_TENKAN_CLOUD])

TENKAN_KIJUN_LESS_KIJUN_CLOUD = transform(ichimokuFeatures[ICHIMOKU_FTR.TENKAN_KIJUN_LESS_KIJUN_CLOUD])

simplePlot = SimplePlot()
simplePlot.addMajorLineData(times, close, 'EURUSD', 'k')
simplePlot.addMajorLineData(times, ichimokuValues["tenkan_sen"], 'tenkan_sen', 'r')
simplePlot.addMinorLineData(times, ichimokuValues["kijun_sen"], 'kijun_sen', 'b')
simplePlot.addMinorLineData(times, senkou_a, 'senkou_span_a', 'g')
simplePlot.addMinorLineData(times, senkou_b, 'senkou_span_b', 'r')
simplePlot.addMinorLineData(times, chikou, 'chikou_span', 'g')
simplePlot.addMinorLineData(times, priceInCloud, 'priceInCloud', 'k')
#simplePlot.addMinorLineData(times, PRICE_TENKAN_LESS_TENKAN_CLOUD, 'PRICE_TENKAN_LESS_TENKAN_CLOUD', 'k')
#simplePlot.addMinorLineData(times, TENKAN_KIJUN_LESS_KIJUN_CLOUD, 'TENKAN_KIJUN_LESS_KIJUN_CLOUD', 'k')
# simplePlot.addMinorLineData( times, priceinCLD, 'priceinCLD', 'm')
xxx = numpy.arange(len(times))

simplePlot.getPlot().fill_between(xxx, senkou_a, senkou_b,
                                  where=senkou_a > senkou_b, color='g', alpha=0.7)

simplePlot.getPlot().fill_between(xxx, senkou_a, senkou_b,
                                  where=senkou_b > senkou_a, color='r', alpha=0.7)

# simplePlot.addMajorLineData(times, macd, 'MACD')
# simplePlot.addMinorLineData(times, trend, 'Trend', 'k')
# simplePlot.addMinorLineData(times, macdsignal, 'MACD-signal', 'r')
# simplePlot.addMinorSignalData(times, macdSellSignals, 'v','sell signals', 'm')
# simplePlot.addMinorSignalData(times, macdBuySignals, '^','buy signals', 'm')
simplePlot.showPlot()
