from datetime import datetime

import numpy
import talib
import pandas as pd

from data_loader.SQLiteAdapter import SQLiteAdapter
from data_visual.DataPlot import DataPlot
from data_visual.SimplePlot import SimplePlot
from indicators.Envelopes import Envelopes
from indicators.ichimoku import Ichimoku
from signals.IchimokuFeatures import IchimokuFeatures, ICHIMOKU_FTR
from signals.MACDFeatures import MACDFeatures
from signals.Signals import Signals


def transform(signal):
    signal = [float(x) / 10.0 + 1.0 for x in signal]
    signal = pd.Series(signal)
    return signal


crossID = 1
timeframe = 15
startDate = datetime(2017, 1, 1)
endDate = datetime(2017, 2, 1)

# startDate = datetime(2016, 8, 1)
# endDate = datetime(2016, 12, 31)

# currencyData = DukascopyCSV.read('EURUSD_Candlestick_1_M_BID_01.03.2018-31.03.2018.csv')

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

dataPlot = DataPlot()
dataPlot.initFigures(3)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

# times = [datetime.combine(x.Date, x.Time) for x in quotes]
times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]


(bottomLine, topLine) = Envelopes.getLines(low, high, 8, 0.22)

price_cross_env_top_up, price_cross_env_top_down = Signals.lineCrossOverW_directions(close, topLine, 0)
price_cross_env_bottom_up, price_cross_env_bottom_down= Signals.lineCrossOverW_directions(close, bottomLine, 0)
price_cross_env_top_down = transform(price_cross_env_top_down)
price_cross_env_bottom_up = transform(price_cross_env_bottom_up)

simplePlot = SimplePlot()
simplePlot.addMajorLineData(times, close, 'EURUSD', 'k')
simplePlot.addMajorLineData(times, bottomLine, 'low', 'r')
simplePlot.addMajorLineData(times, topLine, 'top', 'b')
simplePlot.addMinorLineData(times, price_cross_env_bottom_up, 'price_cross_env_top_down', 'g')

#simplePlot.addMinorLineData(times, priceInCloud, 'priceInCloud', 'k')


xxx = numpy.arange(len(times))

simplePlot.showPlot()
