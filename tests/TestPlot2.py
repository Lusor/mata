from datetime import datetime

import numpy
import numpy as np
import talib
from matplotlib.ticker import FuncFormatter

from data_loader.SQLiteAdapter import SQLiteAdapter
from data_visual.DataPlot import DataPlot
from indicators.ichimoku import Ichimoku
from signals.Signals import Signals

import matplotlib.pyplot as plt

__author__ = 'VB7'

def equidate_ax(fig, ax, dates, fmt="%Y-%m-%d", label="Date"):
    """
    Sets all relevant parameters for an equidistant date-x-axis.
    Tick Locators are not affected (set automatically)

    Args:
        fig: pyplot.figure instance
        ax: pyplot.axis instance (target axis)
        dates: iterable of datetime.date or datetime.datetime instances
        fmt: Display format of dates
        label: x-axis label
    Returns:
        None

    """
    N = len(dates)
    def format_date(index, pos):
        index = np.clip(int(index + 0.5), 0, N - 1)
        return dates[index].strftime(fmt)
    ax.xaxis.set_major_formatter(FuncFormatter(format_date))
    ax.set_xlabel(label)
    fig.autofmt_xdate()
    return



crossID= 1
timeframe = 60
startDate = datetime(2018, 1, 1)
endDate = datetime(2018, 5, 1)

#currencyData = DukascopyCSV.read('EURUSD_Candlestick_1_M_BID_01.03.2018-31.03.2018.csv')

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(crossID, timeframe, startDate, endDate)

dataPlot = DataPlot()
dataPlot.initFigures(3)

print len(quotes)

close = [x.Close for x in quotes]
high = [x.High for x in quotes]
low = [x.Low for x in quotes]
volume = [x.Volume for x in quotes]

dates = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]

# fig, [ax, ax2] = plt.subplots(1, 2)
# x = np.arange(len(dates))
# ax.plot(x, close, 'o-')
# equidate_ax(fig, ax, dates)
#
# plt.show()

rsi = talib.RSI(numpy.asarray(close), timeperiod=14)

dataPlot = DataPlot()
dataPlot.initFigures(1)

dataPlot.addMajorLineData(411, dates, close, 'EURUSD')
dataPlot.addMinorLineData(411, dates, rsi, 'RSI', 'r')

dataPlot.showPlot()
