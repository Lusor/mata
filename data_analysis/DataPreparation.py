import numpy
import pandas as pd
import talib

from indicators.ichimoku import Ichimoku
from signals.IchimokuFeatures import IchimokuFeatures
from signals.MACDFeatures import MACDFeatures
from signals.MFIFeatures import MFIFeatures
from signals.RSIFeatures import RSIFeatures
from signals.SMAFeatures import SMAFeatures


class DataPreparation:

    def setData(self, _low, _high, _close, _volume):
        self.low = _low
        self.high = _high
        self.close = _close
        self.volume = _volume
        return


    def formDataSet(self):

        # result
        df = pd.DataFrame()

        # sma
        sma100 = talib.SMA(numpy.asarray(self.close), timeperiod=100)
        sma200 = talib.SMA(numpy.asarray(self.close), timeperiod=200)
        sma100FeaturesObj = SMAFeatures(self.close, sma100)
        sma200FeaturesObj = SMAFeatures(self.close, sma200)
        # so, we return a map
        sma100Features = sma100FeaturesObj.getFeatures()
        sma200Features = sma200FeaturesObj.getFeatures()


        # macd
        macd, macdsignal, macdhist = talib.MACD(numpy.asarray(self.close), fastperiod=12, slowperiod=26, signalperiod=9)
        macdFeaturesObj = MACDFeatures(macd, macdsignal, macdhist)
        macdFeatures = macdFeaturesObj.getFeatures()

        # mfi
        mfi = talib.MFI(numpy.asarray(self.high), numpy.asarray(self.low), numpy.asarray(self.close), numpy.asarray(self.volume), timeperiod=14)
        mfiFeaturesObj = MFIFeatures(self.close, mfi)
        mfiFeatures = mfiFeaturesObj.getFeatures()

        # rsi
        rsi = talib.RSI(numpy.asarray(self.close), timeperiod=14)
        rsiFeaturesObj = RSIFeatures(self.close, rsi)
        rsiFeatures = rsiFeaturesObj.getFeatures()

        ichimoku = Ichimoku()
        ichimokuValues = ichimoku.compute(self.high, self.low, self.close)
        ichimokuFeaturesObj = IchimokuFeatures(self.close, ichimokuValues["tenkan_sen"],
                                               ichimokuValues["kijun_sen"], ichimokuValues["chikou_span"],
                                               ichimokuValues["senkou_span_a"], ichimokuValues["senkou_span_b"])
        ichimokuFeatures = ichimokuFeaturesObj.getFeatures()


        # add all features to the dataset
        self.addToDataSet(df, sma100Features)
        self.addToDataSet(df, sma200Features)
        self.addToDataSet(df, macdFeatures)
        self.addToDataSet(df, mfiFeatures)
        self.addToDataSet(df, rsiFeatures)
        self.addToDataSet(df, ichimokuFeatures)

        return df

    def blur(self):

        return

    @staticmethod
    def addToDataSet(df, featuresSets):
        for featureName in featuresSets:
            #print featureName
            featuresFloat = [float(i) for i in featuresSets[featureName]]
            df[featureName] = featuresFloat
        return