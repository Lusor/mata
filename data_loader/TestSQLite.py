from datetime import datetime
from sqlalchemy import Date
from data_loader.SQLiteAdapter import SQLiteAdapter

__author__ = 'VB7'


adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotes = adapter.getQuotations(1, 60, datetime(2018, 2, 1), datetime(2018,3,1))


#print datetime(2018, 2, 1).strftime("%Y-%m-%d")

for quote in quotes:
    print str(quote.Date) + " " + str(quote.Time)