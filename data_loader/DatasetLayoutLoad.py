from datetime import datetime, timedelta

import os
import pandas as pd
import xlrd
from sqlalchemy import Enum

class SignalInfo:
    # format
    # signal	entry_period	best_entry	exit_period	best_exit	signal_quality

    def __init__(self, datetimeObj, signalType, signalQuality):
        self.datetimeObj = datetimeObj
        self.signalType = signalType
        self.signalQuality = signalQuality

class SignalType(Enum):
    BUY = 1
    SELL = -1
    NOTHING = 0

class DatasetLayoutLoad:

    signalMap = {}

    def load(self, filename):

        if not os.path.isfile(filename):
            raise Exception("File not found")

        self.signalMap = {}
        df = pd.read_excel(filename)

        for index, row in df.iterrows():
            signalType = 0
            if row['signal'].upper() == "BUY":
                signalType = SignalType.BUY
            else:
                signalType = SignalType.SELL

            (startEntryDate, endEntryDate) = self.getDatesFromStr(row['entry_period'])
            bestEntryDate = datetime.strptime(row['best_entry'] + ' ' + "00:00", '%Y.%m.%d %H:%M')
            signalQuality = int(row['signal_quality'])

            delta = endEntryDate - startEntryDate
            for i in range(0, delta.days+1):
                datetimeKey = startEntryDate + timedelta(days=i)
                self.signalMap[datetimeKey] = SignalInfo(datetimeKey, signalType, signalQuality)
                # print str(datetimeKey)

        #print self.signalMap
        return True


    # get Y for certain xValues (dates) and list of signalTypes
    def transformToLayout(self, xTimes, signalQualities):
        if len(self.signalMap) == 0:
            raise Exception("Signal list is not loaded")

        resultList = [0]*len(xTimes)
        for i in range(0, len(xTimes)):
            key = xTimes[i]
            if key in self.signalMap:
                signalInfo = self.signalMap[key]
                if signalInfo.signalQuality in signalQualities:
                    resultList[i] = signalInfo.signalType
        return resultList


    @staticmethod
    def getDatesFromStr(periodStr):
        datesStrArray = periodStr.split('-')
        start = datetime.strptime(datesStrArray[0] + ' ' + "00:00", '%Y.%m.%d %H:%M')
        end = datetime.strptime(datesStrArray[1] + ' ' + "00:00", '%Y.%m.%d %H:%M')
        if start > end:
            raise Exception('The starting entry date cannot be after the ending entry date: ' + str(start))
        return (start, end)




















