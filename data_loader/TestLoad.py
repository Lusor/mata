import datetime
from data_loader.DukascopyCSV import DukascopyCSV
from data_loader.SQLiteAdapter import SQLiteAdapter
from database_schema.SQLiteFinDBModel import Quotation

__author__ = 'VB7'

adapter = SQLiteAdapter()
adapter.connect("sqlite:///../data/findb.s3db")

quotation = Quotation()
quotation.Open = 10
quotation.Close = 9
quotation.High = 8
quotation.Low = 7
quotation.Volume = 6
quotation.CrossID = 1

quotation.Date = datetime.date(2002, 12, 26)
quotation.Time = datetime.time(15, 35, 0)
#quotation.MaxTimeFrame = 5;

#loader.addQuotation(quotation)

print datetime.datetime.combine(quotation.Date, quotation.Time)

#quotation.Date. + quotation.Time

crosses = adapter.getCrosses()

crossID = adapter.getCrossID("EURUSD")


# get time frames
timeframes = adapter.getTimeframes()
for frame in timeframes:
    print frame.Name

times = [x for x in range(len(timeframes))]
print times

#DukascopyCSV.read('EURUSD_Candlestick_1_M_BID_01.03.2018-31.03.2018.csv')