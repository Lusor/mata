import codecs
from datetime import datetime
#from data_loader.CassandraAdapter import CassandraAdapter
import os

from data_loader.SQLiteAdapter import SQLiteAdapter
from database_schema.SQLiteFinDBModel import Quotation

__author__ = 'VB7'

import csv

class AlpariCSV:
    @staticmethod
    def read(filepath):
        array = []
        # with open(filepath, 'rb') as csvfile:
        #     reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        #     next(reader, None)
        #     for line in reader:
        #         currencyPair = AlpariCSV.lineToCurrencyPair(line)
        #         array.append(currencyPair)
        # return array
        reader = codecs.open(filepath, 'rU', 'utf-16')
        #next(reader, None)
        for line in reader:
            lineElements = line.split(',')
            currencyPair = AlpariCSV.lineToCurrencyPair(lineElements)
            array.append(currencyPair)
        return array
    @staticmethod
    def lineToCurrencyPair(lineElements):

        # time,Open,High,Low,Close,Volume
        quotation = Quotation()

        if len(lineElements[0]) > 11:
            pairDatetime = datetime.strptime(lineElements[0], '%Y.%m.%d %H:%M')
        else:
            pairDatetime = datetime.strptime(lineElements[0], '%Y.%m.%d')
        quotation.Date = pairDatetime.date()
        quotation.Time = pairDatetime.time()
        quotation.Open = float(lineElements[1])
        quotation.High = float(lineElements[2])
        quotation.Low = float(lineElements[3])
        quotation.Close = float(lineElements[4])
        quotation.Volume = float(lineElements[5])

        return quotation

    @staticmethod
    def saveToSQLiteDB(filePath, dbPath, quotationType, timeframeInMin):
        # connect to db
        adapter = SQLiteAdapter()
        adapter.connect(dbPath)
        # get crossID related to quotationType
        crossID = adapter.getCrossID(quotationType)
        if crossID == None:
            raise ValueError('Quotation type is not found')

        print "Started: reading a csv file"
        quotationArray = AlpariCSV.read(filePath)
        print "Finished: reading a csv file"

        count = 0
        for quotation in quotationArray:
            quotation.TimeFrame = timeframeInMin
            quotation.CrossID = crossID
            adapter.addQuotation(quotation)
            count +=1
            print count
        return True

    @staticmethod
    def removeAndAddQuotes(filePath, dbPath, quotationType, timeframeInMin):
        # connect to db
        adapter = SQLiteAdapter()
        adapter.connect(dbPath)
        # get crossID related to quotationType
        crossID = adapter.getCrossID(quotationType)
        if crossID == None:
            raise ValueError('Quotation type is not found')

        adapter.removeQuotations(crossID, timeframeInMin)

        print "Started: reading a csv file"
        quotationArray = AlpariCSV.read(filePath)
        print "Finished: reading a csv file"

        BATCH_SIZE = 10000
        quotationBatch = []
        count = 0
        for quotation in quotationArray:
            quotation.TimeFrame = timeframeInMin
            quotation.CrossID = crossID
            quotationBatch.append(quotation)
            count += 1
            print count
            if len(quotationBatch) == BATCH_SIZE:
                adapter.addQuotationList(quotationBatch)
                quotationBatch = []
                print "added batch " + str(BATCH_SIZE)

        adapter.addQuotationList(quotationBatch)
        return True

    @staticmethod
    def convertTimeframeToMinutes(_timeframeStr):
        if _timeframeStr.startswith('i'):
            timeframeStr = _timeframeStr[1:]
        minutes = 0
        if timeframeStr == "M5":
            minutes = 5
        if timeframeStr == "M15":
            minutes = 15
        if timeframeStr == "M30":
            minutes = 30
        elif timeframeStr == "H1":
            minutes = 60
        elif timeframeStr == "H4":
            minutes = 240
        elif timeframeStr == "Daily":
            minutes = 1440
        return minutes


    @staticmethod
    def loadFiles(folderPath, dbPath, quotationType):
        if not dbPath.__contains__(quotationType):
            print "The current database is not for " + quotationType
            return

        for fullFilename in os.listdir(folderPath):
            if fullFilename.endswith(".csv"):
                filename, file_extension = os.path.splitext(fullFilename)
                parts =filename.split('_')
                print os.path.abspath(os.path.join(folderPath, fullFilename))

                if quotationType == parts[0]:
                    print "export: " + os.path.abspath(os.path.join(folderPath, fullFilename))
                    timeframeInMin = AlpariCSV.convertTimeframeToMinutes(parts[1])
                    absFilePath = os.path.abspath(os.path.join(folderPath, fullFilename))
                    AlpariCSV.removeAndAddQuotes(absFilePath, dbPath, quotationType, timeframeInMin)
                    print "done: " + os.path.abspath(os.path.join(folderPath, fullFilename))

        return True

