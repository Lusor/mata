from datetime import datetime
from sqlalchemy.orm import sessionmaker
from sqlalchemy.util.langhelpers import NoneType
from database_schema.SQLiteFinDBModel import Cross, Timeframe, Quotation

__author__ = 'VB7'

from sqlalchemy import *

class SQLiteAdapter:

    # 'sqlite:///findb.s3db'
    engine = None
    def connect(self, datebasePath):
        self.engine = create_engine(datebasePath)

    def getCrossID(self, crossName):
        Session  = sessionmaker(bind=self.engine)
        session = Session()
        cross = session.query(Cross).filter_by(Name=crossName).first()
        if cross != None:
            return cross.ID
        return None

    def getCrosses(self):
        Session  = sessionmaker(bind=self.engine)
        session = Session()
        crosses = session.query(Cross).all()
        return crosses

    def getTimeframes(self):
        Session  = sessionmaker(bind=self.engine)
        session = Session()
        timeframes = session.query(Timeframe).all()
        return timeframes

    def addQuotationList(self, quotationList):
        Session = sessionmaker(bind=self.engine)
        session = Session()

        for quotation in quotationList:
            session.add(quotation)
        session.commit()
        return True

    def addQuotation(self, quotation):
        Session  = sessionmaker(bind=self.engine)
        session = Session()

        # check if exists
        foundItem = session.query(Quotation).filter_by(Date=quotation.Date, Time=quotation.Time,
                                                       CrossID=quotation.CrossID, TimeFrame=quotation.TimeFrame).first()
        if foundItem != None:
            print "already existed " + str(quotation.Date) + " " + str(quotation.Time)
            return False

        session.add(quotation)
        session.commit()
        return True

    def removeQuotations(self, crossID, timeFrame):
        Session = sessionmaker(bind=self.engine)
        session = Session()
        session.query(Quotation).filter(Quotation.CrossID==crossID, Quotation.TimeFrame==timeFrame).delete()
        session.commit()

    def getQuotations(self, crossName, timeFrame, startDate, endDate):
        Session  = sessionmaker(bind=self.engine)
        session = Session()
        # find cross id
        crossID = self.getCrossID(crossName)
        if crossID == None:
            print "cross not found" + crossName
            return []
        foundQuots = session.query(Quotation).filter(Quotation.CrossID==crossID,
                                                     Quotation.TimeFrame == timeFrame,
                                                     Quotation.TimeFrame==timeFrame,
                                                     Quotation.Date>=startDate.strftime("%Y-%m-%d"),
                                                     Quotation.Date<endDate.strftime("%Y-%m-%d")).all()

        # or_(and_(Quotation.Volume > 50000, Quotation.TimeFrame == 1440), and_(Quotation.Volume > 100, Quotation.TimeFrame == 60)),
        return foundQuots

# 3 - extract all movies
#movies = session.query(Movie).all()
