from sqlalchemy.orm import sessionmaker
from sqlalchemy.util.langhelpers import NoneType
from database_schema.CassandraFinDBModel import Quotations
from database_schema.SQLiteFinDBModel import Cross, Timeframe, Quotation

__author__ = 'VB7'

from cassandra.cqlengine import connection

class CassandraAdapter:

    # '127.0.0.1'
    def connect(self, address):
        connection.setup([address], "findb", protocol_version=3)


    def addQuotation(self, datetime, cross_type, max_time_frame, high, low, open, close, volume):

  #      count = Quotations.objects.filter(datatime=datetime, cross_type=cross_type).count()
  #      if count > 0:
  #          print "already existed " + str(datetime)
  #          return False

        Quotations.create(datetime=datetime,
                         cross_type=cross_type,
                         max_time_frame=max_time_frame,
                         high=high,
                         low=low,
                         open=open,
                         close=close,
                         volume=volume)
        return True


    def getQuotations(self, crossType):

        q = Quotations.objects.filter(max_time_frame=0)
     #   q = q.filter(max_time_frame=0)
        filteredQuots = q.limit(5).all()
        return filteredQuots



# 3 - extract all movies
#movies = session.query(Movie).all()
