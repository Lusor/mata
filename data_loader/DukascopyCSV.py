from datetime import datetime
#from data_loader.CassandraAdapter import CassandraAdapter
from data_loader.SQLiteAdapter import SQLiteAdapter
from database_schema.SQLiteFinDBModel import Quotation

__author__ = 'VB7'

import csv

class DukascopyCSV:
#    def __init__(self):

    @staticmethod
    def read(filepath):
        array = []
        with open(filepath, 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            next(spamreader, None)
            for line in spamreader:
                #print ', '.join(line)

                currencyPair = DukascopyCSV.lineToCurrencyPair(line)
                array.append(currencyPair)
        return array

    @staticmethod
    def lineToCurrencyPair(lineElements):

        # Gmt time,Open,High,Low,Close,Volume
        quotation = Quotation()

        pairDatetime = datetime.strptime(lineElements[0], '%d.%m.%Y %H:%M:%S.%f')
        quotation.Date = pairDatetime.date()
        quotation.Time = pairDatetime.time()
        quotation.Open = float(lineElements[1])
        quotation.High = float(lineElements[2])
        quotation.Low = float(lineElements[3])
        quotation.Close = float(lineElements[4])
        quotation.Volume = float(lineElements[5])

        return quotation

    @staticmethod
    def saveToSQLiteDB(filePath, dbPath, quotationType, setTimeFrame):

        # connect to db
        adapter = SQLiteAdapter()
        adapter.connect(dbPath)
        # get crossID related to quotationType
        crossID = adapter.getCrossID(quotationType)
        if crossID == None:
            raise ValueError('Quotation type is not found')

        print "Started: reading a csv file"
        quotationArray = DukascopyCSV.read(filePath)
        print "Finished: reading a csv file"

        count = 0
        for quotation in quotationArray:
            quotation.TimeFrame = setTimeFrame
            quotation.CrossID = crossID
            adapter.addQuotation(quotation)
            count +=1
            print count
        return True

