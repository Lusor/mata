update Quotations
set MaxTimeFrame = 60
where CrossID = 1 and (60*strftime('%H', Time) + strftime('%M', Time)) % 60 ==0


update Quotations
set MaxTimeFrame = 240
where CrossID = 1 and (60*strftime('%H', Time) + strftime('%M', Time)) % 240 ==0


update Quotations
set MaxTimeFrame = 1440
where CrossID = 1 and (60*strftime('%H', Time) + strftime('%M', Time)) % 1440 ==0


Optimize size:
VACUUM and REINDEX

How to generate model from database:
# cd C:\Programs\Python\Python27\Scripts
# C:\Programs\Python\Python27\Scripts>flask-sqlacodegen --flask --outfile FinDBModel.py sqlite:///findb.s3db

