import uuid
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model



__author__ = 'VB7'

class Crosses(Model):
    id = columns.UUID(primary_key=True, default=uuid.uuid1)
    name = columns.Text(index=True)


class Quotations(Model):
    cross_type = columns.Text(primary_key=True)
    datetime = columns.DateTime(primary_key=True)
    max_time_frame = columns.Integer(default = 1, index=True)
    high = columns.Float()
    low = columns.Float()
    open = columns.Float()
    close = columns.Float()
    volume = columns.Float()

