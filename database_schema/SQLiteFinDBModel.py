# coding: utf-8
from sqlalchemy import BigInteger, Boolean, Column, DateTime, Float, ForeignKey, Integer, LargeBinary, String, Table, Unicode, Date, Time
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import NullType
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Cross(Base):
    __tablename__ = 'Crosses'
    ID = Column(Integer, primary_key=True, autoincrement=True)
    Name = Column(String, nullable=False)


class Quotation(Base):
    __tablename__ = 'Quotations'

#    ID = Column(BigInteger, primary_key=True, autoincrement=True)
    CrossID = Column(ForeignKey(u'Crosses.ID'), primary_key=True, nullable=False)
    Date = Column(Date, primary_key=True, nullable=False)
    Time = Column(Time, primary_key=True, nullable=False)
    High = Column(Float)
    Low = Column(Float)
    Open = Column(Float)
    Close = Column(Float)
    Volume = Column(Float)
    TimeFrame = Column(Integer)

    Cross = relationship(u'Cross', primaryjoin='Quotation.CrossID == Cross.ID', backref=u'quotations')


class Indicator(Base):
    __tablename__ = 'Indicators'

    CrossID = Column(ForeignKey(u'Crosses.ID'), primary_key=True, nullable=False)
    Date = Column(Date, primary_key=True, nullable=False)
    Time = Column(Time, primary_key=True, nullable=False)
    Timeframe = Column(Integer, primary_key=True, nullable=False)
    MA_100 = Column(Float)

    Cross = relationship(u'Cross', primaryjoin='Indicator.CrossID == Cross.ID', backref=u'indicators')

class Timeframe(Base):
    __tablename__ = 'Timeframes'

    ID = Column(Integer, primary_key=True, autoincrement=True)
    Name = Column(String, nullable=False, unique=True)
    Period = Column(Integer, nullable=False, unique=True)


t_sqlite_sequence = Table(
    'sqlite_sequence',
    Base.metadata,
    Column('name', NullType),
    Column('seq', NullType)
)
