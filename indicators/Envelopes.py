import numpy as np
import talib


class Envelopes:
    @staticmethod
    def getLines(low, high, envelopePeriod, deviation):
        smaHigh = talib.SMA(np.asarray(high), timeperiod=envelopePeriod)
        smaLow = talib.SMA(np.asarray(low), timeperiod=envelopePeriod)
        # medianPrice
        smaMedian = (smaHigh + smaLow)/2


        sma_LB = smaMedian*(1.0 - deviation/100.0)
        sma_TB = smaMedian*(1.0 + deviation/100.0)
        return (sma_LB, sma_TB)