<chart>
id=131803589269500514
symbol=EURUSD
period_type=1
period_size=24
digits=5
tick_size=0.000000
position_time=1113523200
scale_fix=0
scale_fixed_min=1.184800
scale_fixed_max=1.370300
scale_fix11=0
scale_bar=0
scale_bar_val=1.000000
scale=8
mode=1
fore=0
grid=1
volume=0
scroll=0
shift=0
shift_size=19.183235
fixed_pos=0.000000
ohlc=0
one_click=0
one_click_btn=0
bidline=1
askline=1
lastline=0
days=0
descriptions=0
tradelines=1
window_left=130
window_top=130
window_right=1648
window_bottom=478
window_type=3
floating=0
floating_left=0
floating_top=0
floating_right=0
floating_bottom=0
floating_type=1
floating_toolbar=1
floating_tbstate=
background_color=16777215
foreground_color=0
barup_color=0
bardown_color=0
bullcandle_color=16777215
bearcandle_color=0
chartline_color=0
volumes_color=32768
grid_color=12632256
bidline_color=12632256
askline_color=12632256
lastline_color=12632256
stops_color=17919
windows_total=1

<window>
height=100.000000
objects=106

<indicator>
name=Main
path=
apply=1
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=0
fixed_height=-1
</indicator>

<indicator>
name=Ichimoku Kinko Hyo
path=
apply=0
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=0
fixed_height=-1

<graph>
name=
draw=128
style=0
width=1
arrow=251
color=255
</graph>

<graph>
name=
draw=128
style=0
width=1
arrow=251
color=16711680
</graph>

<graph>
name=
draw=128
style=2
width=1
arrow=251
shift=26
color=2263842
</graph>

<graph>
name=
draw=128
style=2
width=1
arrow=251
shift=26
color=17919
</graph>

<graph>
name=
draw=128
style=0
width=1
arrow=251
color=65280
</graph>
tenkan=9
kijun=26
senkou=52
</indicator>
<object>
type=109
name=2018.11.26 09:00 FI: Export Prices (YoY)
hidden=1
descr=FI: Export Prices (YoY)
color=16119285
selectable=0
date1=1543222800
</object>

<object>
type=109
name=2018.11.26 09:00 FI: Import Prices (YoY)
hidden=1
descr=FI: Import Prices (YoY)
color=16119285
selectable=0
date1=1543222800
</object>

<object>
type=109
name=2018.11.26 09:00 FI: Producer Price Index (YoY)
hidden=1
descr=FI: Producer Price Index (YoY)
color=16119285
selectable=0
date1=1543222800
</object>

<object>
type=109
name=2018.11.26 09:45 FR: Producer Prices (MoM)
hidden=1
descr=FR: Producer Prices (MoM)
color=16119285
selectable=0
date1=1543225500
</object>

<object>
type=109
name=2018.11.26 11:00 DE: IFO - Business Climate
hidden=1
descr=DE: IFO - Business Climate
color=16119285
selectable=0
date1=1543230000
</object>

<object>
type=109
name=2018.11.26 11:00 DE: IFO - Current Assessment
hidden=1
descr=DE: IFO - Current Assessment
color=16119285
selectable=0
date1=1543230000
</object>

<object>
type=109
name=2018.11.26 11:00 DE: IFO - Expectations
hidden=1
descr=DE: IFO - Expectations
color=16119285
selectable=0
date1=1543230000
</object>

<object>
type=109
name=2018.11.26 15:30 Chicago Fed National Activity Index
hidden=1
descr=Chicago Fed National Activity Index
color=16119285
selectable=0
date1=1543246200
</object>

<object>
type=109
name=2018.11.26 17:30 Dallas Fed Manufacturing Business Index
hidden=1
descr=Dallas Fed Manufacturing Business Index
color=16119285
selectable=0
date1=1543253400
</object>

<object>
type=109
name=2018.11.26 18:30 3-Month Bill Auction
hidden=1
descr=3-Month Bill Auction
color=16119285
selectable=0
date1=1543257000
</object>

<object>
type=109
name=2018.11.26 18:30 6-Month Bill Auction
hidden=1
descr=6-Month Bill Auction
color=16119285
selectable=0
date1=1543257000
</object>

<object>
type=109
name=2018.11.26 20:00 2-Year Note Auction
hidden=1
descr=2-Year Note Auction
color=16119285
selectable=0
date1=1543262400
</object>

<object>
type=109
name=2018.11.26 22:30 EMU: CFTC EUR NC net positions
hidden=1
descr=EMU: CFTC EUR NC net positions
color=16119285
selectable=0
date1=1543271400
</object>

<object>
type=109
name=2018.11.26 22:30 CFTC Gold NC net positions
hidden=1
descr=CFTC Gold NC net positions
color=16119285
selectable=0
date1=1543271400
</object>

<object>
type=109
name=2018.11.26 22:30 CFTC Oil NC net positions
hidden=1
descr=CFTC Oil NC net positions
color=16119285
selectable=0
date1=1543271400
</object>

<object>
type=109
name=2018.11.26 22:30 CFTC USD NC net positions
hidden=1
descr=CFTC USD NC net positions
color=16119285
selectable=0
date1=1543271400
</object>

<object>
type=109
name=2018.12.24 02:00 FI: Christmas Eve
hidden=1
descr=FI: Christmas Eve
color=16119285
selectable=0
date1=1545616800
</object>

<object>
type=109
name=2018.12.24 02:00 PT: Christmas Eve
hidden=1
descr=PT: Christmas Eve
color=16119285
selectable=0
date1=1545616800
</object>

<object>
type=109
name=2018.12.24 02:00 SK: Christmas Eve
hidden=1
descr=SK: Christmas Eve
color=16119285
selectable=0
date1=1545616800
</object>

<object>
type=109
name=2018.12.24 10:30 NL: Gross Domestic Product n.s.a (YoY)
hidden=1
descr=NL: Gross Domestic Product n.s.a (YoY)
color=16119285
selectable=0
date1=1545647400
</object>

<object>
type=109
name=2018.12.24 10:30 NL: Gross Domestic Product s.a (QoQ)
hidden=1
descr=NL: Gross Domestic Product s.a (QoQ)
color=16119285
selectable=0
date1=1545647400
</object>

<object>
type=109
name=2018.12.24 15:30 Chicago Fed National Activity Index
hidden=1
descr=Chicago Fed National Activity Index
color=16119285
selectable=0
date1=1545665400
</object>

<object>
type=109
name=2018.12.24 18:30 3-Month Bill Auction
hidden=1
descr=3-Month Bill Auction
color=16119285
selectable=0
date1=1545676200
</object>

<object>
type=109
name=2018.12.24 18:30 6-Month Bill Auction
hidden=1
descr=6-Month Bill Auction
color=16119285
selectable=0
date1=1545676200
</object>

<object>
type=109
name=2018.12.31 12:00 GR: Retail Sales (YoY)
hidden=1
descr=GR: Retail Sales (YoY) -4.0% / 
color=13353215
selectable=0
date1=1546257600
</object>

<object>
type=109
name=2018.12.31 17:30 Dallas Fed Manufacturing Business Index
hidden=1
descr=Dallas Fed Manufacturing Business Index -5.1 / 
color=13353215
selectable=0
date1=1546277400
</object>

<object>
type=109
name=2018.12.31 18:30 52-Week Bill auction
hidden=1
descr=52-Week Bill auction
color=16119285
selectable=0
date1=1546281000
</object>

<object>
type=109
name=2019.01.03 02:00 ES: 5-y Bond Auction
hidden=1
descr=ES: 5-y Bond Auction
color=16119285
selectable=0
date1=1546480800
</object>

<object>
type=109
name=2019.01.03 14:00 MBA Mortgage Applications
hidden=1
descr=MBA Mortgage Applications -8.5% / 
color=13353215
selectable=0
date1=1546524000
</object>

<object>
type=109
name=2019.01.03 18:30 4-Week Bill Auction
hidden=1
descr=4-Week Bill Auction 2.39% / 
color=15658671
selectable=0
date1=1546540200
</object>

<object>
type=109
name=2019.01.04 02:00 FR: 10-y Bond Auction
hidden=1
descr=FR: 10-y Bond Auction
color=16119285
selectable=0
date1=1546567200
</object>

<object>
type=109
name=2019.01.04 20:30 FOMC member Barkin speech
hidden=1
descr=FOMC member Barkin speech
color=16119285
selectable=0
date1=1546633800
</object>

<object>
type=109
name=2019.01.04 20:30 Fed's Bullard speech
hidden=1
descr=Fed's Bullard speech
color=16119285
selectable=0
date1=1546633800
</object>

<object>
type=109
name=2019.01.02 08:00 IE: Purchasing Manager Index Manufacturing
hidden=1
descr=IE: Purchasing Manager Index Manufacturing 54.5 / 
color=15658671
selectable=0
date1=1546416000
</object>

<object>
type=109
name=2019.01.02 10:00 AT: Unemployment
hidden=1
descr=AT: Unemployment 355.6K / 
color=15658671
selectable=0
date1=1546423200
</object>

<object>
type=109
name=2019.01.02 10:00 AT: Unemployment Rate
hidden=1
descr=AT: Unemployment Rate 8.7% / 
color=15658671
selectable=0
date1=1546423200
</object>

<object>
type=109
name=2019.01.02 10:15 ES: Markit Manufacturing PMI
hidden=1
descr=ES: Markit Manufacturing PMI 51.1 / 52.4
color=13353215
selectable=0
date1=1546424100
</object>

<object>
type=109
name=2019.01.02 10:45 IT: Markit Manufacturing PMI
hidden=1
descr=IT: Markit Manufacturing PMI 49.2 / 48.6
color=15658671
selectable=0
date1=1546425900
</object>

<object>
type=109
name=2019.01.02 10:50 FR: Markit Manufacturing PMI
hidden=1
descr=FR: Markit Manufacturing PMI 49.7 / 49.7
color=15658671
selectable=0
date1=1546426200
</object>

<object>
type=109
name=2019.01.02 10:55 DE: Markit Manufacturing PMI
hidden=1
descr=DE: Markit Manufacturing PMI 51.5 / 51.5
color=15658671
selectable=0
date1=1546426500
</object>

<object>
type=109
name=2019.01.02 11:00 EMU: Markit Manufacturing PMI
hidden=1
descr=EMU: Markit Manufacturing PMI 51.4 / 51.4
color=15658671
selectable=0
date1=1546426800
</object>

<object>
type=109
name=2019.01.02 11:00 GR: Markit Manufacturing PMI
hidden=1
descr=GR: Markit Manufacturing PMI 53.8 / 
color=15658671
selectable=0
date1=1546426800
</object>

<object>
type=109
name=2019.01.02 15:55 Redbook index (MoM)
hidden=1
descr=Redbook index (MoM) 0.6% / 
color=15658671
selectable=0
date1=1546444500
</object>

<object>
type=109
name=2019.01.02 15:55 Redbook index (YoY)
hidden=1
descr=Redbook index (YoY) 9.3% / 
color=15658671
selectable=0
date1=1546444500
</object>

<object>
type=109
name=2019.01.02 16:45 Markit Manufacturing PMI
hidden=1
descr=Markit Manufacturing PMI 53.8 / 
color=15658671
selectable=0
date1=1546447500
</object>

<object>
type=109
name=2019.01.03 02:01 IE: Consumer Confidence
hidden=1
descr=IE: Consumer Confidence
color=16119285
selectable=0
date1=1546480860
</object>

<object>
type=109
name=2019.01.03 11:00 EMU: Private loans (YoY)
hidden=1
descr=EMU: Private loans (YoY) 3.3% / 3.3%
color=15658671
selectable=0
date1=1546513200
</object>

<object>
type=109
name=2019.01.03 15:15 ADP Employment Change
hidden=1
descr=ADP Employment Change 271K / 178K
color=15658671
selectable=0
date1=1546528500
</object>

<object>
type=109
name=2019.01.03 15:30 Initial Jobless Claims
hidden=1
descr=Initial Jobless Claims 231K / 220K
color=15658671
selectable=0
date1=1546529400
</object>

<object>
type=109
name=2019.01.03 17:00 Construction Spending (MoM)
hidden=1
descr=Construction Spending (MoM)
color=16119285
selectable=0
date1=1546534800
</object>

<object>
type=109
name=2019.01.03 17:00 ISM Manufacturing PMI
hidden=1
descr=ISM Manufacturing PMI 54.1 / 57.9
color=13353215
selectable=0
date1=1546534800
</object>

<object>
type=109
name=2019.01.04 10:55 DE: Unemployment Change
hidden=1
descr=DE: Unemployment Change -14K / -11K
color=13353215
selectable=0
date1=1546599300
</object>

<object>
type=109
name=2019.01.04 11:00 IT: Consumer Price Index (MoM)
hidden=1
descr=IT: Consumer Price Index (MoM)
color=16119285
selectable=0
date1=1546599600
</object>

<object>
type=109
name=2019.01.04 15:30 Nonfarm Payrolls
hidden=1
descr=Nonfarm Payrolls 312K / 177K
color=15658671
selectable=0
date1=1546615800
</object>

<object>
type=109
name=2019.01.04 22:30 EMU: CFTC EUR NC net positions
hidden=1
descr=EMU: CFTC EUR NC net positions
color=16119285
selectable=0
date1=1546641000
</object>

<object>
type=109
name=2019.01.04 22:30 CFTC Gold NC net positions
hidden=1
descr=CFTC Gold NC net positions
color=16119285
selectable=0
date1=1546641000
</object>

<object>
type=109
name=2019.01.04 22:30 CFTC Oil NC net positions
hidden=1
descr=CFTC Oil NC net positions
color=16119285
selectable=0
date1=1546641000
</object>

<object>
type=109
name=2019.01.05 17:15 FOMC Member Bostic speech
hidden=1
descr=FOMC Member Bostic speech
color=16119285
selectable=0
date1=1546708500
</object>

<object>
type=109
name=2019.01.03 10:00 ES: Unemployment Change
hidden=1
descr=ES: Unemployment Change -50.6K / 
color=13353215
selectable=0
date1=1546509600
</object>

<object>
type=109
name=2019.01.03 10:00 EMU: Private loans (YoY)
hidden=1
descr=EMU: Private loans (YoY) 3.3% / 3.3%
color=15658671
selectable=0
date1=1546509600
</object>

<object>
type=109
name=2019.01.03 11:00 EMU: M3 Money Supply (YoY)
hidden=1
descr=EMU: M3 Money Supply (YoY) 3.7% / 3.8%
color=13353215
selectable=0
date1=1546513200
</object>

<object>
type=109
name=2019.01.03 11:00 EMU: M3 Money Supply (3m)
hidden=1
descr=EMU: M3 Money Supply (3m) 3.7% / 
color=15658671
selectable=0
date1=1546513200
</object>

<object>
type=109
name=2019.01.03 11:30 PT: Business Confidence
hidden=1
descr=PT: Business Confidence 2.2 / 
color=15658671
selectable=0
date1=1546515000
</object>

<object>
type=109
name=2019.01.03 11:30 PT: Consumer Confidence
hidden=1
descr=PT: Consumer Confidence -2.2 / 
color=13353215
selectable=0
date1=1546515000
</object>

<object>
type=109
name=2019.01.03 11:40 ES: 10-y Obligaciones Auction
hidden=1
descr=ES: 10-y Obligaciones Auction 1.400% / 
color=15658671
selectable=0
date1=1546515600
</object>

<object>
type=109
name=2019.01.03 11:40 ES: 3-y Bond Auction
hidden=1
descr=ES: 3-y Bond Auction -0.039% / 
color=13353215
selectable=0
date1=1546515600
</object>

<object>
type=109
name=2019.01.03 12:00 ES: 5-y Bond Auction
hidden=1
descr=ES: 5-y Bond Auction 0.329% / 
color=15658671
selectable=0
date1=1546516800
</object>

<object>
type=109
name=2019.01.03 12:18 ES: 10-y Obligaciones Auction
hidden=1
descr=ES: 10-y Obligaciones Auction 1.400% / 
color=15658671
selectable=0
date1=1546517880
</object>

<object>
type=109
name=2019.01.03 13:04 ES: 3-y Bond Auction
hidden=1
descr=ES: 3-y Bond Auction -0.039% / 
color=13353215
selectable=0
date1=1546520640
</object>

<object>
type=109
name=2019.01.03 13:05 ES: 5-y Bond Auction
hidden=1
descr=ES: 5-y Bond Auction 0.329% / 
color=15658671
selectable=0
date1=1546520700
</object>

<object>
type=109
name=2019.01.04 18:00 EIA Crude Oil Stocks change
hidden=1
descr=EIA Crude Oil Stocks change
color=16119285
selectable=0
date1=1546624800
</object>

<object>
type=109
name=2019.01.03 14:30 Challenger Job Cuts
hidden=1
descr=Challenger Job Cuts 43.884K / 
color=15658671
selectable=0
date1=1546525800
</object>

<object>
type=109
name=2019.01.03 15:30 Continuing Jobless Claims
hidden=1
descr=Continuing Jobless Claims 1.740M / 
color=15658671
selectable=0
date1=1546529400
</object>

<object>
type=109
name=2019.01.03 16:45 ISM-NY Business Conditions Index
hidden=1
descr=ISM-NY Business Conditions Index 65.4 / 
color=15658671
selectable=0
date1=1546533900
</object>

<object>
type=109
name=2019.01.03 17:00 ISM Prices Paid
hidden=1
descr=ISM Prices Paid 54.9 / 58.0
color=13353215
selectable=0
date1=1546534800
</object>

<object>
type=109
name=2019.01.04 10:15 ES: Markit Services PMI
hidden=1
descr=ES: Markit Services PMI 54.0 / 53.9
color=15658671
selectable=0
date1=1546596900
</object>

<object>
type=109
name=2019.01.03 23:30 API Weekly Crude Oil Stock
hidden=1
descr=API Weekly Crude Oil Stock -4.50M / 
color=13353215
selectable=0
date1=1546558200
</object>

<object>
type=109
name=2019.01.04 22:30 Total Vehicle Sales
hidden=1
descr=Total Vehicle Sales
color=16119285
selectable=0
date1=1546641000
</object>

<object>
type=109
name=2019.01.04 08:00 IE: Purchasing Manager Index Services
hidden=1
descr=IE: Purchasing Manager Index Services 56.3 / 
color=15658671
selectable=0
date1=1546588800
</object>

<object>
type=109
name=2019.01.04 09:45 FR: Consumer Price Index (EU norm) (MoM)
hidden=1
descr=FR: Consumer Price Index (EU norm) (MoM) 0.1% / 
color=15658671
selectable=0
date1=1546595100
</object>

<object>
type=109
name=2019.01.04 09:45 FR: Consumer Price Index (EU norm) (YoY)
hidden=1
descr=FR: Consumer Price Index (EU norm) (YoY) 1.9% / 2.0%
color=13353215
selectable=0
date1=1546595100
</object>

<object>
type=109
name=2019.01.04 10:45 IT: Markit Services PMI
hidden=1
descr=IT: Markit Services PMI 50.5 / 50.3
color=15658671
selectable=0
date1=1546598700
</object>

<object>
type=109
name=2019.01.04 10:50 FR: Markit PMI Composite
hidden=1
descr=FR: Markit PMI Composite 48.7 / 49.4
color=13353215
selectable=0
date1=1546599000
</object>

<object>
type=109
name=2019.01.04 10:50 FR: Markit Services PMI
hidden=1
descr=FR: Markit Services PMI 49.0 / 49.6
color=13353215
selectable=0
date1=1546599000
</object>

<object>
type=109
name=2019.01.04 10:55 DE: Markit PMI Composite
hidden=1
descr=DE: Markit PMI Composite 51.6 / 52.2
color=13353215
selectable=0
date1=1546599300
</object>

<object>
type=109
name=2019.01.04 10:55 DE: Markit Services PMI
hidden=1
descr=DE: Markit Services PMI 51.8 / 52.5
color=13353215
selectable=0
date1=1546599300
</object>

<object>
type=109
name=2019.01.04 10:55 DE: Unemployment Rate s.a.
hidden=1
descr=DE: Unemployment Rate s.a. 5% / 5%
color=15658671
selectable=0
date1=1546599300
</object>

<object>
type=109
name=2019.01.04 11:00 EMU: Markit PMI Composite
hidden=1
descr=EMU: Markit PMI Composite 51.1 / 51.3
color=13353215
selectable=0
date1=1546599600
</object>

<object>
type=109
name=2019.01.04 11:00 EMU: Markit Services PMI
hidden=1
descr=EMU: Markit Services PMI 51.2 / 51.4
color=13353215
selectable=0
date1=1546599600
</object>

<object>
type=109
name=2019.01.04 12:00 EMU: Consumer Price Index (YoY)
hidden=1
descr=EMU: Consumer Price Index (YoY) 1.6% / 1.8%
color=13353215
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 EMU: Consumer Price Index - Core (YoY)
hidden=1
descr=EMU: Consumer Price Index - Core (YoY) 1% / 1%
color=15658671
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 EMU: Producer Price Index (MoM)
hidden=1
descr=EMU: Producer Price Index (MoM) -0.3% / -0.2%
color=13353215
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 EMU: Producer Price Index (YoY)
hidden=1
descr=EMU: Producer Price Index (YoY) 4.0% / 4.1%
color=13353215
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 IT: Consumer Price Index (EU Norm) (MoM)
hidden=1
descr=IT: Consumer Price Index (EU Norm) (MoM) 1.2% / 
color=15658671
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 IT: Consumer Price Index (EU Norm) (YoY)
hidden=1
descr=IT: Consumer Price Index (EU Norm) (YoY) -0.1% / 
color=13353215
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 IT: Consumer Price Index (MoM)
hidden=1
descr=IT: Consumer Price Index (MoM) -0.1% / 0.1%
color=13353215
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 12:00 IT: Consumer Price Index (YoY)
hidden=1
descr=IT: Consumer Price Index (YoY) 1.1% / 
color=15658671
selectable=0
date1=1546603200
</object>

<object>
type=109
name=2019.01.04 15:30 Average Hourly Earnings (MoM)
hidden=1
descr=Average Hourly Earnings (MoM) 0.4% / 0.3%
color=15658671
selectable=0
date1=1546615800
</object>

<object>
type=109
name=2019.01.04 15:30 Average Hourly Earnings (YoY)
hidden=1
descr=Average Hourly Earnings (YoY) 3.2% / 3.0%
color=15658671
selectable=0
date1=1546615800
</object>

<object>
type=109
name=2019.01.04 15:30 Average Weekly Hours
hidden=1
descr=Average Weekly Hours 34.5 / 34.5
color=15658671
selectable=0
date1=1546615800
</object>

<object>
type=109
name=2019.01.04 15:30 Labor Force Participation Rate
hidden=1
descr=Labor Force Participation Rate 63.1% / 
color=15658671
selectable=0
date1=1546615800
</object>

<object>
type=109
name=2019.01.04 15:30 Unemployment Rate
hidden=1
descr=Unemployment Rate 3.9% / 3.7%
color=15658671
selectable=0
date1=1546615800
</object>

<object>
type=109
name=2019.01.04 16:45 Markit PMI Composite
hidden=1
descr=Markit PMI Composite 54.4 / 54.7
color=13353215
selectable=0
date1=1546620300
</object>

<object>
type=109
name=2019.01.04 16:45 Markit Services PMI
hidden=1
descr=Markit Services PMI 54.4 / 53.4
color=15658671
selectable=0
date1=1546620300
</object>

