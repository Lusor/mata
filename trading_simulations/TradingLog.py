import calendar
import io
import time


from datetime import timedelta, datetime

import numpy as np
from docx import Document
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
from docx.shared import Inches, RGBColor, Pt, Cm
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

from data_loader.DatasetLayoutLoad import SignalType
from data_visual.SimplePlot import SimplePlot


class TradingLog:
    @staticmethod
    def generateLog(timeframe, path, fileName, imageFilePath, closedPositions, statisticList):
        # generate plots of distributions
        if len(closedPositions) > 0:
            TradingLog.getStatByDay(path, closedPositions)
            TradingLog.getStatByHour(path, closedPositions)

        document = Document()

        document.add_heading('Simulation - account balance changes', 0)
        document.add_heading(fileName, 1)

        # add image
        document.add_heading("Results", 2)
        document.add_picture(imageFilePath, width=Inches(7))
        last_paragraph = document.paragraphs[-1]
        last_paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

        # add statistics
        document.add_heading("Statistics", 2)
        document.add_paragraph(' ')
        for info in statisticList:
            document.add_paragraph(
                info[0] + ':  ' + str(info[1]), style='List Bullet'
            )
        document.add_paragraph(' ')

        # more stats
        document.add_picture(path+"weekdays.png", width=Inches(7))
        last_paragraph = document.paragraphs[-1]
        last_paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        document.add_paragraph(' ')
        document.add_picture(path+"hours.png", width=Inches(7))
        last_paragraph = document.paragraphs[-1]
        last_paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        document.add_paragraph(' ')

        document.add_heading("Positions log", 2)
        document.add_paragraph(' ')
        table = document.add_table(rows=len(closedPositions) + 1, cols=15)
        table.alignment = WD_TABLE_ALIGNMENT.CENTER
        table.style = 'TableGrid'
        #   table.fontsize = 10
        hdr_cells = table.rows[0].cells
        hdr_cells[0].text = 'ID'
        hdr_cells[0].width = Inches(0.1)  # set_column_width(table.columns[0], Cm(13))
        hdr_cells[1].text = 'Type'
        hdr_cells[1].width = Inches(0.1)
        hdr_cells[2].text = 'Open date'
        hdr_cells[2].width = Inches(1)
        hdr_cells[3].text = 'Open price'

        hdr_cells[4].text = 'Close date'
        hdr_cells[4].width = Inches(1)

        hdr_cells[5].text = 'Close price'
        hdr_cells[6].text = 'Dur'
        hdr_cells[7].text = 'Initial SL'
        hdr_cells[8].text = 'Final SL'
        hdr_cells[9].text = 'Closed by SL'

        hdr_cells[10].text = 'Points'
        hdr_cells[11].text = 'Balance before'
        hdr_cells[12].text = 'Lot'
        hdr_cells[13].text = 'Point cost'
        hdr_cells[14].text = 'Res'
        hdr_cells[14].width = Inches(1)

        for i in range(table._column_count):
            TradingLog.setCellFont(hdr_cells, i, 8, True)

        index = 1
        table_cells = table._cells
        for position in closedPositions:
            positionDuration = float((position.closeDatetime - position.openDatetime).total_seconds()) / (
            60 * timeframe)

            row_cells = table_cells[index * table._column_count:(index + 1) * table._column_count]
            row_cells[0].text = str(index)
            row_cells[1].text = str("Buy" if position.positionType == SignalType.BUY else "Sell")
            row_cells[2].text = str(position.openDatetime)
            row_cells[3].text = str("%.5f" % position.openPrice)
            row_cells[4].text = str(position.closeDatetime)
            row_cells[5].text = str("%.5f" % position.closePrice)
            row_cells[6].text = str("%.0f" % positionDuration)
            row_cells[7].text = str("%.5f" % position.initialStopLoss)
            row_cells[8].text = str("%.5f" % position.stopLoss)
            row_cells[9].text = 'X' if position.isClosedWithStopLoss else ''
            row_cells[10].text = str("%.f" % position.pointsDone)
            row_cells[11].text = str("%.f" % position.currentBalance)
            row_cells[12].text = str("%.3f" % position.lotSize)
            row_cells[13].text = str("%.5f" % position.pointCost)
            row_cells[14].text = str("%.2f" % position.cost)

            for i in range(table._column_count):
                TradingLog.setCellFont(row_cells, i, 8, False)

            if (position.cost < 0):
                shading_elm_1 = parse_xml(r'<w:shd {} w:fill="f7a897"/>'.format(nsdecls('w')))
                row_cells[14]._tc.get_or_add_tcPr().append(shading_elm_1)
            else:
                shading_elm_1 = parse_xml(r'<w:shd {} w:fill="82f278"/>'.format(nsdecls('w')))
                row_cells[14]._tc.get_or_add_tcPr().append(shading_elm_1)
            index = index + 1

            if index % 50 == 0:
                print index

        # document.add_page_break()

        sections = document.sections
        for section in sections:
            section.top_margin = Cm(1)
            section.bottom_margin = Cm(1)
            section.left_margin = Cm(1)
            section.right_margin = Cm(1)

        document.save(path + fileName + '.docx')

        return

    @staticmethod
    def setCellFont(row_cells, colID, size, isBold=False):
        row_cells[colID].paragraphs[0].runs[0].font.size = Pt(size)
        row_cells[colID].paragraphs[0].runs[0].font.bold = isBold
        return


    @staticmethod
    def addToSortResults(path, account, text):
        with open(path + "balances.log", "a") as myfile:
            myfile.write(str("%.f" % account.balance) + "   :" + text + "\n")


    @staticmethod
    def exportToMTTemplate(closedPositions, path, filename):

        with io.open('../data/template.tpl', 'r', encoding='utf16') as f:
            content = f.readlines()
        # for i in range(len(content)):
        #     content[i] = content[i].decode("UTF-16")

        index = 0
        greenColor = '32768'
        for position in closedPositions:
            openDateUnix = time.mktime((position.openDatetime + timedelta(hours=3)).timetuple())
            closeDateUnix = time.mktime((position.closeDatetime + timedelta(hours=3)).timetuple())
            content.append("<object>" + "\n")
            content.append("name=Daily Vertical Line X" + str(index) + "\n")
            if position.cost > 0:
                content.append("color=32768" + "\n")
            content.append("date1=" + str("%.f" % openDateUnix) + "\n")
            content.append("</object>" + "\n")
            content.append("<object>" + "\n")
            content.append("name=Daily Vertical Line X" + str(index+1) + "\n")
            content.append("color=8388608" + "\n")
            content.append("date1=" + str("%.f" % closeDateUnix) + "\n")
            content.append("</object>" + "\n")
            index = index + 2

        content.append("</window>")
        content.append("</chart>")

        with open(path + filename + ".tpl", "w") as myfile:
            for line in content:
                myfile.write(line)
        return


    @staticmethod
    def getStatByDay(path, closedPositions):
        print "STATISTICS PER EACH WEEKDAY"

        weekdayNames = [""]*7
        for i in range(7):
            curDate = datetime.today() + timedelta(days=i)
            weekdayNames[curDate.weekday()] = curDate.strftime("%A")
        posPositionCountPerWeekday = [0]*7
        negPositionCountPerWeekday = [0] * 7

        posPositionSumPerWeekday = [0]*7
        negPositionSumPerWeekday = [0] * 7
        for position in closedPositions:
            weekDayIndex = position.openDatetime.weekday()
            if position.cost > 0:
                posPositionCountPerWeekday[weekDayIndex] = posPositionCountPerWeekday[weekDayIndex] + 1
                posPositionSumPerWeekday[weekDayIndex] = posPositionSumPerWeekday[weekDayIndex] + position.cost
            else:
                negPositionCountPerWeekday[weekDayIndex] = negPositionCountPerWeekday[weekDayIndex] + 1
                negPositionSumPerWeekday[weekDayIndex] = negPositionSumPerWeekday[weekDayIndex] + position.cost
        differences = np.array(posPositionSumPerWeekday) +  np.array(negPositionSumPerWeekday)

        # for i in range(len(weekdayNames)):
        #     print "PosPositionCountPerWeekday:"
        #     print weekdayNames[i] + ": " + str(posPositionCountPerWeekday[i])
        # print ""
        # for i in range(len(weekdayNames)):
        #     print "negPositionCountPerWeekday:"
        #     print weekdayNames[i] + ": " + str(negPositionCountPerWeekday[i])
        # print ""
        # for i in range(len(weekdayNames)):
        #     print "posPositionSumPerWeekday:"
        #     print weekdayNames[i] + ": " + str("%.f" % posPositionSumPerWeekday[i])
        # print ""
        # for i in range(len(weekdayNames)):
        #     print "negPositionSumPerWeekday:"
        #     print weekdayNames[i] + ": " + str("%.f" % negPositionSumPerWeekday[i])
        # print ""
        # for i in range(len(weekdayNames)):
        #     print "differences:"
        #     print weekdayNames[i] + ": " + str("%.f" % differences[i])

        dataPlot = SimplePlot()
        dataPlot.addMajorLineData(weekdayNames, differences, 'Difference', "k")
        dataPlot.addMinorLineData(weekdayNames, posPositionSumPerWeekday, 'Positive', "g")
        dataPlot.addMinorLineData(weekdayNames, negPositionSumPerWeekday, 'Negative', "r")
        dataPlot.savePlot(path+"weekdays.png", 'Deals per weekdays')

    @staticmethod
    def getStatByHour(path, closedPositions):
        posPositionCountPerHour = [0]*24
        negPositionCountPerHour = [0]*24
        posPositionSumPerHour = [0]*24
        negPositionSumPerHour = [0]*24

        for position in closedPositions:
            hour = position.openDatetime.hour
            if position.cost > 0:
                posPositionCountPerHour[hour] = posPositionCountPerHour[hour] + 1
                posPositionSumPerHour[hour] = posPositionSumPerHour[hour] + position.cost
            else:
                negPositionCountPerHour[hour] = negPositionCountPerHour[hour] + 1
                negPositionSumPerHour[hour] = negPositionSumPerHour[hour] + position.cost

        differences = np.array(posPositionSumPerHour) + np.array(negPositionSumPerHour)
        # print "STATISTICS PER EACH HOUR"
        # for i in range(24):
        #     print "PosPositionCountPerWeekday:"
        #     print str(i) + ": " + str(posPositionCountPerHour[i])
        # print ""
        # for i in range(24):
        #     print "negPositionCountPerWeekday:"
        #     print str(i) + ": " + str(negPositionCountPerHour[i])
        # print ""
        # for i in range(24):
        #     print "posPositionSumPerWeekday:"
        #     print str(i) + ": " + str("%.f" % posPositionSumPerHour[i])
        # print ""
        # for i in range(24):
        #     print "negPositionSumPerWeekday:"
        #     print str(i) + ": " + str("%.f" % negPositionSumPerHour[i])
        # print ""
        # for i in range(24):
        #     print "differences:"
        #     print str(i) + ": " + str("%.f" % differences[i])

        hour = range(24)
        dataPlot = SimplePlot()
        dataPlot.addMajorLineData(hour, differences, 'Difference', "k")
        dataPlot.addMinorLineData(hour, posPositionSumPerHour, 'Positive', "g")
        dataPlot.addMinorLineData(hour, negPositionSumPerHour, 'Negative', "r")
        dataPlot.savePlot(path+"hours.png", 'Deals per hours')