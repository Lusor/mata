import itertools
from datetime import datetime

from trading_simulations.TradingAccount import LotType
from trading_simulations.TradingSimulator import TradingSimulator

class MoneySettings:
    def __init__(self, lotType, risk, onePointScale):
        self.lotType = lotType
        self.risk = risk
        self.onePointScale = onePointScale

class SimulationParameters:
    def __init__(self, entryStrategy, exitStrategy, slStrategy, stopLossSize, useTrailing, checkAgainstAllPrices,
                 useWithoutLossFlag, spread, workingHours, createLog, specificParameters):
        self.entryStrategy = entryStrategy
        self.exitStrategy = exitStrategy
        self.slStrategy = slStrategy
        self.stopLossSize = stopLossSize
        self.useTrailing = useTrailing
        self.checkAgainstAllPrices = checkAgainstAllPrices
        self.useWithoutLossFlag = useWithoutLossFlag
        self.spread = spread
        self.workingHours = workingHours
        self.createLog = createLog
        self.specificParameters = specificParameters

class SpecificParameters:
    # parameters is stored in map
    def __init__(self, parameters):
        self.parameters = parameters

    def toString(self):
        result = ""
        for key in self.parameters:
            #result = result + "_" + key + "("
            result = result + "_("
            result = result + SpecificParameters.parameterToString(self.parameters[key])
            result = result + ")"
        return result

    @staticmethod
    def parameterToString(obj):
        if isinstance(obj, list):
            return ', '.join([str(x) for x in obj]) if len(obj) != 0 else 'None'
        else:
            return str(obj)


print 'start'

# crosses = [EURUSD, AUDJPY, AUDUSD, GBPJPY, GBPUSD, XAUUSD]
# timeframes = [5, 15, 30, 60, 240, 1440]

crossName = "AUDUSD"
timeframe = 1440
startDate = datetime(2005, 1, 1)
endDate = datetime(2017, 12, 31)
stopLossSize = 1

# more parameters:
# max distance from price to cloud (None or integers)
max_distance_price_cloud = 10
useWithoutLossFlag = False

# money management
moneySettings = MoneySettings(lotType=LotType.STANDARD, risk = 0.02, onePointScale = 0.00001)
startBalance = 10000
spread = 5.0
#fixRisk = True

entryStrategy = "en_price_leaves_cloud_v2"
entryHacks = ["v2_filter"] # [], ["v2_filter"]
globalStrategy = "Ichimoku"
tenkanPeriod = 9
kijunPeriod = 26
senkouBPeriod = 52
# tenkanPeriod = 9
# kijunPeriod = 26
# senkouBPeriod = 52

workingHours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
createLog = True

# simulation parameters
# entryStrategyList = ["three_lines", "price_leave_cloud"]
# exitStrategyList = ["exit_cloud_changed", "exit_price_cross_cloud", "exit_price_cross_chikou",
#                     "exit_price_kijun", "exit_price_tenkan", "exit_tenkan_kijun"]
# stopLossSizeList = [50, 100]  # [25, 50, 75, 100, 150]
# useTrailingList = [False, True]  # [False, True]
# stopLossStrategyList = ['fixed_from_price', 'simple_kijun', 'kijun_senkou', "tenkan"]

exitStrategyList = ["exit_price_cross_cloud"]
#exitStrategyList = ["exit_price_cross_cloud"]
stopLossSizeList = [100, 200, 300, 400, 500]  # [25, 50, 75, 100, 150]
useTrailingList = [False, True]  # [False, True]
stopLossStrategyList = ['simple_kijun', 'kijun_senkou', 'nearest_senkou']

ichimokuSpecificParameters = [
    {'entryHacks': []}, {'entryHacks': ['price_tenkan_less_tenkan_cloud']}, {'entryHacks':['tenkan_kijun_less_kijun_cloud']},
    {'entryHacks': ['price_tenkan_less_tenkan_cloud', 'tenkan_kijun_less_kijun_cloud']}]

parameters = [exitStrategyList, stopLossStrategyList, stopLossSizeList, useTrailingList, ichimokuSpecificParameters]
allCombinations = list(itertools.product(*parameters))

checkAgainstAllPrices = True


simulator = TradingSimulator(startDate, endDate, timeframe, crossName)
if simulator.loadData():

    if (1 == 1):
        simParams = SimulationParameters(entryStrategy=entryStrategy, exitStrategy="exit_price_cross_cloud",
                                         slStrategy="nearest_senkou", stopLossSize=stopLossSize,
                                         useTrailing=False, checkAgainstAllPrices=checkAgainstAllPrices,
                                         useWithoutLossFlag = useWithoutLossFlag,
                                         spread = spread,
                                         workingHours = workingHours,
                                         createLog = createLog,
                                         specificParameters=SpecificParameters({"entryHacks": entryHacks,
                                                                                "max_distance_price_cloud": max_distance_price_cloud,
                                                                                'tenkanPeriod': tenkanPeriod,
                                                                                'kijunPeriod': kijunPeriod,
                                                                                'senkouBPeriod': senkouBPeriod}))
        simulator.simulate(startBalance, moneySettings, simParams)
    else:
        for i in range(len(allCombinations)):
            (curExitStrategy, curSLStrategy, curStopLossSize, curUseTrailing, specificIchimoku) = allCombinations[i]
            simParams = SimulationParameters(entryStrategy=entryStrategy,exitStrategy=curExitStrategy,
                                             slStrategy=curSLStrategy, stopLossSize=curStopLossSize,
                                             useTrailing=curUseTrailing, checkAgainstAllPrices=checkAgainstAllPrices,
                                             useWithoutLossFlag=useWithoutLossFlag,
                                             spread=spread,
                                             workingHours = workingHours,
                                             createLog=createLog,
                                             specificParameters=SpecificParameters(specificIchimoku))
            simulator.simulate(startBalance, moneySettings, simParams)

print 'finish'
