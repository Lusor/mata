import numpy as np
from enum import Enum

from data_loader.DatasetLayoutLoad import SignalType
from trading_simulations.TradingLog import TradingLog

class LotType(Enum):
    STANDARD = 100000
    MINI = 10000
    MICRO = 1000


class TradingAccount:
    # info about a position (id, openPrice, closePrice, openDatetime, closeDatetime, volume, accountChange)

    def __init__(self, startBalance, maxParallelPositions):
        self.closedPositions = []
        self.startBalance = startBalance
        self.balance = startBalance
        self.maxParallelOperations = maxParallelPositions
        self.accountChanges = []

    def commitOperation(self, position, moneySettings, isClosedWithStopLoss):
        # count cost
        position.pointsDone = (position.closePrice - position.openPrice)/moneySettings.onePointScale
        if(position.positionType == SignalType.BUY):
            cost = (position.closePrice - position.openPrice) * position.pointCost/moneySettings.onePointScale
        if (position.positionType == SignalType.SELL):
            cost = (position.openPrice - position.closePrice) * position.pointCost/moneySettings.onePointScale
        position.cost = cost
        position.isClosedWithStopLoss = isClosedWithStopLoss
        self.balance = self.balance + cost
        self.closedPositions.append(position)
        print "Close the position ("+ position.toString() +") is closed with cost " + str("%.2f" % cost)
        return

    @staticmethod
    def computeLotSize(lotType, risk, currentBalance, stopLossSizePoints):
        lotPointCost = 10
        if lotType == LotType.STANDARD:
            lotPointCost = 10
        elif lotType == LotType.MINI:
            lotPointCost = 1
        elif lotType == LotType.MICRO:
            lotPointCost = 0.1

        onePointCost = TradingAccount.computePointCost(risk, currentBalance, stopLossSizePoints)
        lotSize = onePointCost/lotPointCost
        return lotSize

    @staticmethod
    def computePointCost(risk, currentBalance, stopLossSizePoints):
        riskValue = currentBalance * risk
        onePointCost = riskValue / stopLossSizePoints
        return onePointCost

    @staticmethod
    def evaluatePositionCost(position, currentPrice, moneySettings):
        if position == None:
            return 0.0
        # count cost
        if(position.positionType == SignalType.BUY):
            cost = (currentPrice - position.openPrice) * position.pointCost/moneySettings.onePointScale
        if (position.positionType == SignalType.SELL):
            cost = (position.openPrice - currentPrice) * position.pointCost/moneySettings.onePointScale
        return cost

    def generateLog(self, timeframe, path, fileName, imageFilePath):
        statisticList = self.getStatistics(timeframe)
        TradingLog.generateLog(timeframe, path, fileName, imageFilePath, self.closedPositions, statisticList)
        TradingLog.addToSortResults(path, self, fileName)
        TradingLog.exportToMTTemplate(self.closedPositions, path, fileName)

    def getStatistics(self, timeframe):
        # a list of pairs
        statisticList = []

        beneficialPosition = filter(lambda x: x.cost > 0, self.closedPositions)
        beneficialPositionCosts = [x.cost for x in beneficialPosition]
        nonBeneficialPosition = filter(lambda x: x.cost <= 0, self.closedPositions)
        nonBeneficialPositionCosts = [x.cost for x in nonBeneficialPosition]

        percentagePositive = float(len(beneficialPositionCosts))/(len(self.closedPositions) if len(self.closedPositions) > 0 else 1)
        percentageNonPositive = float(len(nonBeneficialPositionCosts))/(len(self.closedPositions) if len(self.closedPositions) > 0 else 1)

        durations = [(x.closeDatetime - x.openDatetime).total_seconds() / (60 * timeframe)
                                                   for x in self.closedPositions]

        minBalanceValue = np.min(self.accountChanges)
        maxBalanceValue = np.max(self.accountChanges)

        statisticList.append(("Balance at the beginning", "%.0f" % self.startBalance))
        statisticList.append(("Balance at the end", "%.0f" % self.balance))
        statisticList.append(("Min, max account balances", "(" + str("%.0f" % minBalanceValue) + ", " + str("%.0f" % maxBalanceValue) +")"))
        statisticList.append(("Number of positions", len(self.closedPositions)))
        statisticList.append(("Positive positions", "%.2f" % percentagePositive))
        statisticList.append(("Non-positive positions", "%.2f" % percentageNonPositive))
        if len(beneficialPositionCosts) > 0:
            statisticList.append(("Average benefit from positive positions", "%.2f" % np.mean(beneficialPositionCosts)))
        statisticList.append(("Average loss from non-positive positions", "%.2f" % np.mean(nonBeneficialPositionCosts)))
        if len(beneficialPositionCosts) > 0:
            statisticList.append(("The biggest profit", "%.2f" % np.max(beneficialPositionCosts)))
        if len(nonBeneficialPositionCosts) > 0:
            statisticList.append(("The biggest loss", "%.2f" % np.min(nonBeneficialPositionCosts)))
        statisticList.append(("Average position duration", str("%.1f" % np.mean(durations)) + " "))
        return statisticList
