from datetime import datetime

import numpy
import talib

from data_loader.DatasetLayoutLoad import SignalType
from indicators.Envelopes import Envelopes
from signals.Signals import Signals


class RSEVStrategy:

    def __init__(self, exitStrategy, slStrategy, specificParameters, stopLossSize, onePointScale):
        self.openPositionList = []
        self.exitStrategy = exitStrategy
        self.slStrategy = slStrategy
        self.specificParameters = specificParameters
        self.stopLossSize = stopLossSize
        self.onePointScale = onePointScale

    def setData(self, high, low, close, times):
        self.high = high
        self.low = low
        self.close = close
        self.times = times
        self.size = len(close)


    def computeOpenPositions(self):
        RSI_TOP_LEVEL = 80
        RSI_BOTTOM_LEVEL = 20


        midPrice = [sum(x)/2.0 for x in zip(self.high, self.low)] #(self.high + self.low)/2

        self.rsi = talib.RSI(numpy.asarray(midPrice), timeperiod=8)
        (self.envBottomLine, self.envTopLine) = Envelopes.getLines(self.low, self.high, 8, 0.22)
        price_cross_env_top_up, price_cross_env_top_down = Signals.lineCrossOverW_directions(midPrice, self.envTopLine, 0)
        price_cross_env_bottom_up, price_cross_env_bottom_down= Signals.lineCrossOverW_directions(midPrice, self.envBottomLine, 0)

        # for i in range(len(price_cross_env_bottom_up)):
        #     if price_cross_env_top_down[i]:
        #         print i
        self.openPositionList = [SignalType.NOTHING] * self.size
        for i in range(self.size - 1):
            #check times

            if (self.times[i].hour < 14 or self.times[i].hour > 16) or self.times[i].weekday() in [3]:
                continue

            # check to buy
            if price_cross_env_top_down[i] and self.rsi[i] > RSI_TOP_LEVEL:
                self.openPositionList[i] = SignalType.BUY

            # check to sell
            if price_cross_env_bottom_up[i] and self.rsi[i] < RSI_BOTTOM_LEVEL:
                self.openPositionList[i] = SignalType.SELL

    def isTimeToOpen(self, index):
        return self.openPositionList[index]

    def isTimeToClose(self, index, openPosition):
        return None

        # parametrized SL

    def getStopLoss(self, signalType, currentClosePrice, index):
        stopLossValue = self.stopLossSize * self.onePointScale

        if signalType == SignalType.BUY:
            return currentClosePrice - stopLossValue
        elif signalType == SignalType.SELL:
            return currentClosePrice + stopLossValue
        return None

    def trailing_change(self, openPosition, currentClosePrice, curIndex):
        self.trailing_fixed(openPosition, currentClosePrice)

        return None

    def trailing_fixed(self, openPosition, currentClosePrice):
        if openPosition.positionType == SignalType.BUY:
            stopLossValue = openPosition.openPrice - openPosition.initialStopLoss
            currentDifference = currentClosePrice - openPosition.stopLoss
            if currentDifference > stopLossValue:
                openPosition.setStopLoss(currentClosePrice - stopLossValue)
        if openPosition.positionType == SignalType.SELL:
            stopLossValue = openPosition.initialStopLoss - openPosition.openPrice
            currentDifference = openPosition.stopLoss - currentClosePrice
            if currentDifference > stopLossValue:
                openPosition.setStopLoss(currentClosePrice + stopLossValue)
        return