import math

from data_loader.DatasetLayoutLoad import SignalType
from trading_simulations.TradingAccount import TradingAccount


class TradingPosition:

    def __init__(self, positionType, openPrice, stopLoss, openDatetime, moneySettings, currentBalance):
        self.positionType = positionType
        self.openPrice = openPrice
        self.initialStopLoss = stopLoss
        self.stopLoss = stopLoss
        self.openDatetime = openDatetime

        self.closePrice = None
        self.isSLChanged = False
        self.closeDatetime = None
        self.isClosedWithStopLoss = False

        stopLossSizePoints = math.fabs(openPrice - stopLoss)/moneySettings.onePointScale

        self.currentBalance = currentBalance
        self.lotSize = TradingAccount.computeLotSize(moneySettings.lotType, moneySettings.risk, currentBalance, stopLossSizePoints)
        self.pointCost = TradingAccount.computePointCost(moneySettings.risk, currentBalance, stopLossSizePoints)

    def setStopLoss(self, newStopLoss):
        self.stopLoss = newStopLoss
        if not self.isSLChanged:
            self.isSLChanged = (newStopLoss - self.initialStopLoss) > 0.000001

    def toString(self):
        text = "parameters: "
        text = text + str("Buy" if self.positionType == SignalType.BUY else "Sell")
        text = text + ", "
        text = text + "open datetime " + str(self.openDatetime)
        text = text + ", "
        text = text + "open price " + str("%.5f" % self.openPrice)
        text = text + ", "
        text = text + "close datetime " + str(self.closeDatetime)
        text = text + ", "
        text = text + "close price " + str("%.5f" % self.closePrice)
        text = text + ", "
        text = text + "initial stoploss " + str("%.5f" % self.initialStopLoss)
        text = text + ", "
        text = text + "current stoploss " + str("%.5f" % self.stopLoss)
        return text

