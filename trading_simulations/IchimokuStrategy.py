import numpy as np
from enum import Enum
from data_loader.DatasetLayoutLoad import SignalType
from indicators.ichimoku import Ichimoku
from signals.IchimokuFeatures import IchimokuFeatures, ICHIMOKU_FTR

class ThreeLinesHacks(Enum):
    PRICE_TENKAN_LESS_TENKAN_CLOUD = 1
    TENKAN_KIJUN_LESS_KIJUN_CLOUD = 2

class IchimokuStrategy:


    def __init__(self, entryStrategy, exitStrategy, slStrategy, specificParameters, stopLossSize, onePointScale):
        self.openPositionList = []
        self.entryStrategy = entryStrategy
        self.exitStrategy = exitStrategy
        self.slStrategy = slStrategy
        self.specificParameters = specificParameters
        self.stopLossSize = stopLossSize
        self.onePointScale = onePointScale

    def setData(self, high, low, open, close, times):
        self.open = open
        self.close = close
        self.size = len(close)
        self.times = times

        self.tenkanPeriod =  self.specificParameters.parameters["tenkanPeriod"]
        self.kijunPeriod = self.specificParameters.parameters["kijunPeriod"]
        self.senkouPeriod = self.specificParameters.parameters["senkouBPeriod"]
        ichimoku = Ichimoku()
        ichimokuValues = ichimoku.compute(high, low, close, self.tenkanPeriod, self.kijunPeriod, self.senkouPeriod)
        self.ichimokuFeaturesObj = IchimokuFeatures(close, ichimokuValues["tenkan_sen"],
                                                    ichimokuValues["kijun_sen"], ichimokuValues["chikou_span"],
                                                    ichimokuValues["senkou_span_a"], ichimokuValues["senkou_span_b"],
                                                    self.tenkanPeriod, self.kijunPeriod, self.senkouPeriod)
        return

    def isTimeToOpen(self, index):
        return self.openPositionList[index]

    def isTimeToClose(self, index, openPosition):
        if (self.exitStrategy == "exit_cloud_changed"):
            return self.exit_cloud_changed(index, openPosition.positionType)
        elif (self.exitStrategy == "exit_price_cross_cloud"):
            return self.exit_price_cross_cloud(index, openPosition.positionType)
        elif (self.exitStrategy == "exit_price_cross_chikou"):
            return self.exit_price_cross_chikou(index, openPosition.positionType)
        elif (self.exitStrategy == "exit_price_kijun"):
            return self.exit_price_kijun(index, openPosition.positionType)
        elif (self.exitStrategy == "exit_price_tenkan"):
            return self.exit_price_tenkan(index, openPosition.positionType)
        elif (self.exitStrategy == "exit_tenkan_kijun"):
            return self.exit_tenkan_kijun(index, openPosition.positionType)
        else:
            raise 'exit strategy is not set up'

    # entry points
    def computeOpenPositions(self):
        self.ichimokuFeatures = self.ichimokuFeaturesObj.getFeatures(
            [
             ICHIMOKU_FTR.PRICE_MORE_TENKAN_KIJUN_SENKOU_A_SENKOU_B,
             ICHIMOKU_FTR.CHIKOU_ABOVE_PRICE,
             ICHIMOKU_FTR.CHIKOU_BELOW_PRICE,
             ICHIMOKU_FTR.CROSS_SENKOU_A_SENKOU_B,
             ICHIMOKU_FTR.PRICE_LESS_TENKAN_KIJUN_SENKOU_A_SENKOU_B,
             ICHIMOKU_FTR.CROSS_PRICE_BLUELINE,
             ICHIMOKU_FTR.CROSS_PRICE_REDLINE,
             ICHIMOKU_FTR.CROSS_TENKAN_KIJUN,
             ICHIMOKU_FTR.PRICE_TENKAN_LESS_TENKAN_CLOUD,
             ICHIMOKU_FTR.TENKAN_KIJUN_LESS_KIJUN_CLOUD,
             ICHIMOKU_FTR.PRICE_IN_CLOUD,
             ICHIMOKU_FTR.PRICE_BELOW_CLOUD,
             ICHIMOKU_FTR.PRICE_ABOVE_CLOUD
             ])

        self.openPositionList = [SignalType.NOTHING] * self.size

        # for i in range(self.size - 1):
        #     if self.ichimokuFeaturesObj.tenkanBuyHack[i] == True:
        #         self.openPositionList[i] = SignalType.BUY
        #     if self.ichimokuFeaturesObj.tenkanSellHack[i] == True:
        #         self.openPositionList[i] = SignalType.SELL

        self.entry_price_leave_cloud_V2()
        return


    # parametrized SL
    def getStopLoss(self, signalType, openPrice, index, onePointScale):
        stopLossValue = self.stopLossSize*self.onePointScale
        if self.slStrategy == "fixed_from_price":
            if signalType == SignalType.BUY:
                return openPrice - stopLossValue
            elif signalType == SignalType.SELL:
                return openPrice + stopLossValue
        elif self.slStrategy in ["simple_kijun", "kijun_senkou"]:
            if signalType == SignalType.BUY:
                return self.ichimokuFeaturesObj.blue_kijun[index] - stopLossValue
            elif signalType == SignalType.SELL:
                return self.ichimokuFeaturesObj.blue_kijun[index] + stopLossValue
        elif self.slStrategy == "tenkan":
            return self.ichimokuFeaturesObj.red_tenkan[index]
        elif self.slStrategy == "nearest_senkou":
            if self.entryStrategy in ["en_price_leaves_cloud_v2"]:
                senkou_a = self.ichimokuFeaturesObj.senkou_a[index-1-self.kijunPeriod]
                senkou_b = self.ichimokuFeaturesObj.senkou_b[index-1-self.kijunPeriod]
            else:
                senkou_a = self.ichimokuFeaturesObj.senkou_a[index-self.kijunPeriod]
                senkou_b = self.ichimokuFeaturesObj.senkou_b[index-self.kijunPeriod]
            if signalType == SignalType.BUY:
                if senkou_a > senkou_b:
                    stopLoss = senkou_a - stopLossValue
                else:
                    stopLoss = senkou_b - stopLossValue
                if np.math.fabs(stopLoss - openPrice) < onePointScale:
                    stopLoss = openPrice - onePointScale
                return stopLoss
            elif signalType == SignalType.SELL:
                if senkou_a > senkou_b:
                    stopLoss = senkou_b + stopLossValue
                else:
                    #print "soplos "  + str(self.ichimokuFeaturesObj.senkou_a[index])+ str(self.ichimokuFeaturesObj.senkou_b[index]) + " " +str(stopLossValue) + " "+ str(self.times[index])
                    stopLoss = senkou_a + stopLossValue
                if np.math.fabs(stopLoss - openPrice) < onePointScale:
                    stopLoss = openPrice + onePointScale
                return stopLoss
        else:
            raise 'stop loss strategy is not set up'
        return 0.0

    # parametrized trailing
    def trailing_change(self, openPosition, currentClosePrice, curIndex):
        if self.slStrategy in ["fixed_from_price" "simple_kijun"]:
            self.trailing_fixed(openPosition, currentClosePrice)
        elif self.slStrategy == "kijun_senkou" :
            self.trailing_kijun_senkou(openPosition, curIndex)
        else:
            raise 'stop loss strategy is not set up'
        return 0.0

    def trailing_fixed(self, openPosition, currentClosePrice):
        if openPosition.positionType == SignalType.BUY:
            stopLossValue = openPosition.openPrice - openPosition.initialStopLoss
            currentDifference = currentClosePrice - openPosition.stopLoss
            if currentDifference > stopLossValue:
                openPosition.setStopLoss(currentClosePrice - stopLossValue)
        if openPosition.positionType == SignalType.SELL:
            stopLossValue = openPosition.initialStopLoss - openPosition.openPrice
            currentDifference = openPosition.stopLoss - currentClosePrice
            if currentDifference > stopLossValue:
                openPosition.setStopLoss(currentClosePrice + stopLossValue)
        return

    # fixed kijun
    def trailing_kijun_senkou(self, openPosition, index):
        senkouA = self.ichimokuFeaturesObj.senkou_a[index-self.kijunPeriod]
        senkouB = self.ichimokuFeaturesObj.senkou_b[index-self.kijunPeriod]

        if (openPosition.positionType == SignalType.BUY):
            cur_senkou = senkouA if senkouA > senkouB else senkouB
            if openPosition.isSLChanged:
                openPosition.setStopLoss(cur_senkou)
            else:
                openPosition.setStopLoss(np.max([openPosition.initialStopLoss, cur_senkou]))
        elif (openPosition.positionType == SignalType.SELL):
            cur_senkou = senkouA if senkouA < senkouB else senkouB
            if openPosition.isSLChanged:
                openPosition.setStopLoss(cur_senkou)
            else:
                openPosition.setStopLoss(np.min([openPosition.initialStopLoss, cur_senkou]))

            print str("%.6f " % openPosition.initialStopLoss) + " " + str("%.6f " % openPosition.stopLoss)
        return

    def entry_price_leave_cloud_V2(self):
        cloud = self.ichimokuFeaturesObj.cloud
        price_in_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_IN_CLOUD]
        price_below_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_BELOW_CLOUD]
        price_above_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_ABOVE_CLOUD]

        max_distance_price_cloud = None
        if 'max_distance_price_cloud' in self.specificParameters.parameters and \
                        self.specificParameters.parameters['max_distance_price_cloud'] is not None:
            max_distance_price_cloud = self.specificParameters.parameters['max_distance_price_cloud'] * self.onePointScale

        for i in range(self.size - 1):
            if cloud[i-self.kijunPeriod] == None:
                continue
            if (price_in_cloud[i-2] or price_below_cloud[i-2]) and price_above_cloud[i-1] and self.open[i] >= (cloud[i-1-self.kijunPeriod])[1]:
                allChecksPassed = True
                # check a filter
                if 'v2_filter' in self.specificParameters.parameters['entryHacks']:
                    allChecksPassed = allChecksPassed and \
                        self.open[i] >= (cloud[i-1-self.kijunPeriod])[1] \
                        and self.open[i] >= self.ichimokuFeaturesObj.red_tenkan[i-1] \
                        and self.open[i] >= self.ichimokuFeaturesObj.blue_kijun[i-1] \
                        and self.ichimokuFeaturesObj.red_tenkan[i-1] >= self.ichimokuFeaturesObj.blue_kijun[i-1] \
                        and self.ichimokuFeaturesObj.green_chikou[i-1-self.kijunPeriod] >= self.close[i-1-self.kijunPeriod]
                # check the max_distance_price_cloud parameter
                if max_distance_price_cloud is not None:
                    cur_distance_price_cloud = self.open[i] - (cloud[i-1-self.kijunPeriod])[1]
                    allChecksPassed = allChecksPassed and (cur_distance_price_cloud < max_distance_price_cloud)

                if allChecksPassed:
                    self.openPositionList[i] = SignalType.BUY

            if (price_in_cloud[i-2] or price_above_cloud[i-2]) and price_below_cloud[i-1] and self.open[i] <= (cloud[i-1-self.kijunPeriod])[0]:
                allChecksPassed = True
                # check a filter
                if 'v2_filter' in self.specificParameters.parameters['entryHacks']:
                    allChecksPassed = allChecksPassed and \
                        self.open[i] <= (cloud[i-1-self.kijunPeriod])[1] \
                        and self.open[i] <= self.ichimokuFeaturesObj.red_tenkan[i-1] \
                        and self.open[i] <= self.ichimokuFeaturesObj.blue_kijun[i-1] \
                        and self.ichimokuFeaturesObj.red_tenkan[i-1] <= self.ichimokuFeaturesObj.blue_kijun[i-1] \
                        and self.ichimokuFeaturesObj.green_chikou[i-1-self.kijunPeriod] <= self.close[i-1-self.kijunPeriod]
                # check the max_distance_price_cloud parameter
                if max_distance_price_cloud is not None:
                        cur_distance_price_cloud = (cloud[i-1-self.kijunPeriod])[0] - self.open[i]
                        allChecksPassed = allChecksPassed and (cur_distance_price_cloud < max_distance_price_cloud)
                if allChecksPassed:
                    self.openPositionList[i] = SignalType.SELL


    # entry strategies
    def entry_price_leave_cloud(self):
        price_in_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_IN_CLOUD]
        price_below_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_BELOW_CLOUD]
        price_above_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_ABOVE_CLOUD]
        cloud = self.ichimokuFeaturesObj.cloud

        max_distance_price_cloud = None
        if 'max_distance_price_cloud' in self.specificParameters.parameters and \
            self.specificParameters.parameters['max_distance_price_cloud'] != None:
            max_distance_price_cloud = self.specificParameters.parameters['max_distance_price_cloud'] * self.onePointScale

        for i in range(self.size-1):
            if price_in_cloud[i-1] == True and price_in_cloud[i] == False:
                if price_above_cloud[i] == True:
                    if max_distance_price_cloud == None:
                        self.openPositionList[i] = SignalType.BUY
                    else:
                        cur_distance_price_cloud = self.ichimokuFeaturesObj.price[i] - (cloud[i-self.kijunPeriod])[1]
                        if cur_distance_price_cloud < max_distance_price_cloud:
                            self.openPositionList[i] = SignalType.BUY
                elif price_below_cloud[i] == True:
                    if max_distance_price_cloud == None:
                        self.openPositionList[i] = SignalType.SELL
                    else:
                        cur_distance_price_cloud = (cloud[i - self.kijunPeriod])[0] - self.ichimokuFeaturesObj.price[i]
                        if cur_distance_price_cloud < max_distance_price_cloud:
                            self.openPositionList[i] = SignalType.SELL

        return

    def entry_three_lines(self):
        cross_senkouA_sekouB_up = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_SENKOU_A_SENKOU_B + "_UP"]
        cross_senkouA_sekouB_down = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_SENKOU_A_SENKOU_B + "_DOWN"]
        price_more_3_lines = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_MORE_TENKAN_KIJUN_SENKOU_A_SENKOU_B]
        price_less_3_lines = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_LESS_TENKAN_KIJUN_SENKOU_A_SENKOU_B]
        chikou_more_price = self.ichimokuFeatures[ICHIMOKU_FTR.CHIKOU_ABOVE_PRICE]

        isCloudForBuy = False
        for i in range(self.size-1):
            # find when to buy
            # if self.cross_senkouA_sekouB_down[i] == True:
            #     isCloudForBuy = True
            # if self.cross_senkouA_sekouB_up[i] == True:
            #     isCloudForBuy = False
            #
            # if isCloudForBuy:
            #     if (price_more_3_lines[i] == True and chikou_more_price[i-self.kijunPeriod] == True):
            #         self.openPositionList[i] = SignalType.BUY

            # first, check if cloud was changed
            if cross_senkouA_sekouB_down[i] == True: #and self.cross_senkouA_sekouB_down[i + 1] == False:
                for j in range(i+1, self.size):
                    if (cross_senkouA_sekouB_down[j] or cross_senkouA_sekouB_up[j]):
                        i = j - 1
                        break
                    if (price_more_3_lines[j] == True and chikou_more_price[j-self.kijunPeriod] == True):
                        additionalCheckPassed = True
                        # if 'price_tenkan_less_tenkan_cloud' in self.specificParameters.parameters['entryHacks']:
                        #     additionalCheckPassed = additionalCheckPassed & (price_tenkan_less_tenkan_cloud[j])
                        # if 'tenkan_kijun_less_kijun_cloud' in self.specificParameters.parameters['entryHacks']:
                        #     additionalCheckPassed = additionalCheckPassed & (tenkan_kijun_less_kijun_cloud[j])
                        if additionalCheckPassed:
                            self.openPositionList[j] = SignalType.BUY
                            i = j + 1
                            break
                            #print str(i) + " BUY"


        isCloudForSell = False
        # find when to sell
        for i in range(self.size-1):
            # find when to buy
            # if self.cross_senkouA_sekouB_up[i] == True:
            #     isCloudForSell = True
            # if self.cross_senkouA_sekouB_down[i] == True:
            #     isCloudForSell = False
            #
            # if isCloudForSell:
            #     if (price_less_3_lines[i] == True and chikou_more_price[i - self.kijunPeriod] == False):
            #         self.openPositionList[i] = SignalType.SELL

            if (self.cross_senkouA_sekouB_up[i] == True):
                for j in range(i+1, self.size):
                    if (self.cross_senkouA_sekouB_down[j] or self.cross_senkouA_sekouB_up[j]):
                        i = j - 1
                        break
                    if (price_less_3_lines[j] == True and chikou_more_price[j-self.kijunPeriod] == False):
                        additionalCheckPassed = True

                        # if 'price_tenkan_less_tenkan_cloud' in self.specificParameters.parameters['entryHacks']:
                        #     additionalCheckPassed = additionalCheckPassed & (price_tenkan_less_tenkan_cloud[j])
                        # if 'tenkan_kijun_less_kijun_cloud' in self.specificParameters.parameters['entryHacks']:
                        #     additionalCheckPassed = additionalCheckPassed & (tenkan_kijun_less_kijun_cloud[j])
                        if additionalCheckPassed:
                            self.openPositionList[j] = SignalType.SELL
                            i = j + 1
                            #print str(i) + " SELL"
                            break
        return



    # exit strategies
    def exit_cloud_changed(self, index, signalType):
        if(signalType == SignalType.BUY and self.cross_senkouA_sekouB_up[index] == True):
            return index
        if(signalType == SignalType.SELL and self.cross_senkouA_sekouB_down[index] == True):
            return index
        return None

    def exit_price_cross_cloud(self, index, signalType):
        price_in_cloud = self.ichimokuFeatures[ICHIMOKU_FTR.PRICE_IN_CLOUD]
        if (price_in_cloud[index] == True):
            return index
        return None

    def exit_price_cross_chikou(self, index, signalType):
        chikou_more_price = self.ichimokuFeatures[ICHIMOKU_FTR.CHIKOU_ABOVE_PRICE]
        chikou_less_price = self.ichimokuFeatures[ICHIMOKU_FTR.CHIKOU_BELOW_PRICE]
        #print "exit_price_cross_chikou " + str(index)
        if signalType == SignalType.BUY and chikou_less_price[index-self.kijunPeriod] == True:
            return index
        if signalType == SignalType.SELL and chikou_more_price[index-self.kijunPeriod] == True:
            return index
        return None

    def exit_price_kijun(self, index, signalType):
        price_cross_kijun_up = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_PRICE_BLUELINE + "_UP"]
        price_cross_kijun_down = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_PRICE_BLUELINE + "_DOWN"]
        if(signalType == SignalType.BUY and price_cross_kijun_down[index] == True):
            return index
        if(signalType == SignalType.SELL and price_cross_kijun_up[index] == True):
            return index
        return None

    def exit_price_tenkan(self, index, signalType):
        price_cross_tenkan_up = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_PRICE_REDLINE + "_UP"]
        price_cross_tenkan_down = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_PRICE_REDLINE + "_DOWN"]
        if(signalType == SignalType.BUY and price_cross_tenkan_up[index] == True):
            return index
        if(signalType == SignalType.SELL and price_cross_tenkan_down[index] == True):
            return index
        return None

    def exit_tenkan_kijun(self, index, signalType):
        tenkan_cross_kijun_up = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_TENKAN_KIJUN + "_UP"]
        tenkan_cross_kijun_down = self.ichimokuFeatures[ICHIMOKU_FTR.CROSS_TENKAN_KIJUN + "_DOWN"]
        if(signalType == SignalType.BUY and tenkan_cross_kijun_up[index] == True):
            return index
        if(signalType == SignalType.SELL and tenkan_cross_kijun_down[index] == True):
            return index
        return None



