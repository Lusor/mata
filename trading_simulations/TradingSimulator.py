from datetime import timedelta, datetime

import math

import os
import cPickle
from data_loader.DatasetLayoutLoad import SignalType
from data_loader.SQLiteAdapter import SQLiteAdapter
from data_visual.SimplePlot import SimplePlot
from trading_simulations.IchimokuStrategy import IchimokuStrategy
from trading_simulations.RSEVStrategy import RSEVStrategy
from trading_simulations.TradingAccount import TradingAccount
from trading_simulations.TradingPosition import TradingPosition

class TradingSimulator:
    def __init__(self, startDate, endDate, timeframe, crossName):
        self.timeframe = timeframe
        self.startDate = startDate
        self.endDate = endDate
        self.crossName = crossName

    def loadData(self):
        dbFileName = "../data/" + self.crossName +"_DB.s3db"
        if not os.path.isfile(dbFileName):
            print "database file is not found"
            return False
        adapter = SQLiteAdapter()
        adapter.connect("sqlite:///" + dbFileName)

        correctedStartDate = self.startDate - timedelta(days=200)
        quotes = adapter.getQuotations(self.crossName, self.timeframe, correctedStartDate, self.endDate)
        self.open = [x.Open for x in quotes]
        self.close = [x.Close for x in quotes]
        self.high = [x.High for x in quotes]
        self.low = [x.Low for x in quotes]
        self.times = [datetime(x.Date.year, x.Date.month, x.Date.day, x.Time.hour, x.Time.minute) for x in quotes]
        self.deltaBetweenStarts = TradingSimulator.countTimeItems(correctedStartDate, self.startDate, self.times)

        return True

    @staticmethod
    def countTimeItems(startDatetime, endDatetime, times):
        # delta = (self.endDate - self.startDate).total_seconds() / (60 * self.timeframe)
        timeItems = filter(lambda x: x >= startDatetime and x <= endDatetime, times)
        return len(timeItems)

    def simulate(self, startBalance, moneySettings, simParam):

        self.SPREAD = simParam.spread*moneySettings.onePointScale

        # delta = (self.endDate - self.startDate).total_seconds() / (60 * self.timeframe)
        delta = TradingSimulator.countTimeItems(self.startDate, self.endDate, self.times)
        account = TradingAccount(startBalance, 1)
        strategy = IchimokuStrategy(simParam.entryStrategy, simParam.exitStrategy, simParam.slStrategy, simParam.specificParameters,
                                    simParam.stopLossSize, moneySettings.onePointScale)
        # strategy = RSEVStrategy(simParam.exitStrategy, simParam.slStrategy, simParam.specificParameters,
        #                                   simParam.stopLossSize, moneySettings.onePointScale)

        strategy.setData(self.high, self.low, self.open, self.close, self.times)
        strategy.computeOpenPositions()
        openPosition = None

        for i in range(0, int(delta-1)):
            currentIndex = i + int(self.deltaBetweenStarts)

            # crossing the stop loss level
            if (openPosition != None):
                if openPosition.positionType == SignalType.BUY:
                    isStopLossCrossed = self.low[currentIndex] < openPosition.stopLoss if simParam.checkAgainstAllPrices \
                                else self.close[currentIndex] < openPosition.stopLoss
                if openPosition.positionType == SignalType.SELL:
                    isStopLossCrossed = self.high[currentIndex] > openPosition.stopLoss if simParam.checkAgainstAllPrices \
                                else self.close[currentIndex] > openPosition.stopLoss
                if isStopLossCrossed:
                    print "exit using stop loss"
                    openPosition.closePrice = openPosition.stopLoss
                    openPosition.closeDatetime = self.times[currentIndex]
                    account.commitOperation(openPosition, moneySettings, True)
                    openPosition = None

            # if time to close not because of the stop loss level
            if (openPosition != None):
                closeIndex = strategy.isTimeToClose(currentIndex, openPosition)
                if (closeIndex != None):
                    openPosition.closePrice = self.close[currentIndex]
                    openPosition.closeDatetime = self.times[currentIndex]
                    account.commitOperation(openPosition, moneySettings, False)
                    openPosition = None

            # signal if the balance is non-positive
            if (openPosition == None):
                if (account.balance <= 0):
                    print "Account slit ;(. Balance" + str(account.balance)
                    break

            # check if it is necessary to open a position
            if (openPosition == None):
                currentTime = self.times[currentIndex]
                if currentTime.hour in simParam.workingHours:
                    timeToOpen = strategy.isTimeToOpen(currentIndex)
                    if (timeToOpen == SignalType.BUY):
                        print str(self.times[currentIndex]) + " BUY"
                        if simParam.entryStrategy in ["en_price_leaves_cloud_v2"]:
                            openPosPrice = self.open[currentIndex] + self.SPREAD
                        else:
                            openPosPrice = self.close[currentIndex] + self.SPREAD
                        stopLoss = strategy.getStopLoss(SignalType.BUY, openPosPrice, currentIndex, moneySettings.onePointScale)
                        if stopLoss >= openPosPrice:
                            raise Exception("stopLoss >= openPosPrice")
                        openPosition = TradingPosition(SignalType.BUY, openPosPrice, stopLoss, self.times[currentIndex], moneySettings, account.startBalance)
                    elif (timeToOpen == SignalType.SELL):
                        print str(self.times[currentIndex]) + " SELL"
                        if simParam.entryStrategy in ["en_price_leaves_cloud_v2"]:
                            openPosPrice = self.open[currentIndex] - self.SPREAD
                        else:
                            openPosPrice = self.close[currentIndex] - self.SPREAD
                        stopLoss = strategy.getStopLoss(SignalType.SELL, openPosPrice, currentIndex, moneySettings.onePointScale)
                        if stopLoss <= openPosPrice:
                            raise Exception("stopLoss <= openPosPrice")
                        openPosition = TradingPosition(SignalType.SELL, openPosPrice, stopLoss, self.times[currentIndex], moneySettings, account.startBalance)

            # useWithoutLossFlag
            if (openPosition != None and simParam.useWithoutLossFlag):
                if openPosition.positionType == SignalType.BUY:
                    difference = self.close[currentIndex] - openPosition.openPrice
                    #if difference > math.fabs(openPosition.openPrice - openPosition.initialStopLoss):
                    if difference >= moneySettings.onePointScale:
                        openPosition.setStopLoss(openPosition.openPrice + moneySettings.onePointScale)
                elif openPosition.positionType == SignalType.SELL:
                    difference = openPosition.openPrice - self.close[currentIndex]
                    if difference >= moneySettings.onePointScale:
                        openPosition.setStopLoss(openPosition.openPrice - moneySettings.onePointScale)

            # trailing
            if (openPosition != None and simParam.useTrailing):
                strategy.trailing_change(openPosition, self.close[currentIndex], currentIndex)


            # add account statistics
            account.accountChanges.append(account.balance +
                                  TradingAccount.evaluatePositionCost(openPosition, self.close[currentIndex], moneySettings))

        # save how the account balance has been changing
        self.saveSimultionInfo(account, simParam)


    def saveSimultionInfo(self, account, simParam):
        filepath = "../results/trading_simulation/"
        filename = self.generateFilename(self.crossName, simParam.exitStrategy, simParam.stopLossSize, simParam.useTrailing,
                                         simParam.slStrategy, simParam.useWithoutLossFlag, simParam.specificParameters)
        plotFileName = filepath + filename + ".png"
        plotTimes = filter(lambda x: x >= self.startDate, self.times)
        dataPlot = SimplePlot()
        for i in range(len(plotTimes) - len(account.accountChanges)):
            account.accountChanges.append(account.balance)
        dataPlot.addMajorLineData(plotTimes, account.accountChanges, 'Account')
        dataPlot.savePlot(plotFileName, 'Account balance (' + filename + ")")
        print "End: Account balance = " + str("%.2f" % account.balance)

        if simParam.createLog:
            print "log generating..."
            filename = filename.replace("\n", "")
            plotFileName = plotFileName.replace("\n", "")
            account.generateLog(self.timeframe, filepath, filename, plotFileName)


    def generateFilename(self, crossName,curExitStrategy, curStopLossSize, curUseTrailing, curSLStrategy, useWithoutLossFlag, specificParameters):
        if self.timeframe == 5:
            frameStr = "5m"
        if self.timeframe == 15:
            frameStr = "15m"
        if self.timeframe == 30:
            frameStr = "30m"
        elif self.timeframe == 60:
            frameStr = "1h"
        elif self.timeframe == 240:
            frameStr = "4h"
        elif self.timeframe == 1440:
            frameStr = "1d"
        # period
        periodStr = str(self.startDate.month) + "." + str(self.startDate.year) + "-" + str(self.endDate.month) + "." + str(self.endDate.year)
        # for the string
        return crossName + "_" + periodStr + '_TF(' + frameStr +')_EX(' + curExitStrategy + ')\n_SL(' + str(curStopLossSize) + ')_TR(' + str(
            curUseTrailing) + ') SLTYPE(' + curSLStrategy +'), WL('+ str(useWithoutLossFlag) +')\n' + specificParameters.toString()


    def preSaveQuotations(self, filename, quotes):
        #key = self.crossName + "_" + str(self.timeframe) + "_" + str(correctedStartDate.year) + "_" + str(self.endDate.year)
        with open("../../temp/" + filename + ".tmp", 'wb') as fp:
            cPickle.dump(quotes, fp)